<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperacaoIncoterm extends Model
{
    protected $fillable = [
        'operacao_id',
        'incoterm_id'
    ];
}
