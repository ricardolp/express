<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parceiro extends Model
{
    protected $fillable = [
        'razao_social',
        'cnpj',
        'telefone',
        'fundacao',
        'endereco'
    ];

    public static function lista()
    {
        $arr = [];
        $itens = \App\Parceiro::all();

        foreach ($itens as $item) {
            $arr[$item->id] = $item->razao_social;
        }

        return $arr;
    }
}
