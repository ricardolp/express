<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperacaoFromAberta extends Model
{
    protected $fillable = [
        'descricao'
    ];
}
