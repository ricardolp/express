<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoServico extends Model
{
    protected $fillable = [
        'descricao'
    ];

    public static function lista()
    {
        $arr = [];
        $itens = \App\TipoServico::all();

        foreach ($itens as $item) {
            $arr[$item->id] = $item->descricao;
        }

        return $arr;
    }
}
