<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacaoPerfil extends Model
{
    protected $fillable = [
        'is_ncm',
        'is_licenca_importacao',
        'orgao_anuente',
        'is_icms',
        'regime_icms',
        'is_imposto_federal',
        'beneficio_imposto_federal',
        'is_debito',
        'is_debito_afrmm',
        'is_logistica',
        'is_seguro',
        'outros',
        'banco',
        'agencia',
        'cc',
        'cnpj',
        'cliente_id'
    ];
}
