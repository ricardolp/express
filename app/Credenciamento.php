<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Credenciamento extends Model
{
/*    protected $dates = ['vencimento', 'data'];*/

    protected $fillable = [
        'data',
        'vencimento',
        'path_file',
        'cliente_id',
        'item_credenciamento_id',
    ];

    public function item()
    {
        return $this->belongsTo( \App\ItemCredenciamento::class, 'item_credenciamento_id');
    }


    public function getVencimentoAttribute($data)
    {
        if($data != null && $data != ''){
            $data = Carbon::createFromFormat('Y-m-d', $data);
            return $data->format('d/m/Y');
        } else {
            return '';
        }

    }

    public function getDataAttribute($data)
    {

        if($data != null && $data != ''){
            $data = Carbon::createFromFormat('Y-m-d', $data);
            return $data->format('d/m/Y');
        } else {
            return '';
        }

    }

    public static function itemCredenciamento($id)
    {
        $item = \App\ItemCredenciamento::find($id);

        if($item)
            return $item->descricao;

        return '';
    }
}
