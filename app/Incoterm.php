<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incoterm extends Model
{
    protected $fillable = [
        'descricao',
        'is_importacao',
        'is_exportacao',
        'is_maritmo',
        'is_aereo',
        'is_rodoviario',
    ];
}
