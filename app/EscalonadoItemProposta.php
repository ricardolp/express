<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EscalonadoItemProposta extends Model
{
    protected $fillable = [
        'valor',
        'valor_minimo',
        'valor_maximo',
        'id_item_proposta'
    ];
}
