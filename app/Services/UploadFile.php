<?php
/**
 * Created by PhpStorm.
 * User: Ricardo
 * Date: 15/10/2016
 * Time: 22:04
 */

namespace App\Services;


class UploadFile
{
    protected $host = '54.94.180.41';
    protected $user = 'ricardo';
    protected $pwd = '6720720054e9d24fbf6c20a831ff287e';


    public function toDirect($path, $name){
        $data = date('Y.m.d');
        $path_file = "/Avulso/" . Session::get('usuario_id') . "/". $data . "/";

        $conn_id = ftp_connect($this->host, 2100) or die ("Cannot connect to host");

        ftp_login($conn_id, $this->usr, $this->pwd) or die("Cannot login");

        ftp_pasv ($conn_id, true);

        if(!ftp_chdir($conn_id, $path_file."/")){
            $newdir = explode('/', $path_file);
            $iddir = $newdir[2];
            $datadir = $newdir[3];

            ftp_chdir($conn_id, "/Avulso/");

            if(!ftp_chdir($conn_id, "/".$iddir."/")){
                ftp_mkdir($conn_id, $iddir);

                ftp_chdir($conn_id, "/Avulso/".$iddir."/");
                if(!ftp_chdir($conn_id, "/".$datadir."/")){
                    ftp_mkdir($conn_id, $datadir);

                    ftp_chdir($conn_id, "/Avulso/".$iddir."/".$datadir."/");
                    $upload = ftp_put($conn_id, $name, $path, FTP_BINARY);
                }
            }
        } else {
            $upload = ftp_put($conn_id, $name, $path, FTP_BINARY);
        }

        if(!$upload) {
            ftp_close($conn_id);
            return false;
        } else {
            ftp_close($conn_id);
            return true;
        }
    }
}