<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direct extends Model
{
    protected $fillable = [
        'processo_importacao',
        'data_importacao',
        'processo_exportacao',
        'data_exportacao',
        'cliente_id'
    ];
}
