<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Ligacao extends Model
{
    protected $fillable = [
        'data',
        'hora',
        'duracao',
        'assunto',
        'cliente_id'
    ];

    public function getDataAttribute($data)
    {

        if($data != null && $data != ''){
            $data = Carbon::createFromFormat('Y-m-d', $data);
            return $data->format('d/m/Y');
        } else {
            return '';
        }

    }
}
