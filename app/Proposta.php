<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Proposta extends Model
{
    protected $fillable = [
        'data_proposta',
        'is_enviado',
        'is_accept',
        'justificativa',
        'tipo_proposta',
        'path_url',
        'obs',
        'contato',
        'is_imported',
        'cliente_id',
        'contato_id',
        'tokenizer',
    ];

    public function getStatusAttribute()
    {

        switch($this->is_accept){
            case 1:
                return "<span class=\"label label-default\">Não enviada</span>";
                break;
            case 2:
                return "<span class=\"label label-success\">Aguardando cliente</span>";
                break;
            case 3:
                return "<span class=\"label label-primary\">Aceita pelo cliente</span>";
                break;
            case 4:
                return "<span class=\"label label-danger\">Negada pelo cliente</span>";
                break;
        }

    }

    public function getCreatedAtAttribute($date)
    {
        if(strpos('000', $date) || $date == null){
            return null;
        }

        $date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        return $date->format('d/m/Y');
    }
}
