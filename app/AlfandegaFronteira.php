<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlfandegaFronteira extends Model
{
    protected $fillable = [
        'descricao',
        'is_obrigatorio',
        'is_followup',
        'is_kpi',
        'is_direct',
        'is_noticia',
    ];

    public static function lista()
    {
        $arr = [];
        $itens = \App\AlfandegaFronteira::all();

        foreach ($itens as $item) {
            $arr[$item->id] = $item->descricao;
        }

        return $arr;
    }
}
