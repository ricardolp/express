<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seguro extends Model
{
    protected $fillable = [
        'valor_compra',
        'valor_venda',
        'cliente_id'
    ];
}
