<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableRelCredenciamento extends Model
{
    protected $fillable = [
        'user_id',
        'column_0',
        'column_1',
        'column_2',
    ];
}
