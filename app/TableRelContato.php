<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableRelContato extends Model
{
    protected $fillable = [
        'user_id',
        'column_0',
        'column_1',
        'column_2',
        'column_3',
        'column_4',
        'column_5',
        'column_6',
        'column_7',
        'column_8',
        'column_9',
        'column_10',
    ];
}
