<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipanteVisita extends Model
{
    protected $fillable = [
        'participante_visita',
        'visita_id'
    ];
}
