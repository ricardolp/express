<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DepartamentoController extends Controller
{
    public function index()
    {
        $departamentos = \App\Departamento::all();
        return view('admin.departamento.index')->with(compact('departamentos'));
    }

    public function create()
    {
        return view('admin.departamento.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        \App\Departamento::create($data);

        return redirect()->route('departamento.index')->with('success', 'Departamento Cadastrada com Sucesso!');
    }

    public function edit($id)
    {
        $departamento = \App\Departamento::findOrFail($id);

        return view('admin.departamento.edit')->with(compact('departamento'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $departamento = \App\Departamento::findOrFail($id);
        $departamento->update($data);
        $departamento->is_obrigatorio = (isset($data['is_obrigatorio'])) ? 1 : 0;
        $departamento->is_followup    = (isset($data['is_followup'])) ? 1 : 0;
        $departamento->is_kpi         = (isset($data['is_kpi'])) ? 1 : 0;
        $departamento->is_direct      = (isset($data['is_direct'])) ? 1 : 0;
        $departamento->is_noticia     = (isset($data['is_noticia'])) ? 1 : 0;
        $departamento->save();

        return redirect()->route('departamento.index')->with('success', 'Departamento Alterada com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            \App\Departamento::find($id)->delete();
            return redirect()->route('departamento.index')->with('success', 'Departamento Deletada com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('departamento.index')->with('error', 'Ocorreu um erro ao deletar a Departamento!');
        }

    }
}
