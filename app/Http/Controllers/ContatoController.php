<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContatoController extends Controller
{
    public function create($cliente_id)
    {
        $cliente = \App\Cliente::find($cliente_id);
        $funcao = \App\Funcao::lista();
        $departamento = \App\Departamento::lista();

        return view('admin.contato.create')->with(compact('cliente_id', 'cliente', 'funcao', 'departamento'));
    }

    public function edit($id)
    {
        $contato = \App\Contato::find($id);
        $cliente = \App\Cliente::find($contato->cliente_id);
        $funcao = \App\Funcao::pluck('descricao', 'id')->toArray();
        $departamento = \App\Departamento::pluck('descricao', 'id')->toArray();
        

        $cliente_id = $contato->cliente_id;
        return view('admin.contato.edit')->with(compact('cliente_id', 'cliente', 'funcao', 'departamento', 'contato'));


    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_noticia']  = (isset($data['is_noticia'])) ? 1 : 0;
        $data['is_followup']       = (isset($data['is_followup'])) ? 1 : 0;
        $data['is_kpi']    = (isset($data['is_kpi'])) ? 1 : 0;
        $data['is_direct']    = (isset($data['is_direct'])) ? 1 : 0;

        $data['data_lembranca']           = $this->format($data['data_lembranca']);
        $data['tempo_empresa']           = $this->format($data['tempo_empresa']);

        $contato = \App\Contato::create($data);

        return redirect()->route('cliente.view', $contato->cliente_id)->with('success', 'Contato cadastrado com sucesso');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['is_noticia']  = (isset($data['is_noticia'])) ? 1 : 0;
        $data['is_followup']       = (isset($data['is_followup'])) ? 1 : 0;
        $data['is_kpi']    = (isset($data['is_kpi'])) ? 1 : 0;
        $data['is_direct']    = (isset($data['is_direct'])) ? 1 : 0;

        $data['data_lembranca']           = $this->format($data['data_lembranca']);
        //$data['tempo_empresa']           = $this->format($data['tempo_empresa']);

        $contato = \App\Contato::find($id);
        $contato->update($data);

        return redirect()->route('cliente.view', $contato->cliente_id)->with('success', 'Contato cadastrado com sucesso');
    }

    public function destroy($id)
    {
        $contato = \App\Contato::find($id);

        if($contato)
            $contato->delete();

        return back();
    }

    public function set($id)
    {
        $contato = \App\Contato::find($id);

        $contato->status = ($contato->status == 1) ? 0 : 1;

        $contato->save();

        return;
    }

    public function show($id)
    {
        $proposta = \App\Proposta::find($id);
        $contatos = \App\Contato::where('cliente_id', $proposta->cliente_id)->get();

        return $contatos;
    }

    public function all($id)
    {
        $users = \App\User::all();
        $contatos = \App\Contato::where('cliente_id', $id)->get();

        $html = '<li class="clearfix">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" >
                                <select id="select13" name="participantes[]" class="form-control select2-list<%=index%>">
                                    <optgroup label="Usuários TWS">';

                                    foreach($users as $user){
                                        $html .= '<option value="'.$user->name.'">'.$user->name.'</option>';
                                    }

                                    $html .= '<optgroup label="Contatos do Cliente">';
                                    foreach($contatos as $contato){
                                        $html .= '<option value="'.$contato->nome.'">'.$contato->nome.'</option>';
                                    }


                            $html .= '</select>
                                <label for="participante">Participante</label>
                            </div>
                        </div>
                    </div>
                </li>';

        return ['html' => $html];

    }
}
