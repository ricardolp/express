<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class VisitaController extends Controller
{
    public function create($cliente)
    {
        $cliente = \App\Cliente::find($cliente);
        $users = \App\User::all();
        $contatos = \App\Contato::where('cliente_id', $cliente->id)->get();

        return view('admin.visita.create')->with(compact('cliente', 'users', 'contatos'));
    }

    public function edit($id)
    {
        $visita = \App\Visita::find($id);
        $cliente = \App\Cliente::find($visita->cliente_id);
        $users = \App\User::all();
        $contatos = \App\Contato::where('cliente_id', $cliente->id)->get();
        $participantes = \App\ParticipanteVisita::where('visita_id', $id)->get();
        return view('admin.visita.edit')->with(compact('visita','cliente', 'users', 'contatos', 'participantes'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['is_comercial']    = (isset($data['is_comercial'])) ? 1 : 0;
        $data['is_financeiro']   = (isset($data['is_financeiro'])) ? 1 : 0;
        $data['is_logistico']    = (isset($data['is_logistico'])) ? 1 : 0;
        $data['is_suporte']      = (isset($data['is_suporte'])) ? 1 : 0;

        $data['data']           = $this->format($data['data']);

        $visita = \App\Visita::find($id);
        $visita->update($data);


        if($request->has('participantes')){
            foreach ($data['participantes'] as $participante) {
                $item = new \App\ParticipanteVisita();
                $item->participante_visita = $participante;
                $item->visita_id = $visita->id;
                $item->save();
            }
        }


        return redirect()->route('cliente.view', $visita->cliente_id)->with('success', 'Visita Adicionada');
    }

    public function removeParticipante($id)
    {
        $participante = \App\ParticipanteVisita::find($id)->delete();

        return back();
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_comercial']    = (isset($data['is_comercial'])) ? 1 : 0;
        $data['is_financeiro']   = (isset($data['is_financeiro'])) ? 1 : 0;
        $data['is_logistico']    = (isset($data['is_logistico'])) ? 1 : 0;
        $data['is_suporte']      = (isset($data['is_suporte'])) ? 1 : 0;

        $data['data']           = $this->format($data['data']);

        $visita = \App\Visita::create($data);

        if($request->has('participantes')){
            foreach ($data['participantes'] as $participante) {
                $item = new \App\ParticipanteVisita();
                $item->participante_visita = $participante;
                $item->visita_id = $visita->id;
                $item->save();
            }
        }


        return redirect()->route('cliente.view', $visita->cliente_id)->with('success', 'Visita Adicionada');

    }

    public function reagendar(Request $request)
    {
        $data = $request->all();

        $data['data'] = $this->format($data['data']);

        $visita = \App\Visita::find($data['id']);
        $visita->status = 4;
        $visita->update($data);

        return redirect()->route('cliente.view', $visita->cliente_id)->with('success', 'A Visita foi reagendada');
    }

    public function cancelar(Request $request)
    {
        $data = $request->all();

        $visita = \App\Visita::find($data['id']);
        $visita->status = 3;
        $visita->justificativa = $data['justificativa'];
        $visita->save();

        return redirect()->route('cliente.view', $visita->cliente_id)->with('success', 'A Visita foi cancelada');
    }

    public function confirmar($id)
    {
        $visita = \App\Visita::find($id);
        $visita->status = 2;
        $visita->save();

        return back()->with('success', 'A Visita foi confirmada');
    }

    public function relatorio($visita_id, $cliente_id)
    {
        $visita = \App\Visita::skip(1)->take(1)->get();
        $razao = \App\Cliente::find($cliente_id)->razao_social;

        if($visita->count() > 0){
            if($visita[0]->data != '0000-00-00' && $visita[0]->data != '30/11/-0001'){
                $data = $visita[0]->data;
            } else {
                $data = '2015-01-01';
            }
        } else {
            $data = '2015-01-01';
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://express.twscomex.com.br/ws/generic_query.php',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'query' => 'select
                              ITENSFATURA.ITF_OPERACAO,
                              sum(ITENSFATURA.ITF_VALOR) as total,
                              count(ITENSFATURA.ITF_CODIGO) as qtd
                            from ITENSFATURA
                            inner join OPERACAO on OPERACAO.OPE_CODIGO = ITENSFATURA.ITF_OPERACAO
                            inner join fatura on FATURA.FAT_CODIGO = ITENSFATURA.ITF_CODIGO and fatura.FAT_MOVIMENTO = ITENSFATURA.ITF_MOVIMENTO
                            inner join cliente on CLIENTE.CLI_CODIGO = fatura.FAT_CLIENTE
                            where
                                operacao.OPE_SERVICO = \'T\' and
                                FATURA.FAT_ESTADO = \'F\' and
                                ITENSFATURA.ITF_PAG = \'P\' and
                                FATURA.FAT_MOVIMENTO = 1 and
                                ITENSFATURA.itf_obs <> \'\' AND
                                CLIENTE.CLI_RS = \''.$razao.'\' and
                                ITENSFATURA.ITF_DATA >= \''.$data.'\'
                            group by
                              ITENSFATURA.ITF_OPERACAO
                            order by
                              ITENSFATURA.ITF_OPERACAO'
            )
        ));
        $resp = curl_exec($curl);
                
        curl_close($curl);

        $manage = (array) json_decode($resp);

        return view('admin.cliente.relatorio')->with(compact('manage', 'visita_id'));
    }

    public function submit(Request $request)
    {
        $data = $request->all();

        $visita = \App\Visita::find($data['visita_id']);

        $visita->status = 6;
        $visita->save();

        foreach ($data as $key => $item) {
            if(is_array($data[$key])){
                $data[$key]['visita_id'] = $data['visita_id'];
                $data[$key]['servico'] = $key;
                $data[$key]['data_relatorio'] = date('Y-m-d');
                \App\RelatorioVisita::create($data[$key]);
            }
        }

        return redirect()->route('cliente.view', $visita->cliente_id)->with('success', 'A Visita foi Concluida');

    }

    public function view($visita_id)
    {

        $visita = \App\Visita::find($visita_id);

        $relatorio = \App\RelatorioVisita::where('visita_id', $visita->id)->get();               

        $participantes = \App\ParticipanteVisita::where('visita_id', $visita->id)->get();

        

        $html= '<table class="footable table table-stripped">
                    <tr>
                        <td class="col-md-1"><b>Status: </b></td>
                        <td class="col-md-9">'.$visita->statusVisita.'</td>
                    </tr>
                    <tr>
                        <td class="col-md-1"><b>Responsável</b></td>
                        <td class="col-md-9">'.$visita->responsavel.'</td>
                    </tr>
                    <tr>
                        <td class="col-md-1"><b>Local</b></td>
                        <td class="col-md-9">'.$visita->local.'</td>
                    </tr>
                    <tr>
                        <td class="col-md-1"><b>Data</b></td>
                        <td class="col-md-9">'.$visita->data.'</td>
                    </tr>
                    <tr>
                        <td class="col-md-1"><b>Hora</b> </td>
                        <td class="col-md-9"> '.$visita->hora.'</td>
                    </tr>
                    <tr>
                        <td class="col-md-1"<b>Pauta</b></td>
                        <td class="col-md-9">'.$visita->pauta.'</td>
                    </tr>
                    <tr>
                        <table class="table table-striped">
                            <thead>
                                <tr><th>Relatório de Visita</th></tr>
                            </thead>
                            <tbody>
                                <!--tr><td style="text-align:center;" colspan=2-->';

        if($relatorio->count() > 0){
            
            //$html .= '<a href="'.route('visita.relatorio.generate').'" class="btn btn-success pull-left" style="margin-bottom: 10px;"><i class="fa fa-download"></i>&nbsp; Visita</a>';
            foreach($relatorio as $row){                
                $html .='<tr style="text-align:left;"><td style="text-align:left;">Serviço: '.$row->servico.'</td><td style="text-align:left;">Avaliação: '.$row->avaliacao.'</td></tr>';
                $html .='<tr><td style="text-align:left;" colspan=2>Dificuldades:<br>'.$row->obs_dificuldade.'<!--/td></tr-->';
                $html .='<!--tr style="text-align:left;"><td style="text-align:left;" colspan=2--><br>Problemas:<br>'.$row->obs_problema.'</td></tr>';
            }
        } else {
            $html .= 'não possui relatório disponível!';
        }


        $html .= '                    <!--/td>
                                    </tr-->
                                </tbody>
                            </table> 
                        </tr>
                        <tr>
                            <table class="table table-striped">
                                <thead>
                                    <tr><th>Participantes</th></tr>
                                    </thead>
                                    <tbody>';

        foreach($participantes as $row){

            $html .='<tr style="text-align:left;"><td style="text-align:left;">'.$row->participante_visita.'</td></tr>';
        }


        $html .='                    </tbody>
                            </table> 
                        </tr>
                        <tr>
                          <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th width="10px" style="width: 10px:text-align:center;" >#</th>
                                        <th>Assunto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td style="text-align:center;">'.$this->getTrueOrFalseSpam($visita->is_comercial).'</td>
                                        <td>Comercial</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">'.$this->getTrueOrFalseSpam($visita->is_financeiro).'</td>
                                        <td>Financeiro</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">'.$this->getTrueOrFalseSpam($visita->is_logigistico).'</td>
                                        <td>Logistico</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:center;">'.$this->getTrueOrFalseSpam($visita->is_suporte).'</td>
                                        <td>Suporte</td>
                                    </tr>
                                    </tbody>
                                </table> 
							</tr>
						</tbody>
					 </table>';

        return ['html' => $html];
    }
}
