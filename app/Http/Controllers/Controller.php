<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public $query_resumo = "select OPERACAO.OPE_CODIGO, sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, ITENSNFSE.INE_VALOR)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, ITENSFATURA.ITF_VALOR, 0)) as valor, sum(iif(ITENSNFSE.INE_VALOR is NULL, 1, 0)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, 1)) as quantidade from operacao inner join ITENSFATURA on ITENSFATURA.ITF_OPERACAO = OPERACAO.OPE_CODIGO inner join fatura on FATURA.FAT_CODIGO = ITENSFATURA.ITF_CODIGO and fatura.FAT_MOVIMENTO = ITENSFATURA.ITF_MOVIMENTO inner join cliente on CLIENTE.CLI_CODIGO = fatura.FAT_CLIENTE left join NFSEANTERIOR on NFSEANTERIOR.NSE_FATURA = FATURA.FAT_CODIGO and NFSEANTERIOR.NSE_MOVIMENTO = FATURA.FAT_MOVIMENTO and NFSEANTERIOR.NSE_INDITF = ITENSFATURA.ITF_IND and (NFSEANTERIOR.NSE_DATAGERACAO >= '{first_date}' and NFSEANTERIOR.NSE_DATAGERACAO <= '{current_date}') left join ITENSNFSE on ITENSNFSE.INE_CODIGOITF = ITENSFATURA.ITF_CODIGONOVO left join NFSE on nfse.NSE_CODIGO = ITENSNFSE.INE_CODIGONFSE and NFSE.NSE_DATAGERACAO >= '{first_date}' and NFSE.NSE_DATAGERACAO <= '{current_date}' where operacao.OPE_SERVICO = 'T' and FATURA.FAT_ESTADO = 'F' and (NFSEANTERIOR.NSE_ESTADO = 'Emitido' or NFSE.NSE_ESTADO = 'Emitido') and CLIENTE.CLI_RS = '{razao_social}' group by OPERACAO.OPE_CODIGO order by OPERACAO.OPE_CODIGO ";

    public $query_detail = "select   OPERACAO.OPE_CODIGO,   FATURA.FAT_TIPO,   sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, ITENSNFSE.INE_VALOR)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, ITENSFATURA.ITF_VALOR, 0)) as valor,   iif(ITENSNFSE.INE_VALOR is NULL, extract(year from cast (ITENSFATURA.ITF_DATA as date)), extract(year from cast (NFSE.NSE_DATAGERACAO as date))) as ano,   sum(iif(ITENSNFSE.INE_VALOR is NULL, 1, 0)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, 1)) as quantidade   from   operacao   inner join ITENSFATURA on ITENSFATURA.ITF_OPERACAO = OPERACAO.OPE_CODIGO   inner join fatura on FATURA.FAT_CODIGO = ITENSFATURA.ITF_CODIGO and   fatura.FAT_MOVIMENTO = ITENSFATURA.ITF_MOVIMENTO   inner join cliente on CLIENTE.CLI_CODIGO = fatura.FAT_CLIENTE   left join NFSEANTERIOR on NFSEANTERIOR.NSE_FATURA = FATURA.FAT_CODIGO and NFSEANTERIOR.NSE_MOVIMENTO = FATURA.FAT_MOVIMENTO and   NFSEANTERIOR.NSE_INDITF = ITENSFATURA.ITF_IND and   (NFSEANTERIOR.NSE_DATAGERACAO >= '{first_date}' and NFSEANTERIOR.NSE_DATAGERACAO <= '{current_date}')   left join ITENSNFSE on ITENSNFSE.INE_CODIGOITF = ITENSFATURA.ITF_CODIGONOVO   left join NFSE on nfse.NSE_CODIGO = ITENSNFSE.INE_CODIGONFSE and   NFSE.NSE_DATAGERACAO >= '{first_date}' and   NFSE.NSE_DATAGERACAO <= '{current_date}'   where   operacao.OPE_SERVICO = 'T' and   FATURA.FAT_ESTADO = 'F' and   (NFSEANTERIOR.NSE_ESTADO = 'Emitido' or NFSE.NSE_ESTADO = 'Emitido') and   CLIENTE.CLI_RS = '{razao_social}' and   OPE_CODIGO = '{servico}'   group by   fatura.FAT_TIPO,   ano,   OPERACAO.OPE_CODIGO   order by   OPERACAO.OPE_CODIGO,   ano";

    public $query_sub_detail = 'select  OPERACAO.OPE_CODIGO,  FATURA.fat_tipo,  sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, ITENSNFSE.INE_VALOR)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, ITENSFATURA.ITF_VALOR, 0)) as valor,  iif(ITENSNFSE.INE_VALOR is NULL, extract(year from cast (ITENSFATURA.ITF_DATA as date)), extract(year from cast (NFSE.NSE_DATAGERACAO as date))) as ano,  iif(ITENSNFSE.INE_VALOR is NULL, extract(month from cast (ITENSFATURA.ITF_DATA as date)), extract(month from cast (NFSE.NSE_DATAGERACAO as date))) as mes,  sum(iif(ITENSNFSE.INE_VALOR is NULL, 1, 0)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, 1)) as quantidade  from  operacao  inner join ITENSFATURA on ITENSFATURA.ITF_OPERACAO = OPERACAO.OPE_CODIGO  inner join fatura on FATURA.FAT_CODIGO = ITENSFATURA.ITF_CODIGO and  fatura.FAT_MOVIMENTO = ITENSFATURA.ITF_MOVIMENTO  inner join cliente on CLIENTE.CLI_CODIGO = fatura.FAT_CLIENTE  left join NFSEANTERIOR on NFSEANTERIOR.NSE_FATURA = FATURA.FAT_CODIGO and NFSEANTERIOR.NSE_MOVIMENTO = FATURA.FAT_MOVIMENTO and  NFSEANTERIOR.NSE_INDITF = ITENSFATURA.ITF_IND and    (NFSEANTERIOR.NSE_DATAGERACAO >= \'{first_date}\' and NFSEANTERIOR.NSE_DATAGERACAO <= \'{current_date}\')  left join ITENSNFSE on ITENSNFSE.INE_CODIGOITF = ITENSFATURA.ITF_CODIGONOVO  left join NFSE on nfse.NSE_CODIGO = ITENSNFSE.INE_CODIGONFSE and  NFSE.NSE_DATAGERACAO >= \'{first_date}\' and  NFSE.NSE_DATAGERACAO <= \'{current_date}\'    where  operacao.OPE_SERVICO = \'T\' and  FATURA.FAT_ESTADO = \'F\' and  (NFSEANTERIOR.NSE_ESTADO = \'Emitido\' or NFSE.NSE_ESTADO = \'Emitido\') and  CLIENTE.CLI_RS = \'{razao_social}\' and   OPE_CODIGO = \'{servico}\'  and fatura.fat_tipo = \'{fatura}\' group by  fatura.fat_tipo,  ano,  MES,  OPERACAO.OPE_CODIGO    order by  OPERACAO.OPE_CODIGO,  ano,  mes';

    //public $query_resumo_cliente = 'select  ITENSFATURA.ITF_OPERACAO,  extract(year from cast (ITENSFATURA.ITF_DATA as date)) as ano,  extract(month from cast (ITENSFATURA.ITF_DATA as date)) as mes,  sum(ITENSFATURA.ITF_VALOR) as total,  count(ITENSFATURA.ITF_CODIGO) as qtd  from ITENSFATURA  inner join OPERACAO on OPERACAO.OPE_CODIGO = ITENSFATURA.ITF_OPERACAO  inner join fatura on FATURA.FAT_CODIGO = ITENSFATURA.ITF_CODIGO and fatura.FAT_MOVIMENTO = ITENSFATURA.ITF_MOVIMENTO  inner join cliente on CLIENTE.CLI_CODIGO = fatura.FAT_CLIENTE  where  operacao.OPE_SERVICO = \'T\' and  FATURA.FAT_ESTADO = \'F\' and  ITENSFATURA.ITF_PAG = \'P\' and  FATURA.FAT_MOVIMENTO = 1 and  ITENSFATURA.itf_obs <> \'\' AND  CLIENTE.CLI_RS = \'{razao_social}\' and  ITENSFATURA.ITF_DATA >= \'{first_date}\'  group by  ano,  mes,  ITENSFATURA.ITF_OPERACAO  order by  ano,  mes,  ITENSFATURA.ITF_OPERACAO';
    public $query_resumo_cliente = 'select OPERACAO.OPE_CODIGO, iif(ITENSNFSE.INE_VALOR is NULL, extract(year from cast (NFSEANTERIOR.NSE_DATAGERACAO as date)), extract(year from cast (NFSE.NSE_DATAGERACAO as date))) as ano, iif(ITENSNFSE.INE_VALOR is NULL, extract(month from cast (NFSEANTERIOR.NSE_DATAGERACAO as date)), extract(month from cast (NFSE.NSE_DATAGERACAO as date))) as mes, sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, ITENSNFSE.INE_VALOR)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, ITENSFATURA.ITF_VALOR, 0)) as valor, sum(iif(ITENSNFSE.INE_VALOR is NULL, 1, 0)) + sum(iif(ITENSNFSE.INE_VALOR is NULL, 0, 1)) as quantidade from operacao inner join ITENSFATURA on ITENSFATURA.ITF_OPERACAO = OPERACAO.OPE_CODIGO inner join fatura on FATURA.FAT_CODIGO = ITENSFATURA.ITF_CODIGO and fatura.FAT_MOVIMENTO = ITENSFATURA.ITF_MOVIMENTO inner join cliente on CLIENTE.CLI_CODIGO = fatura.FAT_CLIENTE left join NFSEANTERIOR on NFSEANTERIOR.NSE_FATURA = FATURA.FAT_CODIGO and NFSEANTERIOR.NSE_MOVIMENTO = FATURA.FAT_MOVIMENTO and NFSEANTERIOR.NSE_INDITF = ITENSFATURA.ITF_IND and (NFSEANTERIOR.NSE_DATAGERACAO >= \'{first_date}\' and NFSEANTERIOR.NSE_DATAGERACAO <= \'{current_date}\') left join ITENSNFSE on ITENSNFSE.INE_CODIGOITF = ITENSFATURA.ITF_CODIGONOVO left join NFSE on nfse.NSE_CODIGO = ITENSNFSE.INE_CODIGONFSE and NFSE.NSE_DATAGERACAO >= \'{first_date}\' and NFSE.NSE_DATAGERACAO <= \'{current_date}\' where operacao.OPE_SERVICO = \'T\' and FATURA.FAT_ESTADO = \'F\' and (NFSEANTERIOR.NSE_ESTADO = \'Emitido\' or NFSE.NSE_ESTADO = \'Emitido\') and CLIENTE.CLI_RS = \'{razao_social}\' group by ano, mes,fatura.FAT_TIPO, OPERACAO.OPE_CODIGO order by OPERACAO.OPE_CODIGO';

    public function format($date)
    {

        if((isset($date) && !empty($date)) && ($date != '30/11/-0001' && $date != '0000-00-00')){
            $format = Carbon::createFromFormat('d/m/Y', $date);
            return $format->format($format);
        }

        return null;
    }

    public function clear($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9@.\-]/', '', $string); // Removes special chars.
    }

    public function objectToArray($d) {
        if (is_object ( $d )) {
            $d = get_object_vars ( $d );
        }

        if (is_array ( $d )) {
            return array_map ( __FUNCTION__, $d );
        } else {
            return $d;
        }
    }

    public function statusVisita($id){
        switch($id){
            case 1:
                return "<span class=\"label label-success\">Agendada</span>";
                break;
            case 2:
                return "<span class=\"label label-primary\">Confirmada</span>";
                break;
            case 3:
                return "<span class=\"label label-danger\">Cancelada</span>";
                break;
            case 4:
                return "<span class=\"label label-warning\">Reagendada</span>";
                break;
            case 5:
                return "<span class=\"label label-info\">Atrasada</span>";
                break;
            case 6:
                return "<span class=\"label label-info\">Concluída</span>";
                break;
        }

    }

    public function getTrueOrFalseSpam($value)
    {
        if(!$value){
            return '<span class="label label-primary"><i class="fa fa-check"></i></span>';
        }

        return '<span class="label label-danger"><i class="fa fa-close"></i></span>';
    }

    public function getMonth($m)
    {
        switch ($m){
            case 1:
                return 'Janeiro';
                break;
            case 2:
                return 'Fevereiro';
                break;
            case 3:
                return 'Março';
                break;
            case 4:
                return 'Abril';
                break;
            case 5:
                return 'Maio';
                break;
            case 6:
                return 'Junho';
                break;
            case 7:
                return 'Julho';
                break;
            case 8:
                return 'Agosto';
                break;
            case 9:
                return 'Setembro';
                break;
            case 10:
                return 'Outubro';
                break;
            case 11:
                return 'Novembro';
                break;
            case 12:
                return 'Dezembro';
                break;
        }
    }

}
