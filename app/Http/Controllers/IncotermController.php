<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class IncotermController extends Controller
{
    public function index()
    {
        $incoterms = \App\Incoterm::all();
        return view('admin.incoterm.index')->with(compact('incoterms'));
    }

    public function create()
    {
        return view('admin.incoterm.create');
    }

    public function show(Request $request)
    {
        $data = $request->all();
        $data['is_aereo'] = ($data['aereo'] == 1) ? 1 : 0;
        $data['is_rodoviario'] = ($data['rodoviario'] == 1) ? 1 : 0;
        $data['is_maritmo'] = ($data['maritmo'] == 1) ? 1 : 0;

        if($data['operacao'] == 1){
            $incoterms = \App\Incoterm::where(['is_maritmo' => $data['is_maritmo']])
                    ->orWhere(['is_aereo' => $data['is_aereo']])
                    ->orWhere(['is_rodoviario' => $data['is_rodoviario']])
                    ->orWhere(['is_importacao' => 1])
                    ->get();
        } else {
            $incoterms = \App\Incoterm::where(['is_maritmo' => $data['is_maritmo']])
                ->orWhere(['is_aereo' => $data['is_aereo']])
                ->orWhere(['is_rodoviario' => $data['is_rodoviario']])
                ->orWhere(['is_exportacao' => 1,])
                ->get();
        }



        return $incoterms;

    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_maritmo']         = (isset($data['is_maritmo'])) ? 1 : 0;
        $data['is_aereo']           = (isset($data['is_aereo'])) ? 1 : 0;
        $data['is_rodoviario']      = (isset($data['is_rodoviario'])) ? 1 : 0;

        \App\Incoterm::create($data);

        return redirect()->route('incoterm.index')->with('success', 'Incoterm Cadastrado com Sucesso!');
    }

    public function edit($id)
    {
        $incoterm = \App\Incoterm::findOrFail($id);

        return view('admin.incoterm.edit')->with(compact('incoterm'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['is_importacao']          = (isset($data['is_importacao'])) ? 1 : 0;
        $data['is_exportacao']          = (isset($data['is_exportacao'])) ? 1 : 0;
        $data['is_maritmo']             = (isset($data['is_maritmo'])) ? 1 : 0;
        $data['is_aereo']               = (isset($data['is_aereo'])) ? 1 : 0;
        $data['is_rodoviario']          = (isset($data['is_rodoviario'])) ? 1 : 0;

        $incoterm = \App\Incoterm::findOrFail($id);
        $incoterm->update($data);
        $incoterm->save();


        return redirect()->route('incoterm.index')->with('success', 'Incoterm Alterado com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            $delete = \App\Incoterm::find($id);

            if($delete){
                $delete->delete();
            }
            return redirect()->route('incoterm.index')->with('success', 'Incoterm Deletado com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('incoterm.index')->with('error', 'Ocorreu um erro ao deletar o Incoterm!');
        }

    }
}
