<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RelatorioController extends Controller
{
    public function cliente()
    {
        $clientes = \App\Cliente::all();
        $table = \App\TableRelCliente::where('user_id', Session::get('usuario_id'))->get();

        if($table->count() > 0)
            $table = $table->first();

        return view('admin.relatorio.cliente')->with(compact('clientes', 'table'));
    }

    public function visita()
    {
        $visitas = \App\Visita::join('clientes', 'clientes.id', '=', 'visitas.cliente_id')->get();
        $table = \App\TableRelVisita::where('user_id', Session::get('usuario_id'))->get();

        if($table->count() > 0)
            $table = $table->first();

        return view('admin.relatorio.visita')->with(compact('visitas', 'table'));
    }

    public function contato()
    {
        $contatos = \App\Contato::join('clientes', 'clientes.id', '=', 'contatos.cliente_id')->select('contatos.*', 'clientes.is_status',  'clientes.razao_social',  'clientes.cnpj')->get();
        $table = \App\TableRelContato::where('user_id', Session::get('usuario_id'))->get();

        if($table->count() > 0)
            $table = $table->first();


        return view('admin.relatorio.contato')->with(compact('contatos', 'table'));
    }

    public function credenciamento()
    {
        $tipos = \App\ItemCredenciamento::listaDesc();
        $credenciamentos = \App\Credenciamento::join('clientes', 'clientes.id', '=', 'credenciamentos.cliente_id')->select('credenciamentos.*', 'clientes.razao_social')->get();
        $table = \App\TableRelCredenciamento::where('user_id', Session::get('usuario_id'))->get();

        if($table->count() > 0)
            $table = $table->first();

        return view('admin.relatorio.credenciamento')->with(compact('credenciamentos', 'table', 'tipos'));
    }

    public function ligacao()
    {
        $ligacaos = \App\Ligacao::join('clientes', 'clientes.id', '=', 'ligacaos.cliente_id')->select('ligacaos.*', 'clientes.razao_social')->get();
        $clientes = \App\Cliente::listaFiltro();
        $table = \App\TableRelLigacao::where('user_id', Session::get('usuario_id'))->get();

        if($table->count() > 0)
            $table = $table->first();
        return view('admin.relatorio.ligacao')->with(compact('ligacaos','clientes', 'table'));
    }

    public function generateCliente($id, $status_param, $param)
    {

        $select = '';
        $fields = \App\TableRelCliente::where('user_id', $id)->select('*')->get();

        if($fields->count() > 0){
            $fields = $fields->first();
            $select .= ($fields->column_0 == 1) ? 'razao_social,' : '';
            $select .= ($fields->column_1 == 1) ? 'cnpj,' : '';
            $select .= ($fields->column_2 == 1) ? 'telefone,' : '';
            $select .= ($fields->column_3 == 1) ? 'fundacao,' : '';
            $select .= ($fields->column_4 == 1) ? 'is_status' : '';
        }
        $status = ($status_param == 'x') ? 'is_status is not null' : 'is_status = ' . $status_param;

        $ids = explode('-', $param);
        array_shift($ids);
        $clientes = \App\Cliente::whereIn('id', $ids)->whereRaw($status)->select(DB::raw($select))->get();

        $html = "
                <table width='90%' border='1'>
                    <thead><tr>";
        $html .= ($fields->column_0 == 1) ? '<th>Razão Social</th>' : '';
        $html .= ($fields->column_1 == 1) ? '<th>CNPJ</th>' : '';
        $html .= ($fields->column_2 == 1) ? '<th>Telefone</th>' : '';
        $html .= ($fields->column_3 == 1) ? '<th>Fundacao</th>' : '';
        $html .= ($fields->column_4 == 1) ? '<th>Status</th>' : '';
        $html .="</tr></thead>
                    <tbody>";

        foreach ($clientes as $cliente) {
            $ss = ($cliente->is_status == 0) ? 'Inativo' : (($cliente->is_status == 1) ? 'Prospect' : 'Ativo');
            $html .= '<tr>';
            $html .= ($fields->column_0 == 1) ? '<th>'.$cliente->razao_social.'</th>' : '';
            $html .= ($fields->column_1 == 1) ? '<th>'.$cliente->cnpj.'</th>' : '';
            $html .= ($fields->column_2 == 1) ? '<th>'.$cliente->telefone.'</th>' : '';
            $html .= ($fields->column_3 == 1) ? '<th>'.$cliente->fundacao.'</th>' : '';
            $html .= ($fields->column_4 == 1) ? '<th>'.$ss.'</th>' : '';
            $html .= '</tr>';
        }

        $html .="</tbody>
               <table>";


        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment;filename=relatorio_cliente.xls");
        header("Content-Transfer-Encoding: binary ");
        // Imprime o conteúdo da nossa tabela no arquivo que será gerado
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $html;
    }

    public function generateContato($id, $status_param, $param)
    {

        $select = '';
        $fields = \App\TableRelContato::where('user_id', $id)->select('*')->get();

        if($fields->count() > 0){
            $fields = $fields->first();
            $select .= ($fields->column_0 == 1) ? 'contatos.nome,' : '';
            $select .= ($fields->column_1 == 1) ? 'contatos.departamento_id,' : '';
            $select .= ($fields->column_2 == 1) ? 'contatos.funcao_id,' : '';
            $select .= ($fields->column_3 == 1) ? 'contatos.telefone,' : '';
            $select .= ($fields->column_4 == 1) ? 'contatos.celular,' : '';
            $select .= ($fields->column_5 == 1) ? 'contatos.aniversario,' : '';
            $select .= ($fields->column_6 == 1) ? 'contatos.email,' : '';
            $select .= ($fields->column_7 == 1) ? 'clientes.is_status,' : '';
            $select .= ($fields->column_8 == 1) ? 'contatos.status,' : '';
            $select .= ($fields->column_9 == 1) ? 'clientes.razao_social,' : '';
            $select .= ($fields->column_10 == 1) ? 'clientes.cnpj' : '';
        }


        $status_param = ($status_param == 2) ? 0 : $status_param;
        $status = ($status_param == 'x') ? 'is_status is not null' : 'is_status = ' . $status_param;

        $ids = explode('-', $param);
        array_shift($ids);
        $contatos = \App\Contato::join('clientes', 'clientes.id', '=', 'contatos.cliente_id')
            ->whereIn('contatos.id', $ids)
            ->whereRaw($status)
            ->select(DB::raw($select), 'contatos.*', 'clientes.razao_social', 'clientes.is_status', 'clientes.cnpj')
            ->get();

        $html = "
                <table width='90%' border='1'>
                    <thead><tr>";
        $html .= ($fields->column_0 == 1) ? '<th>Nome</th>' : '';
        $html .= ($fields->column_1 == 1) ? '<th>Departamento</th>' : '';
        $html .= ($fields->column_2 == 1) ? '<th>Funçao</th>' : '';
        $html .= ($fields->column_3 == 1) ? '<th>Telefone</th>' : '';
        $html .= ($fields->column_4 == 1) ? '<th>Celular</th>' : '';
        $html .= ($fields->column_5 == 1) ? '<th>Aniversário</th>' : '';
        $html .= ($fields->column_6 == 1) ? '<th>Email</th>' : '';
        $html .= ($fields->column_7 == 1) ? '<th>Status do Cliente</th>' : '';
        $html .= ($fields->column_8 == 1) ? '<th>Status do Contato</th>' : '';
        $html .= ($fields->column_9 == 1) ? '<th>Razão Social</th>' : '';
        $html .= ($fields->column_10 == 1) ? '<th>CNPJ</th>' : '';
        $html .="</tr></thead>
                    <tbody>";

        foreach ($contatos as $contato) {
            $ss = ($contato->is_status == 0) ? 'Inativo' : (($contato->is_status == 1) ? 'Prospect' : 'Ativo');
            $dept = \App\Departamento::find($contato->departamento_id);
            $dept = ($dept) ? $dept->descricao : '';

            $func = \App\Funcao::find($contato->funcao_id);
            $func = ($func) ? $func->descricao : '';

            $s = ($contato->status == 0) ? 'Inativo' : 'Ativo';

            $html .= '<tr>';
            $html .= ($fields->column_0 == 1) ? '<th>'.$contato->nome.'</th>' : '';
            $html .= ($fields->column_1 == 1) ? '<th>'.$dept.'</th>' : '';
            $html .= ($fields->column_2 == 1) ? '<th>'.$func.'</th>' : '';
            $html .= ($fields->column_3 == 1) ? '<th>'.$contato->telefone.'</th>' : '';
            $html .= ($fields->column_4 == 1) ? '<th>'.$contato->celular.'</th>' : '';
            $html .= ($fields->column_5 == 1) ? '<th>'.$contato->aniversario.'</th>' : '';
            $html .= ($fields->column_6 == 1) ? '<th>'.$contato->email.'</th>' : '';
            $html .= ($fields->column_7 == 1) ? '<th>'.$ss.'</th>' : '';
            $html .= ($fields->column_8 == 1) ? '<th>'.$s.'</th>' : '';
            $html .= ($fields->column_9 == 1) ? '<th>'.$contato->razao_social.'</th>' : '';
            $html .= ($fields->column_10 == 1) ?'<th>'.$contato->cnpj.'</th>' : '';
            $html .= '</tr>';
        }

        $html .="</tbody>
               <table>";


        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment;filename=relatorio_cliente.xls");
        header("Content-Transfer-Encoding: binary ");
        // Imprime o conteúdo da nossa tabela no arquivo que será gerado
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $html;
    }

    public function generateCredenciamento($id, $param)
    {

        $fields = \App\TableRelCredenciamento::where('user_id', $id)->select('*')->get();

        $ids = explode('-', $param);
        array_shift($ids);
        $credenciamentos = \App\Credenciamento::join('clientes', 'clientes.id', '=', 'credenciamentos.cliente_id')
            ->whereIn('credenciamentos.id', $ids)
            ->select('credenciamentos.*', 'clientes.razao_social', 'clientes.is_status', 'clientes.cnpj')
            ->get();
        if($fields->count() > 0){
            $fields = $fields->first();

        $html = "
                <table width='90%' border='1'>
                    <thead><tr>";
        $html .= ($fields->column_0 == 1) ? '<th>Cliente</th>' : '';
        $html .= ($fields->column_1 == 1) ? '<th>Descrição</th>' : '';
        $html .= ($fields->column_2 == 1) ? '<th>Vencimento</th>' : '';
        $html .="</tr></thead>
                    <tbody>";

        foreach ($credenciamentos as $credenciamento) {
            $item = \App\ItemCredenciamento::find($credenciamento->item_credenciamento_id);
            $item = ($item) ? $item->descricao : '';

            $html .= '<tr>';
            $html .= ($fields->column_0 == 1) ? '<th>'.$credenciamento->razao_social.'</th>' : '';
            $html .= ($fields->column_1 == 1) ? '<th>'.$item.'</th>' : '';
            $html .= ($fields->column_2 == 1) ? '<th>'.$credenciamento->vencimento.'</th>' : '';
            $html .= '</tr>';
        }

        $html .="</tbody>
               <table>";

        }
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment;filename=relatorio_cliente.xls");
        header("Content-Transfer-Encoding: binary ");
        // Imprime o conteúdo da nossa tabela no arquivo que será gerado
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $html;
    }

    public function generateLigacao($id, $param)
    {

        $fields = \App\TableRelLigacao::where('user_id', $id)->select('*')->get();

        $ids = explode('-', $param);
        array_shift($ids);
        $ligacoes = \App\Ligacao::join('clientes', 'clientes.id', '=', 'ligacaos.cliente_id')
            ->whereIn('ligacaos.id', $ids)
            ->select('ligacaos.*', 'clientes.razao_social', 'clientes.is_status', 'clientes.cnpj')
            ->get();

        if($fields->count() > 0){
            $fields = $fields->first();

            $html = "
                <table width='90%' border='1'>
                    <thead><tr>";
            $html .= ($fields->column_0 == 1) ? '<th>Cliente</th>' : '';
            $html .= ($fields->column_1 == 1) ? '<th>Data</th>' : '';
            $html .= ($fields->column_2 == 1) ? '<th>Hora</th>' : '';
            $html .= ($fields->column_3 == 1) ? '<th>Duração</th>' : '';
            $html .= ($fields->column_4 == 1) ? '<th>Assunto</th>' : '';
            $html .="</tr></thead>
                    <tbody>";

            foreach ($ligacoes as $ligacao) {

                $html .= '<tr>';
                $html .= ($fields->column_0 == 1) ? '<th>'.$ligacao->razao_social.'</th>' : '';
                $html .= ($fields->column_1 == 1) ? '<th>'.$ligacao->data.'</th>' : '';
                $html .= ($fields->column_2 == 1) ? '<th>'.$ligacao->hora.'</th>' : '';
                $html .= ($fields->column_3 == 1) ? '<th>'.$ligacao->duracao.'</th>' : '';
                $html .= ($fields->column_4 == 1) ? '<th>'.$ligacao->assunto.'</th>' : '';
                $html .= '</tr>';
            }

            $html .="</tbody>
               <table>";

        }
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment;filename=relatorio_ligacao.xls");
        header("Content-Transfer-Encoding: binary ");
        // Imprime o conteúdo da nossa tabela no arquivo que será gerado
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $html;
    }

}
