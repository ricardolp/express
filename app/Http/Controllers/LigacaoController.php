<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class LigacaoController extends Controller
{
    public function index()
    {
        $ligacoes = \App\Ligacao::join('clientes', 'clientes.id', '=', 'ligacaos.cliente_id')->select('ligacaos.*', 'clientes.razao_social')->get();
        return view('admin.ligacao.index')->with(compact('ligacoes'));
    }

    public function create()
    {
        $clientes = \App\Cliente::lista();
        return view('admin.ligacao.create')->with(compact('clientes'));
    }

    public function edit($id)
    {
        $clientes = \App\Cliente::lista();
        $ligacao = \App\Ligacao::find($id);
        return view('admin.ligacao.edit')->with(compact('clientes', 'ligacao'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['data']           = $this->format($data['data']);
        $ligacao = \App\Ligacao::create($data);

        return redirect()->route('ligacao.index')->with('success', 'A Ligação foi cadastrada');
    }

    public function update(Request $request,$id)
    {
        $data = $request->all();

        $data['data']           = $this->format($data['data']);
        $ligacao = \App\Ligacao::find($id);
        $ligacao->update($data);

        return redirect()->route('ligacao.index')->with('success', 'A Ligação foi cadastrada');
    }

    public function destroy($id)
    {
        $ligacao = \App\Ligacao::find($id);

        if($ligacao){
            $ligacao->delete();
            return redirect()->route('ligacao.index')->with('success', 'A Ligação foi deletada');

        }
        return redirect()->route('ligacao.index')->with('error', 'Ocorreu um erro ao deletar a ligação');

    }
}
