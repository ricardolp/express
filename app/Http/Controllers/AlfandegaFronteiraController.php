<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AlfandegaFronteiraController extends Controller
{
    public function index()
    {
        $alfandegafronteiras = \App\AlfandegaFronteira::all();
        return view('admin.alfandegafronteira.index')->with(compact('alfandegafronteiras'));
    }

    public function create()
    {
        return view('admin.alfandegafronteira.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        \App\AlfandegaFronteira::create($data);

        return redirect()->route('alfandegafronteira.index')->with('success', 'Alfandega\Fronteira Cadastrada com Sucesso!');
    }

    public function edit($id)
    {
        $alfandegafronteira = \App\AlfandegaFronteira::findOrFail($id);

        return view('admin.alfandegafronteira.edit')->with(compact('alfandegafronteira'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $alfandegafronteira = \App\AlfandegaFronteira::findOrFail($id);
        $alfandegafronteira->update($data);
        $alfandegafronteira->save();

        return redirect()->route('alfandegafronteira.index')->with('success', 'Alfandega\Fronteira Alterada com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            \App\AlfandegaFronteira::find($id)->delete();
            return redirect()->route('alfandegafronteira.index')->with('success', 'Alfandega\Fronteira Deletada com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('alfandegafronteira.index')->with('error', 'Ocorreu um erro ao deletar a Alfandega\Fronteira!');
        }

    }
}
