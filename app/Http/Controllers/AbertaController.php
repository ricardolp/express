<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class AbertaController extends Controller
{
    public function servico(Request $request)
    {
        $return = [];
        $data = $request->all();

        if($data['start'] != ''){
            $format = Carbon::createFromFormat('m/d/Y', $data['start']);
            $start = $format->format($format);
            $start = explode(' ', $start);
            $start = $start[0];
        } else {
            $start = date('Y-m-d');
        }

        if($data['start'] != ''){
            $format = Carbon::createFromFormat('m/d/Y', $data['end']);
            $end = $format->format($format);
            $end = explode(' ', $end);
            $end = $end[0];
        } else {
            $end = date('Y-m-d');
        }

        $aberta = $this->getDataAberta($data['razao_social'], $start, $end);
        $aberta_result = json_decode($aberta, true);

        if(count($aberta_result) > 0){
            $ch = curl_init();
            $query = $this->query_resumo_cliente;
            $query = str_replace('{razao_social}', $data['razao_social'], $query);

            $url = "http://express.twscomex.com.br/ws/generic_query.php"; // where you want to post data
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['query'=>$query]); // define what you want to post
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
            $output = curl_exec ($ch); // execute
            $resumo_cliente = json_decode($output);
            curl_close ($ch); // close curl handle


            $i = 0;
            foreach ($aberta_result as $item) {
                $return[$i]['servico'] = $item['OPE_CODIGO'];
                $return[$i]['valor'] = $item['VALOR'];
                $return[$i]['data'] = [];

                $detail = $this->getDataAbertaDetail($data['razao_social'], $item['OPE_CODIGO'], $start, $end);
                $detail_results = json_decode($detail, true);

                foreach ($detail_results as $detail_result) {

                    if(!isset($return[$i]['data'][$detail_result['FAT_TIPO']])){
                        $return[$i]['data'][$detail_result['FAT_TIPO']] = [];
                    }

                    if(!isset($return[$i]['data'][$detail_result['FAT_TIPO']][$detail_result['ANO']])){
                        $return[$i]['data'][$detail_result['FAT_TIPO']][$detail_result['ANO']] = [];
                    }

                    $detail_sub = $this->getDataAbertaSubDetail($data['razao_social'], $item['OPE_CODIGO'], $detail_result['FAT_TIPO'], $detail_result['ANO'], $start, $end);
                    $sub_detail_results = json_decode($detail_sub, true);

                    foreach ($sub_detail_results as $sub_detail_result) {
                        if($sub_detail_result['ANO'] == $detail_result['ANO']){
                            array_push($return[$i]['data'][$detail_result['FAT_TIPO']][$detail_result['ANO']], $sub_detail_result);
                        }
                    }
                }

                $i++;
            }

        }

        $controller = new \App\Http\Controllers\Controller();
        $draggable = $return;

        $html = '<div class="col-md-12"><div class="panel-group" id="accordion-services">';

            for($i = 0; $i < count($draggable); $i++) {

                $html .= '<div class="card panel"><div class="card-head collapsed style-default ac" data-toggle="collapse" data-parent="#accordion' . $i . '" data-target="#accordion' . $i . 'i" aria-expanded="false"><header>' . $draggable[$i]["servico"] . '</header><div class="tools">R$ ' . $draggable[$i]["valor"] . '<a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a></div></div><div id="accordion' . $i . 'i" class="collapse" aria-expanded="false" style="height: 0px;"><div class="card-body">';

                if (isset($draggable[$i]["data"]["Importação"])) {
                    $html .='<div class="card panel"><div class="card-head style-default collapsed ac" data-toggle="collapse" data-parent="#accordion' . $i . '" data-target="#accordion' . $i . 'importacao" aria-expanded="false"><header>Importação</header><div class="tools"><a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a></div></div><div id="accordion'.  $i .'importacao" class="collapse" aria-expanded="false" style="height: 0px;"><div class="card-body">';

                    $j = 0;
                    foreach ($draggable[$i]["data"]["Importação"] as $key => $importacao) {

                    $html .='<div class="card panel"><div class="card-head style-default collapsed ac" data-toggle="collapse" data-parent="#accordion' . $i . '" data-target="#accordion' . $i . $j . '" aria-expanded="false"><header>' . $key . '</header><div class="tools"><a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a></div></div><div id="accordion'.  $i . $j .'" class="collapse" aria-expanded="false" style="height: 0px;"><div class="card-body"><table class="table no-margin"><thead><tr><th>#</th><th>Valor</th><th>Quantidade</th></tr></thead><tbody>';

                        foreach ($draggable[$i]["data"]["Importação"][$key] as $arr_o) {
                            $html .= '<tr><td>' . $controller->getMonth($arr_o["MES"]) . '</td><td>' . $arr_o["VALOR"] . '</td><td>' . $arr_o["QUANTIDADE"] . '</td></tr>';
                        }

                        $html .='</tbody></table></div></div></div>';

                        $j++;

                    }

                    $html .='</div></div></div>';
                }

                if (isset($draggable[$i]["data"]["Exportação"])) {
                    $html .='<div class="card panel"><div class="card-head style-default collapsed ac" data-toggle="collapse" data-parent="#accordion' . $i . '" data-target="#accordion' . $i . 'exportacao" aria-expanded="false"><header>Exportação</header><div class="tools"><a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a></div></div><div id="accordion' . $i . 'exportacao" class="collapse" aria-expanded="false" style="height: 0px;"><div class="card-body">';

                    $j = 0;

                    foreach ($draggable[$i]["data"]["Exportação"] as $key => $importacao) {
                        $html .='<div class="card panel"><div class="card-head style-default collapsed ac" data-toggle="collapse" data-parent="#accordion' . $i . '" data-target="#accordion' . $i . $j . 'ex" aria-expanded="false"><header>' . $key . '</header><div class="tools"><a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a></div></div><div id="accordion' . $i . $j . 'ex" class="collapse" aria-expanded="false" style="height: 0px;"><div class="card-body"><table class="table no-margin"><thead><tr><th>#</th><th>Valor</th><th>Quantidade</th></tr></thead><tbody>';

                        foreach ($draggable[$i]["data"]["Exportação"][$key] as $arr_o) {
                            $html .='<tr><td>' . $controller->getMonth($arr_o["MES"]) . '</td><td>' . $arr_o["VALOR"] . '</td><td>' . $arr_o["QUANTIDADE"] . '</td></tr>';
                        }

                        $html .='</tbody></table></div></div></div>';
                        $j++;

                    }

                    $html .='</div></div></div>';
                }
                $html .='</div></div></div>';
            }

        $html .='</div></div>';

        return ['html' => $html];

    }

    public function getDataAbertaSubDetail($cliente, $servico, $fatura, $ano,  $start, $end)
    {
        $ch = curl_init();
        $query = $this->query_sub_detail;
        $query = str_replace('{razao_social}', $cliente, $query);
        $query = str_replace('{servico}', $servico, $query);
        $query = str_replace('{fatura}', $fatura, $query);

        $query = str_replace('{current_date}', $end, $query);
        $query = str_replace('{first_date}', $start, $query);


        $url = "http://express.twscomex.com.br/ws/generic_query.php"; // where you want to post data
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['query'=>$query]); // define what you want to post
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        $output = curl_exec ($ch); // execute
        curl_close ($ch); // close curl handle

        return $output;
    }

    public function getDataAbertaDetail($cliente, $servico, $start, $end)
    {
        $ch = curl_init();
        $query = $this->query_detail;
        $query = str_replace('{razao_social}', $cliente, $query);
        $query = str_replace('{servico}', $servico, $query, $query);
        $query = str_replace('{current_date}', $end, $query);
        $query = str_replace('{first_date}', $start, $query);

        $url = "http://express.twscomex.com.br/ws/generic_query.php"; // where you want to post data
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['query'=>$query]); // define what you want to post
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        $output = curl_exec ($ch); // execute

        curl_close ($ch); // close curl handle

        return $output;
    }


    public function getDataAberta($cliente, $start, $end)
    {
        $ch = curl_init();
        $query = $this->query_resumo;
        $query = str_replace('{razao_social}', $cliente, $query);
        $query = str_replace('{current_date}', $end, $query);
        $query = str_replace('{first_date}', $start, $query);

        $url = "http://express.twscomex.com.br/ws/generic_query.php"; // where you want to post data
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);  // tell curl you want to post something
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['query'=>$query]); // define what you want to post
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // return the output in string format
        $output = curl_exec ($ch); // execute
        curl_close ($ch); // close curl handle

        return $output;
    }

}
