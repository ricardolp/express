<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class OperacaoIncotermController extends Controller
{
    public function incoterm($id)
    {
        $operacaoes = \App\OperacaoIncoterm::where('operacao_id', $id)->get();

        return $operacaoes;
    }
}
