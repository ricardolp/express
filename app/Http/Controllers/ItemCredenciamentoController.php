<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ItemCredenciamentoController extends Controller
{
    public function index()
    {
        $itemcredenciamentos = \App\ItemCredenciamento::all();
        return view('admin.itemcredenciamento.index')->with(compact('itemcredenciamentos'));
    }

    public function create()
    {
        return view('admin.itemcredenciamento.create');
    }

    public function isFile($id)
    {
        $item = \App\ItemCredenciamento::find($id);

        if($item->is_file){
            return ['success' => true];
        }

        return ['success' => false];
    }

    public function store(Request $request)
    {
        $data = $request->all();

        \App\ItemCredenciamento::create($data);

        return redirect()->route('itemcredenciamento.index')->with('success', 'Item de Credenciamento Cadastrado com Sucesso!');
    }

    public function edit($id)
    {
        $itemcredenciamento = \App\ItemCredenciamento::findOrFail($id);

        return view('admin.itemcredenciamento.edit')->with(compact('itemcredenciamento'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $itemcredenciamento = \App\ItemCredenciamento::findOrFail($id);
        $itemcredenciamento->update($data);
        $itemcredenciamento->is_file = (isset($data['is_file'])) ? 1 : 0;
        $itemcredenciamento->save();

        return redirect()->route('itemcredenciamento.index')->with('success', 'Item de Credenciamento Alterado com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            $delete = \App\ItemCredenciamento::find($id);

            if($delete){
                $delete->delete();
            }
            return redirect()->route('itemcredenciamento.index')->with('success', 'Item de Credenciamento Deletado com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('itemcredenciamento.index')->with('error', 'Ocorreu um erro ao deletar o Item de Credenciamento!');
        }

    }
}
