<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

class TableController extends Controller
{
    public function cliente(Request $request)
    {
        $arr = [];
        $data = $request->all();

        $table = \App\TableRelCliente::where('user_id',Session::get('usuario_id'))->get();

        $field = $data['column'];
        $arr['column_'.$field] = ($data['state'] == 'true') ? 1 : 0;

        if($table->count() > 0){
            $table->first()->update($arr);
        } else {
            $arr['user_id'] = Session::get('usuario_id');
            \App\TableRelCliente::create($arr);
        }

        return;
    }

    public function visita(Request $request)
    {
        $arr = [];
        $data = $request->all();

        $table = \App\TableRelVisita::where('user_id',Session::get('usuario_id'))->get();

        $field = $data['column'];
        $arr['column_'.$field] = ($data['state'] == true) ? 1 : 0;

        if($table->count() > 0){
            $table->first()->update($arr);
        } else {
            $arr['user_id'] = Session::get('usuario_id');
            \App\TableRelVisita::create($arr);
        }

        return;
    }

    public function credenciamento(Request $request)
    {
        $arr = [];
        $data = $request->all();

        $table = \App\TableRelCredenciamento::where('user_id',Session::get('usuario_id'))->get();

        $field = $data['column'];
        $arr['column_'.$field] = ($data['state'] == 'true') ? 1 : 0;

        if($table->count() > 0){
            $table->first()->update($arr);
        } else {
            $arr['user_id'] = Session::get('usuario_id');
            \App\TableRelCredenciamento::create($arr);
        }

        return;
    }

    public function contato(Request $request)
    {
        $arr = [];
        $data = $request->all();

        $table = \App\TableRelContato::where('user_id',Session::get('usuario_id'))->get();

        $field = $data['column'];
        $arr['column_'.$field] = ($data['state'] == 'true') ? 1 : 0;

        if($table->count() > 0){
            $table->first()->update($arr);
        } else {
            $arr['user_id'] = Session::get('usuario_id');
            \App\TableRelContato::create($arr);
        }

        return;
    }

    public function ligacao(Request $request)
    {
        $arr = [];
        $data = $request->all();

        $table = \App\TableRelLigacao::where('user_id',Session::get('usuario_id'))->get();

        $field = $data['column'];
        $arr['column_'.$field] = ($data['state'] == 'true') ? 1 : 0;

        if($table->count() > 0){
            $table->first()->update($arr);
        } else {
            $arr['user_id'] = Session::get('usuario_id');
            \App\TableRelLigacao::create($arr);
        }

        return;
    }
}
