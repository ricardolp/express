<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ExpressController extends Controller
{
    public function index()
    {
        $ativos = \App\Cliente::where('is_status', 2)->count();
        $inativos = \App\Cliente::where('is_status', 0)->count();
        $prospect = \App\Cliente::where('is_status', 1)->count();

        return view('admin.dashboard.index')->with(compact('ativos', 'inativos', 'prospect'));
    }

    public function getView()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $data = $request->all();

        $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-Type: application/json'
                ),
                'ssl' => array(
                    'verify_peer'      => false,
                    'verify_peer_name' => false,
                ),
            )
        );


        $endereço = 'https://54.94.180.41/api/CheckLogin.php?Login=' . $data['email'] . '&Senha=' . hash ( 'sha256', $data['password']) . '';

        $json_file = file_get_contents($endereço, FALSE, $context);
        $json_str = json_decode ( $json_file, true );
        if ($json_str ['ok'] === 1) {

            for($i = 0; $i < count ( $json_str ['listaEmpresas'] ); $i ++) {

                $empresa[$json_str['listaEmpresas'][$i]['value']] = $json_str['listaEmpresas'][$i]['RazaoSocial'];
            }

            Session::put('login', true);
            Session::put('usuario_id', $json_str['idUsuario']);
            Session::put('token', $json_str['token']);

            return redirect()->route('index');
        }

        return back();
    }

    public function events()
    {
        $result = [];
        $temp = [];
        $start_date = $_GET['start'];
        $end_date = $_GET['end'];

        $visitas = \App\Visita::join('clientes', 'clientes.id', '=', 'visitas.cliente_id')
            ->select(DB::raw('visitas.id, clientes.razao_social as descricao, visitas.data, "" as cliente, 1 AS v'))
            ->get();

        foreach ($visitas as $visita) {
            $temp['id'] = $visita->id;
            $temp['descricao'] = $visita->descricao;
            $temp['data'] = $visita->data;
            $temp['cliente'] = '';
            $temp['v'] = 1;

            array_push($result, $temp);
        }

        $credenciamentos = \App\Credenciamento::join('item_credenciamentos', 'item_credenciamentos.id', '=', 'credenciamentos.item_credenciamento_id')
            ->join('clientes', 'credenciamentos.cliente_id', '=','clientes.id')
            ->select(DB::raw("credenciamentos.id, item_credenciamentos.descricao, vencimento as data, razao_social as cliente, 2 as v"))
            ->get();

        foreach ($credenciamentos as $credenciamento) {
            $temp['id'] = $credenciamento->id;
            $temp['descricao'] = $credenciamento->descricao;
            $temp['data'] = $credenciamento->data;
            $temp['cliente'] = $credenciamento->cliente;
            $temp['v'] = 2;

            array_push($result, $temp);
        }


        $json = '[';
        foreach($result as $row) {

            if($row['v'] == 1){
                $title = "Visita";
            } else {
                $title = $row['cliente'];
            }
            $json .= '{"title": "'.$title.' - '. $row['descricao'] .'","start": "'.$row['data'].'", "id": "'.$row['id'].'"}';

            if ($row !== end($result)) {
                $json .= ',';
            }
        }
        $json .= ']';

        return $json;
    }

    public function visita($id)
    {
        return \App\Visita::find($id);
    }

    public function credenciamento($id)
    {
        return \App\Credenciamento::find($id);
    }
}
