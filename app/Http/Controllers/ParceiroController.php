<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ParceiroController extends Controller
{
    public function index()
    {
        $parceiros = \App\Parceiro::all();
        return view('admin.parceiro.index')->with(compact('parceiros'));
    }

    public function create()
    {
        return view('admin.parceiro.create');
    }

    public function edit($id)
    {
        $parceiro = \App\Parceiro::find($id);
        $contatos = \App\ContatoParceiro::where('parceiro_id', $id)->get();
        return view('admin.parceiro.edit')->with(compact('parceiro', 'contatos'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $parceiro = \App\Parceiro::create($data);

        return redirect()->route('parceiro.index')->with('success', 'O Parceiro foi adicionado');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $parceiro = \App\Parceiro::find($id);
        $parceiro->update($data);

        return redirect()->route('parceiro.index')->with('success', 'O Parceiro foi atualizado');
    }
}
