<?php

namespace App\Http\Controllers;

use App\Services\UploadFile;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class CredenciamentoController extends Controller
{
    /**
     * @var UploadFile
     */
    private $uploadFile;

    public function __construct(UploadFile $uploadFile)
    {
        $this->uploadFile = $uploadFile;
    }

    public function create($cliente_id)
    {
        $cliente = \App\Cliente::find($cliente_id);
        $itens_credenciamentos = \App\ItemCredenciamento::lista();
        return view('admin.credenciamento.create')->with(compact('cliente', 'itens_credenciamentos'));
    }

    public function edit($id)
    {
        $credenciamento = \App\Credenciamento::find($id);
        $cliente = \App\Cliente::find($credenciamento->cliente_id);
        $itens_credenciamentos = \App\ItemCredenciamento::lista();
        return view('admin.credenciamento.edit')->with(compact('cliente', 'itens_credenciamentos','credenciamento'));
    }

    public function show($id)
    {
        $credenciamento = \App\Credenciamento::find($id);


        return ['data' => $credenciamento->data, 'vencimento' => $credenciamento->vencimento, 'file' => URL::to('/files/credenciamento/'.$credenciamento->path_file), 'label' => $credenciamento->item->descricao];
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['data'] = date('Y-m-d');

        if($request->hasFile('file')){
            $file = $request->file('file');
            $name = \App\ItemCredenciamento::find($data['item_credenciamento_id'])->descricao;
            $extension = $file->getClientOriginalExtension();
            $real_name = $this->clear($name) . '_' . round(microtime(true) * 1000). '.' .$extension;
            $file->move(public_path('files/credenciamento/'), $real_name);

            $data['path_file'] = $real_name;

            //$this->uploadFile->toDirect(public_path('files/credenciamento/'), $real_name);
        }

        $data['vencimento'] = $this->format($data['vencimento']);

        $credenciamento = \App\Credenciamento::find($id);
        $credenciamento->update($data);

        return redirect()->route('cliente.view', $credenciamento->cliente_id)->with('success', 'O Credenciamento foi criado com Sucesso');
    }

    public function store(Request $request)
    {
        $data = $request->all();


        $data['data'] = date('Y-m-d');
        if($request->hasFile('file')){
            $file = $request->file('file');
            $name = \App\ItemCredenciamento::find($data['item_credenciamento_id'])->descricao;
            $extension = $file->getClientOriginalExtension();
            $real_name = $this->clear($name) . '_' . round(microtime(true) * 1000). '.' .$extension;
            $file->move(public_path('files/credenciamento/'), $real_name);

            $data['path_file'] = $real_name;

            //$this->uploadFile->toDirect(public_path('files/credenciamento/'), $real_name);
        }

        $data['vencimento'] = $this->format($data['vencimento']);

        $credenciaento = \App\Credenciamento::create($data);

        return redirect()->route('cliente.view', $credenciaento->cliente_id)->with('success', 'O Credenciamento foi criado com Sucesso');
    }
}
