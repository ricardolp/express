<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TipoCobrancaController extends Controller
{
    public function index()
    {
        $tiposcobrancas = \App\TipoCobranca::all();
        return view('admin.tipocobranca.index')->with(compact('tiposcobrancas'));
    }

    public function create()
    {
        return view('admin.tipocobranca.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        \App\TipoCobranca::create($data);

        return redirect()->route('tipocobranca.index')->with('success', 'Tipo de Cobrança Cadastrado com Sucesso!');
    }

    public function edit($id)
    {
        $tipocobranca = \App\TipoCobranca::findOrFail($id);

        return view('admin.tipocobranca.edit')->with(compact('tipocobranca'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $tipocobranca = \App\TipoCobranca::findOrFail($id);
        $tipocobranca->update($data);
        $tipocobranca->is_value = (isset($data['is_value'])) ? 1 : 0;
        $tipocobranca->save();

        return redirect()->route('tipocobranca.index')->with('success', 'Tipo de Cobrança Alterado com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            $delete = \App\TipoCobranca::find($id);

            if($delete){
                $delete->delete();
            }
            return redirect()->route('tipocobranca.index')->with('success', 'Tipo de Cobrança Deletado com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('tipocobranca.index')->with('error', 'Ocorreu um erro ao deletar o Tipo de Cobrança!');
        }

    }

    public function show($id)
    {
        return \App\TipoCobranca::find($id);
    }
}
