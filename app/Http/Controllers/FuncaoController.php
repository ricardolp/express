<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mockery\CountValidator\Exception;

class FuncaoController extends Controller
{
    public function index()
    {
        $funcoes = \App\Funcao::all();
        return view('admin.funcao.index')->with(compact('funcoes'));
    }

    public function create()
    {
        return view('admin.funcao.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        \App\Funcao::create($data);

        return redirect()->route('funcao.index')->with('success', 'Função Cadastrada com Sucesso!');
    }

    public function edit($id)
    {
        $funcao = \App\Funcao::findOrFail($id);

        return view('admin.funcao.edit')->with(compact('funcao'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $funcao = \App\Funcao::findOrFail($id);
        $funcao->update($data);
        $funcao->is_obrigatorio = (isset($data['is_obrigatorio'])) ? 1 : 0;
        $funcao->is_followup    = (isset($data['is_followup'])) ? 1 : 0;
        $funcao->is_kpi         = (isset($data['is_kpi'])) ? 1 : 0;
        $funcao->is_direct      = (isset($data['is_direct'])) ? 1 : 0;
        $funcao->is_noticia     = (isset($data['is_noticia'])) ? 1 : 0;
        $funcao->save();

        return redirect()->route('funcao.index')->with('success', 'Função Alterada com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            \App\Funcao::find($id)->delete();
            return redirect()->route('funcao.index')->with('success', 'Função Deletada com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('funcao.index')->with('error', 'Ocorreu um erro ao deletar a Função!');
        }

    }
}
