<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TipoServicoController extends Controller
{
    public function index()
    {
        $tiposservicos = \App\TipoServico::all();
        return view('admin.tiposervico.index')->with(compact('tiposservicos'));
    }

    public function create()
    {
        return view('admin.tiposervico.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        \App\TipoServico::create($data);

        return redirect()->route('tiposervico.index')->with('success', 'Tipo de Serviço Cadastrado com Sucesso!');
    }

    public function edit($id)
    {
        $tiposervico = \App\TipoServico::findOrFail($id);

        return view('admin.tiposervico.edit')->with(compact('tiposervico'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $tiposervico = \App\TipoServico::findOrFail($id);
        $tiposervico->update($data);
        $tiposervico->save();

        return redirect()->route('tiposervico.index')->with('success', 'Tipo de Serviço Alterado com Sucesso!');
    }

    public function destroy($id)
    {
        try{
            $delete = \App\TipoServico::find($id);

            if($delete){
                $delete->delete();
            }
            return redirect()->route('tiposervico.index')->with('success', 'Tipo de Serviço Deletado com Sucesso!');
        }catch(Exception $e){
            return redirect()->route('tiposervico.index')->with('error', 'Ocorreu um erro ao deletar o Tipo de Serviço!');
        }

    }
}
