<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ContatoParceiroController extends Controller
{
    public function create($parceiro_id)
    {
        $parceiro = \App\Parceiro::find($parceiro_id);
        $funcao = \App\Funcao::all();
        $departamento = \App\Departamento::all();
        return view('admin.parceiro.contato.create')->with(compact('parceiro', 'departamento', 'funcao'));
    }

    public function edit($contato)
    {
        $contato = \App\ContatoParceiro::find($contato);
        $parceiro = \App\Parceiro::find($contato->parceiro_id);
        $funcao = \App\Funcao::all();
        $departamento = \App\Departamento::all();
        return view('admin.parceiro.contato.edit')->with(compact('parceiro', 'departamento', 'funcao', 'contato'));
    }


    public function store(Request $request)
    {
        $data = $request->all();

        $contato = \App\ContatoParceiro::create($data);

        return redirect()->route('parceiro.edit', $contato->parceiro_id)->with('success', 'Contato cadastrado!');

    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $contato = \App\ContatoParceiro::find($id);
        $contato->update($data);

        return redirect()->route('parceiro.edit', $contato->parceiro_id)->with('success', 'Contato atualizado!');
    }

    public function destroy($id)
    {
        $contato = \App\ContatoParceiro::find($id);

        if($contato){
            $contato->delete();
            return back()->with('success', 'Contato deletado!');
        }

        return back()->with('error', 'Erro ao deletar contato!');

    }
}
