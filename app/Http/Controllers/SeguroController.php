<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SeguroController extends Controller
{
    public function create($id)
    {
        return view('admin.seguro.create')->with(compact('id'));
    }

    public function edit($id)
    {
        $seguro = \App\Seguro::find($id);
        $id = $seguro->cliente_id;
        return view('admin.seguro.edit')->with(compact('seguro', 'id'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $seguro = \App\Seguro::create($data);

        return redirect()->route('cliente.view', $seguro->cliente_id);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $seguro = \App\Seguro::find($id);
        $seguro->update($data);

        return redirect()->route('cliente.view', $seguro->cliente_id);
    }
}
