<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class DirectController extends Controller
{
    public function create($id)
    {
        $cliente = \App\Cliente::find($id);

        $curl = curl_init();

        $fatura = $cliente->aberta_id;


        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://twsdirect.com.br/express/ws/generic_query.php',
            CURLOPT_USERAGENT => 'Codular Sample cURL Request',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'query' => "SELECT * FROM fatura where fat_codigo > '0000005663' AND fat_cliente = ".$fatura
            )
        ));

        //dd("SELECT * FROM fatura where fat_codigo > '0000005663' AND fat_cliente = ".$fatura);

        $resp = curl_exec($curl);

        $faturas = (array)  json_decode($resp);

        return view('admin.direct.create')->with(compact('id', 'faturas'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['data_importacao']  = $this->format($data['data_importacao']);
        $data['data_exportacao']  = $this->format($data['data_exportacao']);

        $direct = \App\Direct::create($data);

        return redirect()->route('cliente.view', $direct->cliente_id)->with('success', '');

    }
}
