<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class EnderecoController extends Controller
{
    public function create($cliente_id)
    {
        $cliente = \App\Cliente::find($cliente_id);
        return view('admin.endereco.create')->with(compact('cliente', 'cliente_id'));
    }

    public function edit($id)
    {
        $endereco = \App\Endereco::find($id);
        $cliente = \App\Cliente::find($endereco->cliente_id);
        $cliente_id = $cliente->id;
        return view('admin.endereco.edit')->with(compact('cliente', 'cliente_id', 'endereco'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $endereco = \App\Endereco::create($data);

        return redirect()->route('cliente.view', $endereco->cliente_id);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $endereco = \App\Endereco::find($id);
        $endereco->update($data);

        return redirect()->route('cliente.view', $endereco->cliente_id);
    }

    public function destroy($id)
    {
        $endereco = \App\Endereco::find($id);
        $cliente = $endereco->cliente_id;

        if($endereco){
            $endereco->delete();
        }

        return redirect()->route('cliente.view', $cliente);

    }
}
