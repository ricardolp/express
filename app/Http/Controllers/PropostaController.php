<?php

namespace App\Http\Controllers;

use App\TipoCobranca;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use spec\Prophecy\Promise\RequiredArgumentException;

class PropostaController extends Controller
{
    public function create($cliente)
    {
        $cliente = \App\Cliente::find($cliente);
        $tipos_proposta = ['1' => 'Contrato', '2'=>'Serviço Eventual'];
        $servicos = \App\OperacaoFromAberta::all();
        $cobrancas = \App\TipoCobranca::all();

        return view('admin.proposta.create')->with(compact('cliente', 'tipos_proposta', 'servicos', 'cobrancas'));
    }

    public function store(Request $request)
    {

        $data = $request->all();

        $data['path_url'] = md5(microtime()).$data['cliente_id'];
        $data['is_accept'] = 1;

        $proposta = \App\Proposta::create($data);

        for($i=0; $i < count($data['operacao']) ; $i++) {
            $z = $data['operacao'][$i];            
            $temp['proposta_id'] = $proposta->id;
            $temp['tipo_cobranca_id'] = $data[$z.'-operacao'];
            $temp['valor'] = $data[$z.'_valor'];
            $temp['valor_minimo'] = (isset($data[$z.'-ValorMinimo'])) ? $data[$z.'-ValorMinimo'] : null ;
            $temp['valor_maximo'] = (isset($data[$z.'-ValorMaximo'])) ? $data[$z.'-ValorMaximo'] : null ;
            $temp['operacao_aberta_id'] = $z;
            $temp['is_escalonado'] = (isset($data[$z.'_escalonadoView'])) ? true : false ;
        
            $operacao = \App\TipoCobranca::find( $data[$z.'-operacao']);
            
           
            $item_proposta = \App\ItemProposta::create($temp);

            if(isset($data[$z.'_escalonado'])){
                echo('entrou 1 loop');
                echo $data[$z.'_escalonado'];
                echo '['.substr($data[$z.'_escalonado'],0,-1).']';
                
                
                $arr = '['.substr($data[$z.'_escalonado'],0,-1).']';
                $arr = str_replace("'", '"', $arr);
                //$arr = str_replace(",", '.', $arr);
                
                                
                $arr = (array) json_decode($arr);
                echo "<hr>";
            
                var_dump($arr);
                
                
                foreach ($arr as $item) {
                    echo('entrou 2 loop');
                    
                    $arr_escalonado['valor'] = (string) $item->valor;
                    $arr_escalonado['valor_minimo'] = (string)  $item->valorInicial;
                    $arr_escalonado['valor_maximo']= (string) $item->valorFinal;
                    $arr_escalonado['id_item_proposta']= $item_proposta->id;
                    echo "<hr>";
                    var_dump($arr_escalonado);
                    //die;
                    \App\EscalonadoItemProposta::create($arr_escalonado);
                    unset($arr_escalonado);
                }
            }

            unset($temp);
        }        
        return redirect()->route('cliente.view', $proposta->cliente_id);

    }

    public function view($id)
    {
        $proposta = \App\Proposta::find($id);
        $itens = \App\ItemProposta::join('tipo_cobrancas', 'tipo_cobrancas.id','=','item_propostas.tipo_cobranca_id')
            ->join('operacao_from_abertas', 'operacao_from_abertas.id', '=', 'item_propostas.operacao_aberta_id')
            ->select('item_propostas.*', 'tipo_cobrancas.descricao', 'operacao_from_abertas.descricao as operacao_aberta')
            ->where('proposta_id', $proposta->id)
            ->get();

        $html = '<table class="table no-margin">
                    <tbody>
                        <tr>
                            <td class="col-md-3">Status da Proposta:</td>
                            <td>'.$proposta->status_proposta.'</td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Tipo da Proposta :</td>
                            <td>'.$proposta->tipo_proposta.'</td>
                        </tr>
                        <tr>
                            <td class="col-md-3">Data da Proposta:</td>
                            <td>'.$proposta->created_at.'</td>
                        </tr>
                    </tbody>
                    </table>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th width="10px">#</th>
                                <th>Operação</th>
                                <th>Tipo de Cobrança</th>
                                <th>Valor</th>
                                <th>Valor Mínimo</th>
                                <th>Valor Máximo</th>
                            </tr>
                        </thead>
                        <tbody>';



               foreach ($itens as $iten) {

                   if($iten->is_escalonado == 0){
                       $html .='<tr>
                                    <td style="text-align:center;"><span class="label label-primary"><i class="fa fa-check"></i></span></td>
                                    <td>'.$iten->operacao_aberta.'</td>
                                    <td>'.$iten->descricao.'</td>
                                    <td>'.$iten->valor.'</td>
                                    <td>'.$iten->valor_minimo.'</td>
                                    <td>'.$iten->valor_maximo.'</td>
                                </tr>';


                   } else {
                       $html .='<tr>
                                    <td style="text-align:center;"><span class="label label-primary"><i class="fa fa-check"></i></span></td>
                                    <td>'.$iten->operacao_aberta.'</td>
                                    <td>'.$iten->descricao.'</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>';

                       foreach ($this->escalonado($iten->id)['escalonados'] as $escalonados) {

                            $valor = $escalonados->valor;
                            $valor_min = $escalonados->valor_minimo;
                            $valor_max = $escalonados->valor_maximo;
                           $html .='<tr>
                                        <td style="text-align:center;"></td>
                                        <td></td>
                                        <td></td>
                                        <td>'.$valor.'</td>
                                        <td>'.$valor_min.'</td>
                                        <td>'.$valor_max.'</td>
                                    </tr>';
                       }

                   }
               }


        $html .='<tr>
                    <td style="text-align:center;" colspan="6">
                        Observações
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        '.$proposta->obs.'           
                    </td>
	            </tr>
                </tbody>
                 </table>';

        return ['success' => true, 'html' => $html];
    }
    

    public function send(Request $request)
    {
        $data = $request->all();

        $token = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10/strlen($x)) )),1,10);

        $proposta = \App\Proposta::find($data['id']);
        $proposta->contato_id = $data['contato_id'];
        $proposta->is_accept = 2;
        $proposta->path_url = URL::to('/cliente/proposta/'.md5($token));
        $proposta->save();

        $contato = \App\Contato::find($data['contato_id']);
        $email = $contato->email;

        Mail::send('emails.send', [ 'nome' => $contato->nome, 'link' => $proposta->path_url], function ($message) use ($email){
            $message->from('express@twscomex.com.br', 'TWS Comex');
            $message->to($email);
        });


        return redirect()->route('cliente.view', $proposta->cliente_id);
    }

    public function escalonado($id)
    {
        $escalonados = \App\EscalonadoItemProposta::where('id_item_proposta', $id)->get();

        return compact('escalonados');
    }

    public function show($token)
    {
        $proposta = \App\Proposta::where(['path_url' => $token, 'is_accept' => 2])->get();

        if(!$proposta->count() > 0) return view('admin.proposta.vencida');

        $proposta = \App\Proposta::join('clientes', 'clientes.id', '=', 'propostas.cliente_id')
            ->join('contatos', 'contatos.id', '=', 'propostas.contato_id')
            ->join('enderecos', 'clientes.id', '=', 'enderecos.cliente_id')
            ->select('propostas.*', 'clientes.*', 'contatos.*', 'propostas.id as proposta_id', 'enderecos.*')
            ->get();

        $proposta = $proposta->first();

        $items = \App\ItemProposta::join('tipo_cobrancas', 'tipo_cobrancas.id','=','item_propostas.tipo_cobranca_id')
            ->join('operacao_from_abertas', 'operacao_from_abertas.id', '=', 'item_propostas.operacao_aberta_id')
            ->select('item_propostas.*', 'tipo_cobrancas.descricao', 'operacao_from_abertas.descricao as operacao_aberta')
            ->where('proposta_id', $proposta->proposta_id)
            ->get();

        $controller = new \App\Http\Controllers\PropostaController();

        return view('admin.proposta.view')->with(compact('proposta', 'items', 'controller'));
    }

    public function submit(Request $request)
    {
        $data = $request->all();

        foreach ($data as $key => $item) {
            if($key != 'proposta_id'){
                $item = \App\ItemProposta::find($key);
                $item->is_accept = 1;
                $item->save();
            }
        }

        $proposta = \App\Proposta::find($data['proposta_id']);
        if($proposta){
            $proposta->is_accept = 3;
            $proposta->save();
        }

        $cliente = \App\Cliente::find($proposta->cliente_id);

        $contato = \App\Contato::find($proposta->contato_id);

        Mail::send('emails.proposta.return', ['cliente' => $cliente->razao_social, 'contato' => $contato->nome], function ($message) use ($cliente){
            $message->from('express@twscomex.com.br', 'Retorno de Proposta Express');
            $message->to('comercial@twscomex.com.br');
        });

        //return view('admin.proposta.enviada');
        header("location: http://www.twscomex.com.br");
    }
}
