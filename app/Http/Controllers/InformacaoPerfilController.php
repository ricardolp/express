<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class InformacaoPerfilController extends Controller
{
    public function create($cliente_id)
    {
        $cliente = \App\Cliente::find($cliente_id);
        return view('admin.perfil.create')->with(compact('cliente', 'cliente_id'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_ncm']                     = (isset($data['is_ncm'])) ? 1 : 0;
        $data['is_licenca_importacao']      = (isset($data['is_licenca_importacao'])) ? 1 : 0;
        $data['is_icms']                    = (isset($data['is_icms'])) ? 1 : 0;
        $data['is_imposto_federal']         = (isset($data['is_imposto_federal'])) ? 1 : 0;
        $data['is_debito']                  = (isset($data['is_debito'])) ? 1 : 0;
        $data['is_debito_afrmm']            = (isset($data['is_debito_afrmm'])) ? 1 : 0;
        $data['is_logistica']               = (isset($data['is_logistica'])) ? 1 : 0;

        $perfil = \App\InformacaoPerfil::create($data);

        return redirect()->route('cliente.view', $perfil->cliente_id)->with('success', 'O Perfil foi Adicionado');
    }

    public function edit($id)
    {                
        $perfil = \App\InformacaoPerfil::find($id);
                
        //if($perfil->count() > 0){
        //    $perfil = $perfil->last();
        //}
        
        $cliente = \App\Cliente::find($perfil->cliente_id);

        return view('admin.perfil.edit')->with(compact('cliente', 'perfil'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['is_ncm']                     = (isset($data['is_ncm'])) ? 1 : 0;
        $data['is_licenca_importacao']      = (isset($data['is_licenca_importacao'])) ? 1 : 0;
        $data['is_icms']                    = (isset($data['is_icms'])) ? 1 : 0;
        $data['is_imposto_federal']         = (isset($data['is_imposto_federal'])) ? 1 : 0;
        $data['is_debito']                  = (isset($data['is_debito'])) ? 1 : 0;
        $data['is_debito_afrmm']            = (isset($data['is_debito_afrmm'])) ? 1 : 0;
        $data['is_logistica']               = (isset($data['is_logistica'])) ? 1 : 0;
        $data['is_seguro']                  = (isset($data['is_seguro'])) ? 1 : 0;

        $perfil = \App\InformacaoPerfil::find($id);
        $perfil->update($data);

        return redirect()->route('cliente.view', $perfil->cliente_id)->with('success', 'O Perfil foi Adicionado');
    }
}
