<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class OperacaoController extends Controller
{
    public function create($cliente)
    {
        $cliente = \App\Cliente::find($cliente);
        $tipo_servico = \App\TipoServico::lista();
        return view('admin.operacao.create')->with(compact('cliente', 'tipo_servico'));
    }

    public function edit($id)
    {

        $operacao = \App\Operacao::find($id);
        $cliente = \App\Cliente::find($operacao->cliente_id);
        $tipo_servico = \App\TipoServico::lista();
        $find = \App\OperacaoIncoterm::where('operacao_id', $id)->get();

        $incoterm = '[';

        $i = 1;
        foreach ($find as $incoterms) {
            if($i == $find->count()){
                $incoterm .= $incoterms->incoterm_id;
            } else {
                $incoterm .= $incoterms->incoterm_id . ',';
            }
            $i++;
        }

        $incoterm .= ']';

        $all = \App\Incoterm::all();
        return view('admin.operacao.edit')->with(compact('cliente', 'tipo_servico', 'operacao','incoterm', 'all'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_before']      = (isset($data['is_before'])) ? 1 : 0;
        $data['is_know']        = (isset($data['is_know'])) ? 1 : 0;
        $data['is_maritmo']     = (isset($data['is_maritmo'])) ? 1 : 0;
        $data['is_rodoviario']  = (isset($data['is_rodoviario'])) ? 1 : 0;
        $data['is_aereo']       = (isset($data['is_aereo'])) ? 1 : 0;
        $data['is_radar']       = (isset($data['is_radar'])) ? 1 : 0;

        $operacao = \App\Operacao::create($data);

        if($request->has('incoterms')){
            foreach ($data['incoterms'] as $incoterm) {
                $op_incoterm = new \App\OperacaoIncoterm();
                $op_incoterm->incoterm_id = $incoterm;
                $op_incoterm->operacao_id = $operacao->id;
                $op_incoterm->save();
            }
        }

        return redirect()->route('cliente.view', $operacao->cliente_id)->with('success', 'Operação cadastradao com sucesso');
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['is_before']      = (isset($data['is_before'])) ? 1 : 0;
        $data['is_know']        = (isset($data['is_know'])) ? 1 : 0;
        $data['is_maritmo']     = (isset($data['is_maritmo'])) ? 1 : 0;
        $data['is_rodoviario']  = (isset($data['is_rodoviario'])) ? 1 : 0;
        $data['is_aereo']       = (isset($data['is_aereo'])) ? 1 : 0;
        $data['is_radar']       = (isset($data['is_radar'])) ? 1 : 0;

        $operacao = \App\Operacao::find($id);
        $operacao->update($data);

        $incoterms = \App\OperacaoIncoterm::where('operacao_id', $operacao->id)->count();


        if($incoterms > 0)
            \App\OperacaoIncoterm::where('operacao_id', $operacao->id)->delete();

        if($request->has('incoterms')){
            foreach ($data['incoterms'] as $incoterm) {
                $op_incoterm = new \App\OperacaoIncoterm();
                $op_incoterm->incoterm_id = $incoterm;
                $op_incoterm->operacao_id = $operacao->id;
                $op_incoterm->save();
            }
        }

        return redirect()->route('cliente.view', $operacao->cliente_id)->with('success', 'Operação foi atualizada com sucesso');
    }

    public function destroy($id)
    {
        $incoterms = \App\OperacaoIncoterm::where('operacao_id', $id)->count();

        if($incoterms > 0)
            \App\OperacaoIncoterm::where('operacao_id', $id)->delete();

        $operacao = \App\Operacao::find($id);

        if($operacao)
            $operacao->delete();


        return redirect()->route('cliente.view', $operacao->cliente_id)->with('success', 'Operação foi deletada com sucesso');

    }

    public function show($id)
    {
        return \App\Operacao::find($id);
    }

}
