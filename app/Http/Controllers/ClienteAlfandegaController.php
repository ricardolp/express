<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ClienteAlfandegaController extends Controller
{
    public function create($cliente)
    {
        $cliente = \App\Cliente::find($cliente);
        $alfandegas = \App\AlfandegaFronteira::lista();

        return view('admin.alfandegacliente.create')->with(compact('cliente', 'alfandegas'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $data['is_visita']      = (isset($data['is_visita'])) ? 1 : 0;
        $data['is_acordo']      = (isset($data['is_acordo'])) ? 1 : 0;

        $alfandega = \App\ClienteAlfandega::create($data);

        return redirect()->route('cliente.view', $alfandega->cliente_id)->with('success', 'A Alfândega/Fronteira foi adicionada');
    }

    public function edit($id)
    {
        $data = \App\ClienteAlfandega::find($id);
        $cliente = \App\Cliente::find($data->id);
        $alfandegas = \App\AlfandegaFronteira::lista();

        return view('admin.alfandegacliente.edit')->with(compact('cliente', 'alfandegas', 'data'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $data['is_visita']      = (isset($data['is_visita'])) ? 1 : 0;
        $data['is_acordo']      = (isset($data['is_acordo'])) ? 1 : 0;

        $alfandega = \App\ClienteAlfandega::find($id);

        if($alfandega)
            $alfandega->update($data);

        return redirect()->route('cliente.view', $alfandega->cliente_id)->with('success', 'A Alfândega/Fronteira foi atualizada');
    }

    public function destroy($id)
    {
        $alfandega = \App\ClienteAlfandega::find($id);

        if($alfandega)
            $alfandega->delete();

        return redirect()->route('cliente.view', $alfandega->cliente_id)->with('success', 'A Alfândega/Fronteira foi deletada');

    }
}
