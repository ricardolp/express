<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoCobranca extends Model
{
    protected $fillable = [
        'descricao',
        'is_value'
    ];
}
