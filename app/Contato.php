<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{

    protected $fillable = [
        'nome',
        'telefone',
        'celular',
        'fax',
        'aniversario',
        'email',
        'observacao',
        'is_noticia',
        'is_followup',
        'is_kpi',
        'is_direct',
        'data_lembranca',
        'descricao_lembranca',
        'empresa_anterior',
        'hobby',
        'politica',
        'religiao',
        'time_futebol',
        'tempo_empresa',
        'status',
        'departamento_id',
        'funcao_id',
        'cliente_id'
    ];

    public function getDataLembrancaAttribute($date)
    {
        if(strpos('000', $date) || $date == null){
            return null;
        }

        $date = Carbon::createFromFormat('Y-m-d', $date);
        $formated = $date->format('d/m/Y');

        return ($formated != '30/11/-0001') ? $formated : '' ;
    }
}
