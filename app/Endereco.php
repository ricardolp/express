<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'endereco',
        'cep',
        'bairro',
        'cidade',
        'uf',
        'pais',
        'tipo_endereco',
        'complemento',
        'num_endereco',
        'cliente_id'
    ];
}
