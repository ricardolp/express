<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCredenciamento extends Model
{
    protected $fillable = [
        'descricao',
        'descricao_direct',
        'is_file'
    ];

    public static function lista()
    {
        $arr = [];
        $itens = \App\ItemCredenciamento::all();

        $arr[0] = 'Todos';
        foreach ($itens as $item) {
            $arr[$item->id] = $item->descricao;
        }

        return $arr;
    }

    public static function listaDesc()
    {
        $arr = [];
        $itens = \App\ItemCredenciamento::all();

        $arr['0'] = 'Todos';
        foreach ($itens as $item) {
            $arr[$item->descricao] = $item->descricao;
        }

        return $arr;
    }
}
