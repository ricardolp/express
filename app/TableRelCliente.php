<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TableRelCliente extends Model
{
    protected $fillable = [
        'user_id',
        'column_0',
        'column_1',
        'column_2',
        'column_3',
        'column_4'
    ];
}
