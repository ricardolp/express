<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcao extends Model
{
    protected $table = 'funcao';

    protected $fillable = [
        'descricao',
        'is_obrigatorio',
        'is_followup',
        'is_kpi',
        'is_direct',
        'is_noticia',
    ];

    
     public static function lista()
    {
        $arr = [];
        $itens = \App\Funcao::all();

        foreach ($itens as $item) {
            $arr[$item->id] = $item->descricao;
        }

        return $arr;
    }
    
    
}
