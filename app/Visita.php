<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Visita extends Model
{
    protected $fillable = [
        'local',
        'data',
        'hora',
        'duracao',
        'is_comercial',
        'is_financeiro',
        'is_logistico',
        'is_suporte',
        'pauta',
        'is_operacional',
        'justificativa',
        'status',
        'responsavel',
        'cliente_id'
    ];

    public function getDataAttribute($data)
    {
        if($data != null && $data != ''){
            $data = Carbon::createFromFormat('Y-m-d', $data);
            return $data->format('d/m/Y');
        } else {
            return '';
        }

    }

    public function getStatusVisitaAttribute()
    {
            switch($this->status){
                case 1:
                    return (strtotime ( $this->data ) > strtotime ( date('Y-m-d') )) ? "<span class=\"label label-success\">Agendada</span>" : "<span class=\"label label-success\">Agendada</span>";
                    break;
                case 2:
                    return "<span class=\"label label-primary\">Confirmada</span>";
                    break;
                case 3:
                    return "<span class=\"label label-danger\">Cancelada</span>";
                    break;
                case 4:
                    return "<span class=\"label label-warning\">Reagendada</span>";
                    break;
                case 5:
                    return "<span class=\"label label-info\">Atrasada</span>";
                    break;
                case 6:
                    return "<span class=\"label label-info\">Concluída</span>";
                    break;
                case 2:
                    return "<span class=\"label label-primary\">Confirmada</span>";
                    break;
            }
    }



}
