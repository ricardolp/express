<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $dates = ['ultima_importacao', 'ultima_exportacao', 'data_ativo', 'data_inativo', 'fundacao'];

    protected $fillable = [
        'aberta_id',
        'matriz_id',
        'razao_social',
        'nome_fantasia',
        'cnpj',
        'inscricao_municipal',
        'inscricao_estadual',
        'telefone',
        'fax',
        'CNAE',
        'fundacao',
        'data_ativo',
        'data_inativo',
        'ultima_importacao',
        'ultima_exportacao',
        'frequencia_visita',
        'responsavel_visita',
        'atividade',
        'limite_radar',
        'tipo_indicacao',
        'indicado_to',
        'is_radar',
        'is_filial',
        'is_aviso_dia',
        'is_aviso_quinzena',
        'is_aviso_semana',
        'is_aviso_mes',
        'is_status'
    ];

    public function getUltimaImportacaoAttribute($date)
    {
        if(strpos('000', $date) || $date == null){
            return null;
        }

        $date = Carbon::createFromFormat('Y-m-d', $date);
        return $date->format('d/m/Y');
    }

    public function getUltimaExportacaoAttribute($date)
    {
        if(strpos('000', $date) || $date == null){
            return null;
        }

        $date = Carbon::createFromFormat('Y-m-d', $date);
        return $date->format('d/m/Y');
    }

    public function getFundacaoAttribute($date)
    {
        if(strpos('000', $date) || $date == null){
            return null;
        }

        $date = Carbon::createFromFormat('Y-m-d', $date);
        $formated = $date->format('d/m/Y');

        return ($formated != '30/11/-0001') ? $formated : '' ;
    }

    public static function lista()
    {
        $arr = [];
        $itens = \App\Cliente::all();

        foreach ($itens as $item) {
            $arr[$item->id] = $item->razao_social;
        }

        return $arr;
    }

    public static function listaFiltro()
    {
        $arr = [];
        $itens = \App\Cliente::all();

        $arr['0'] = "Todos";
        foreach ($itens as $item) {
            $arr[$item->razao_social] = $item->razao_social;
        }

        return $arr;
    }

    public function getStatusDescAttribute()
    {
        switch ($this->is_status){
            case 1;
                return 'Prospect';
                break;
            case 2;
                return 'Ativo';
                break;
            case 3;
                return 'Inativo';
                break;
        }


    }
}
