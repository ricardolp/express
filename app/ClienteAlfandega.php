<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteAlfandega extends Model
{
    protected $fillable = [
        'is_visita',
        'is_acordo',
        'contato',
        'alfandega_fronteira_id',
        'cliente_id'
    ];

    public function alfandega($id)
    {
        $alfandega = \App\AlfandegaFronteira::find($id);

        if($alfandega)
            return $alfandega->descricao;

        return '';
    }


}
