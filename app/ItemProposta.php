<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemProposta extends Model
{
    protected $fillable = [
        'valor',
        'operacao_aberta_id',
        'is_accept',
        'valor_minimo',
        'valor_maximo',
        'is_escalonado',
        'tipo_cobranca_id',
        'proposta_id'
    ];
}
