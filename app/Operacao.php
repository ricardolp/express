<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operacao extends Model
{
    protected $fillable = [
        'tipo_operacao',
        'previsao',
        'finalidade',
        'origem',
        'tempo_importacao',
        'fornecedor',
        'expectativa',
        'dificuldade',
        'comentario',
        'is_maritmo',
        'is_aereo',
        'is_rodoviario',
        'is_radar',
        'is_before',
        'is_know',
        'cliente_id',
        'tipo_servico_id'

    ];
}
