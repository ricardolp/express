<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContatoParceiro extends Model
{
    protected $fillable = [
        'nome',
        'aniversario',
        'email',
        'observacao',
        'parceiro_id',
        'departamento_id',
        'funcao_id'
    ];
}
