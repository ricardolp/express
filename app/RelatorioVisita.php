<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelatorioVisita extends Model
{
    protected $fillable = [
        'servico',
        'is_problema',
        'is_dificuldade',
        'avaliacao',
        'cliente_id',
        'data_relatorio',
        'visita_id',
        'obs_dificuldade',
        'obs_problema'
    ];
}
