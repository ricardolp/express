<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>

    <!-- Define Charset -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- Responsive Meta Tag -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />

    <link href="http://fonts.googleapis.com/css?family=Questrial" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Oxygen:400,300,700" rel="stylesheet" type="text/css">

    <title>TWS | Express</title><!-- Responsive Styles and Valid Styles -->

    <style type="text/css">

        body{
            width: 100%;
            background-color: #eeeeee;
            margin:0;
            padding:0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt:0px; mso-margin-bottom-alt:0px; mso-padding-alt: 0px 0px 0px 0px;
        }

        p,h1,h2,h3,h4{
            margin-top:0;
            margin-bottom:0;
            padding-top:0;
            padding-bottom:0;
        }

        span.preheader{display: none; font-size: 1px;}

        html{
            width: 100%;
        }

        table{
            font-size: 14px;
            border: 0;
        }

        /* ----------- responsivity ----------- */
        @media only screen and (max-width: 640px){
            /*------ top header ------ */
            body[mail] .show{display: block !important;}
            body[mail] .hide{display: none !important;}

            /*----- main image -------*/
            body[mail] .main-image img{width: 440px !important; height: auto !important;}

            /* ====== divider ====== */
            body[mail] .divider img{width: 440px !important;}

            /*--------- banner ----------*/
            body[mail] .banner img{width: 440px !important; height: auto !important;}
            /*-------- container --------*/
            body[mail] .container590{width: 440px !important;}
            body[mail] .container580{width: 400px !important;}
            body[mail] .container1{width: 420px !important;}
            body[mail] .container2{width: 400px !important;}
            body[mail] .container3{width: 380px !important;}

            /*-------- secions ----------*/
            body[mail] .section-item{width: 440px !important;}
            body[mail] .section-img img{width: 440px !important; height: auto !important;}
        }

        @media only screen and (max-width: 479px){
            /*------ top header ------ */
            body[mail] .main-header{font-size: 24px !important;}
            body[mail] .resize-text{font-size: 14px !important;}

            /*----- main image -------*/
            body[mail] .main-image img{width: 280px !important; height: auto !important;}

            /* ====== divider ====== */
            body[mail] .divider img{width: 280px !important;}
            body[mail] .align-center{text-align: center !important;}


            /*-------- container --------*/
            body[yahoo] .container590{width: 280px !important;}
            body[yahoo] .container580{width: 240px !important;}
            body[yahoo] .section-img img{width: 280px !important; height: auto !important;}

            /*------- CTA -------------*/
            body[yahoo] .cta-button{width: 200px !important;}
            body[yahoo] .cta-text{font-size: 16px !important;}
        }

    </style>
</head>

<body mail="fix" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!-- ======= main section ======= -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="eeeeee" style="background-image: url(img/bg.png); background-size: 100% 100%; background-position: top center;" background="http://themastermail.com/alerta/notif14/img/bg.png">

    <tr><td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td></tr>

    <tr>
        <!-- ======= logo ======= -->
        <td align="center">
            <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img width="99" border="0" style="display: block; width: 99px;" src="http://i.imgur.com/AgcvGtQ.png?1" alt="logo" /></a>
        </td>
    </tr>

    <tr><td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td></tr>

    <tr>
        <td>
            <table border="0" align="center" width="510" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="container590 bodybg_color" style="border-radius: 3px;">

                <tr><td height="60" style="font-size: 60px; line-height: 60px;">&nbsp;</td></tr>

                <tr>
                    <td align="center" style="color: #29353b; font-size: 24px; font-family: \'Questrial\', sans-serif; mso-line-height-rule: exactly; line-height: 30px;" class="title_color main-header">

                        <!-- ======= section header ======= -->

                        <div style="line-height: 30px;">

                            {{ $nome }}

                        </div>
                    </td>
                </tr>

                <tr><td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>

                <tr>
                    <td>
                        <table border="0" width="440" align="center" cellpadding="0" cellspacing="0" class="container580">
                            <tr>
                                <td align="center" style="color: #727e84; font-size: 14px; font-family: \'Questrial\', sans-serif; mso-line-height-rule: exactly; line-height: 24px;" class="resize-text text_color">
                                    <div style="line-height: 24px">
                                        {texto_descritivo}
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr><td height="35" style="font-size: 35px; line-height: 35px;">&nbsp;</td></tr>

                <tr>
                    <td align="center" style="background-image: url(http://i.imgur.com/rVQAlaC.png?1); background-size: 100% 100%; background-position: top center;" background="http://themastermail.com/alerta/notif14/img/divider-bg.png">

                        <table border="0" align="center" width="220" cellpadding="0" cellspacing="0" bgcolor="3C8DBC" style="border-radius: 3px;" class="cta-button main_color">

                            <tr><td height="17" style="font-size: 17px; line-height: 17px;">&nbsp;</td></tr>

                            <tr>

                                <td align="center" style="color: #ffffff; font-size: 16px; font-family: \'Questrial\', sans-serif;" class="cta-text">
                                    <!-- ======= main section button ======= -->


                                    <div style="line-height: 24px;">
                                        <a href="{{ $link }}" style="color: #ffffff; text-decoration: none;">Link da Proposta</a>
                                    </div>

                                </td>

                            </tr>

                            <tr><td height="17" style="font-size: 17px; line-height: 17px;">&nbsp;</td></tr>

                        </table>
                    </td>
                </tr>

                <tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>

                <tr>
                    <!-- ======= image ======= -->
                    <td align="center" class="section-img">
                        <a href="#" style="display: block; border-style: none !important; border: 0 !important;"><img width="420" border="0" style="display: block; width: 420px;" src="http://i.imgur.com/hTAzum1.png?1" alt="logo" /></a>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr><td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td></tr>

    <tr>
        <td>
            <table border="0" align="center" width="510" cellpadding="0" cellspacing="0" class="container590 bodybg_color">
                <tr>
                    <td>
                        <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">

                            <tr>
                                <td align="center" class="copyright" style="color: #8e8e8e; font-size: 14px; font-family: \'Questrial\', sans-serif; line-height: 22px;">
                                    <div style=" line-height: 22px;">

                                        © 2015 TWS Comex. Todos os Direitos Reservados.

                                    </div>
                                </td>
                            </tr>

                        </table>

                        <table border="0" align="left" width="5" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="container590">
                            <tr><td height="20" width="5" style="font-size: 20px; line-height: 20px;">&nbsp;</td></tr>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr><td height="50" style="font-size: 50px; line-height: 50px;">&nbsp;</td></tr>

</table>

</body>
</html>