<!-- BEGIN EXPERIENCE TEMPLATES -->
<script type="text/html" id="experienceTmpl">
    <li class="clearfix">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" >
                    <select id="select13" name="participantes[]" class="form-control select2-list<%=index%>" id="itemoperacao-<%=index%>">
                    <optgroup label="Usuários TWS">
                        @foreach($users as $user)
                                <option value="{{ $user->name }}">{{ $user->name }}</option>
                        @endforeach
                    <optgroup label="Contatos do Cliente">
                        @foreach($contatos as $contato)
                            <option value="{{ $contato->nome }}">{{ $contato->nome }}</option>
                        @endforeach
                    </select>
                    <label for="participante-<%=index%>">Participante <%=index%></label>
                </div>
            </div>
        </div>
    </li>
</script>
