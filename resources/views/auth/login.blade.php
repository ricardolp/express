<!DOCTYPE html>
<html lang="en">
<head>
    <title>TWS EXPRESS | Login</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link
            href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900'
            rel='stylesheet' type='text/css' />
    <link type="text/css" rel="stylesheet"
          href="{{ asset('assets/css/theme-default/bootstrap.css') }}" />
    <link type="text/css" rel="stylesheet"
          href="{{ asset('assets/css/theme-default/materialadmin.css') }}" />
    <link type="text/css" rel="stylesheet"
          href="{{ asset('assets/css/theme-default/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet"
          href="{{ asset('assets/css/theme-default/material-design-iconic-font.min.css?1421434286') }}" />

    <link type="text/css" rel="stylesheet"
          href="{{ asset('assets/css/theme-default/libs/sweetalert/sweetalert2.css') }}" />
    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="{{ asset('ssets/js/libs/utils/html5shiv.js?1403934957') }}"></script>
    <script type="text/javascript" src="{{ asset('ssets/js/libs/utils/respond.min.js?1403934956') }}"></script>
    <![endif]-->
</head>
<body class="menubar-hoverable header-fixed ">
<style>
    body{
        background-image: url("assets/img/bg_html.jpg");
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
    }
</style>

<!-- BEGIN LOCKED SECTION -->
<section class="section-account">
    <div class="spacer"></div>
    <div class="card contain-xs style-transparent" style="background-color: rgba(98, 98, 98, 0.22);border-radius:15px;">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12" >
                    <h2 style="text-align: center">TWS EXPRESS </h2>
                    <form class="form floating-label" id="form-login" action="{{ route('express.login') }}" accept-charset="utf-8" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email"> <label for="email">Login</label>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password"> <label for="password">Senha</label>
                            <p class="help-block">
                                <a href="#">Esqueceu?</a>
                            </p>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-xs-7 text-right">
                                <button class="btn btn-primary btn-raised" type="submit">Login</button>
                            </div>
                            <!--end .col -->
                        </div>
                        <!--end .row -->
                    </form>
                </div>
                <!--end .col -->
            </div>
            <!--end .row -->
        </div>
        <!--end .card-body -->
    </div>
    <!--end .card -->
</section>
<!-- END LOCKED SECTION -->

<!-- BEGIN JAVASCRIPT -->
<script src="{{ ('assets/js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
<script src="{{ ('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ ('assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ ('assets/js/libs/spin.js/spin.min.js') }}"></script>
<script src="{{ ('assets/js/libs/autosize/jquery.autosize.min.js') }}"></script>
<script src="{{ ('assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ ('assets/js/libs/sweetalert/sweetalert2.min.js') }}"></script>
<script src="{{ ('assets/js/core/source/App.js') }}"></script>
<script src="{{ ('assets/js/core/source/AppNavigation.js') }}"></script>
<script src="{{ ('assets/js/core/source/AppOffcanvas.js') }}"></script>
<script src="{{ ('assets/js/core/source/AppCard.js') }}"></script>
<script src="{{ ('assets/js/core/source/AppForm.js') }}"></script>
<script src="{{ ('assets/js/core/source/AppNavSearch.js') }}"></script>
<script src="{{ ('assets/js/core/source/AppVendor.js') }}"></script>
<script src="{{ ('assets/js/core/demo/Demo.js') }}"></script>
<script src="{{ ('assets/js/atividades.js') }}"></script>
<!-- END JAVASCRIPT -->

</body>
</html>