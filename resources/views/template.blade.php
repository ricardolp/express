<!DOCTYPE html>
<html lang="en">
<head>
    <title> TWS - Express</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/bootstrap.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/materialadmin.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/material-design-iconic-font.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/rickshaw/rickshaw.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/morris/morris.core.css') }}" />
    <!-- END STYLESHEETS -->
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/select2/select2.css') }}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/DataTables/jquery.dataTables.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css') }}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-default/libs/fullcalendar/fullcalendar.css') }}" />


    <!-- BEGIN JAVASCRIPT -->
    <script src="{{ asset('/assets/js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->
</head>
<body class="menubar-hoverable header-fixed ">

<!-- BEGIN HEADER-->
<header id="header" >
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="headerbar-right">
            <ul class="header-nav header-nav-profile">
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                        <img src="http://twscomex.com.br/img/logo.png" alt="" />
                        <span class="profile-info">
                            <small>Opções</small>
                        </span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        <li class="dropdown-header">Config</li>
                        <li class="divider"></li>
                        <li><a href="../../html/pages/login.html"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
            </ul><!--end .header-nav-profile -->
        </div><!--end #header-navbar-collapse -->
    </div>
</header>
<!-- END HEADER-->

<!-- BEGIN BASE-->
<div id="base">
    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
          @yield('content')
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->

    @include('menu')

</div><!--end #base-->
<!-- END BASE -->

<script src="{{ asset('/assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/spin.js/spin.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/autosize/jquery.autosize.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/flot/jquery.flot.orderBars.js') }}"></script>
<script src="{{ asset('/assets/js/libs/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('/assets/js/libs/flot/curvedLines.js') }}"></script>
<script src="{{ asset('/assets/js/libs/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/moment/moment.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('/assets/js/libs/select2/select2.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/DataTables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/microtemplating/microtemplating.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/d3/d3.min.js') }}"></script>
<script src="{{ asset('/assets/js/libs/d3/d3.v3.js') }}"></script>
<script src="{{ asset('/assets/js/libs/rickshaw/rickshaw.min.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/App.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/AppNavigation.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/AppOffcanvas.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/AppCard.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/AppForm.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/AppNavSearch.js') }}"></script>
<script src="{{ asset('/assets/js/core/source/AppVendor.js') }}"></script>
<script src="{{ asset('/assets/js/core/demo/Demo.js') }}"></script>
<script src="{{ asset('/assets/js/core/demo/DemoFormComponents.js') }}"></script>
<script src="{{ asset('/assets/js/core/demo/DemoPageContacts.js') }}"></script>
<script src="{{ asset('/js/express.js') }}"></script>
<script src="{{ asset('/assets/js/libs/fullcalendar/fullcalendar.min.js') }}"></script>

<script src="{{ asset('/js/service.js') }}"></script>

@yield('formicro')

<script type="text/javascript">
    $('#date').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy"});
    $('#date-impo').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy"});
    $('#date-expo').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy"});
    $('#date-fundacao').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy"});
    $('#date-importacao').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy"});
    $('#date-exportacao').datepicker({autoclose: true, todayHighlight: true, format: "dd/mm/yyyy"});

    $('#date-inline').datepicker({autoclose: true,todayHighlight: true, format: "dd/mm/yyyy"});


    $('#datatable-servico').DataTable({
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#datatable1').DataTable({
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#datatable2').DataTable({
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#datatable3').DataTable({
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#datatable4').DataTable({
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#datatable5').DataTable({
        "language": {
            "lengthMenu": '_MENU_ entries per page',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        }
    });

    $('#datatable1 tbody').on('click', 'tr', function() {
        $(this).toggleClass('selected');
    });

    $(".single").select2({
        placeholder: "Select a state",
        allowClear: true
    });

    $(".select2-list").select2({
        placeholder : 'Selecione',
        allowClear: true
    });
</script>

@yield('script')
</body>
</html>
