@extends('template')


@section('content')

<form id="form-questionario" action="{{ route('visita.submit.relatorio') }}" method="post">
    <input type="hidden" value="{{ $visita_id }}" name="visita_id">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    <table class="footable table table-stripped" data-page-size="50" data-filter=#filter>
                        <thead>
                        <tr>
                            <th class="col-md-3"></th>
                            <th class="col-md-1"></th>
                            <th class="col-md-3"></th>
                        </tr>
                        </thead>
                        
                        @foreach( $manage as $key => $row )
                        
                        <tbody>
                            <tr>
                                <td>Classifica os últimos  serviços de {{ $row->ITF_OPERACAO }} como: </td>
                                <td>
                                    <select class="form-control select2" name="{{ trim($row->ITF_OPERACAO) }}[avaliacao]" style="width: 100%;">
                                        <option selected="selected">Selecione...</option>
                                        <option value="Ruim">Ruim</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Bom">Bom</option>
                                        <option value="Ótimo">Ótimo</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr class="">
                                <td> Teve algum problema nos últimos serviços de  </td>
                                <td>
                                    <input type="radio" name="{{ trim($row->ITF_OPERACAO) }}[is_problema]" value="1">Sim
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    <input type="radio" name="{{ trim($row->ITF_OPERACAO) }}[is_problema]" value="0">Não
                                </td>
                                <td id="trp" >
                                    <input type="text" name="{{ trim($row->ITF_OPERACAO) }}[obs_problema]" placeholder="Observação.." class="form-control">
                                </td>
                            </tr>
                            <tr class="">
                                <td> Teve algum dificuldade ou dúvida nos últimos serviços de  </td>
                                <td>
                                    <input type="radio" name="{{ trim($row->ITF_OPERACAO) }}[is_dificuldade]" value="1">Sim
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    <input type="radio" name="{{ trim($row->ITF_OPERACAO) }}[is_dificuldade]" value="0">Não
                                </td>
                                <td id="trpdif">
                                    <input type="text" name="{{ trim($row->ITF_OPERACAO) }}[obs_dificuldade]" placeholder="Observação.." class="form-control">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <dib class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </dib>
</form>
@endsection