@extends('template')

@section('content')
    <br><br>
    {{ Form::model($cliente,['route' => ['cliente.update', $cliente->id], 'id'=>'form']) }}

    <div class="col-md-12">
        <div class="card card-bordered">
            <div class="card-head style-info">
                <div class="tools pull-left" style="margin-left:25px;">
                    <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('cliente.index') }}">
                        <span class="glyphicon glyphicon-arrow-left"></span>
                        &nbsp; Voltar para listagem
                    </a>
                </div><!--end .tools -->
            </div>
            <!-- BEGIN DEFAULT FORM ITEMS -->
            <div class="card-body style-default">
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.cliente.form.geral')
                    </div>
                </div><!--end .row -->
            </div><!--end .card-body -->
            <!-- END DEFAULT FORM ITEMS -->

            <!-- BEGIN FORM TABS -->
            <div class="card-head style-primary">
                <ul class="nav nav-tabs tabs-text-contrast tabs-accent" data-toggle="tabs">
                    <li class="active"><a href="#">Informações de Visita</a></li>
                </ul>
            </div><!--end .card-head -->
            <!-- END FORM TABS -->

            <!-- BEGIN FORM TAB PANES -->
            <div class="card-body tab-content">
                <div class="tab-pane active" id="contact">
                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.cliente.form.visita')
                        </div>
                    </div><!--end .row -->
                </div><!--end .tab-pane -->
            </div><!--end .card-body.tab-content -->
            <!-- END FORM TAB PANES -->


            <!-- BEGIN FORM FOOTER -->
            <div class="card-actionbar">
                <div class="card-actionbar-row">
                    <a class="btn btn-flat" href="#">Cancelar</a>
                    <button type="submit" class="btn btn-info">Salvar Cliente</button>
                </div><!--end .card-actionbar-row -->
            </div><!--end .card-actionbar -->
            <!-- END FORM FOOTER -->

        </div>
    </div>
    {{ Form::close() }}
@endsection

@section('script')
    <script>
        $("#tipo_indicacao").ready(function (){
            var val = $("#tipo_indicacao").val();

            if(val == 0) {
                $(".cliente-lista").css('display', 'block');
                $(".parceiro-lista").css('display', 'none');
                $(".equipe-lista").css('display', 'none');
            }

            if(val == 1) {
                $(".cliente-lista").css('display', 'none');
                $(".parceiro-lista").css('display', 'block');
                $(".equipe-lista").css('display', 'none');
            }

            if(val == 2) {
                $(".cliente-lista").css('display', 'none');
                $(".parceiro-lista").css('display', 'none');
                $(".equipe-lista").css('display', 'block');
            }

            if(val == 3) {
                $(".cliente-lista").css('display', 'none');
                $(".parceiro-lista").css('display', 'none');
                $(".equipe-lista").css('display', 'none');
            }
        })
    </script>
@endsection