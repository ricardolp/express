<div class="col-md-6">
    <div class="card card-underline">
        <div class="card-head">
            <header>Direct</header>
            <div class="tools">
                <div class="btn-group">
                    @if($direct->count() == 0)
                        <a href="{{ route('direct.create', $cliente->id) }}" class="btn btn-icon-toggle">
                            <i class="md md-add"></i>
                        </a>
                    @else
                        <a href="{{ route('direct.edit', $direct->id) }}" class="btn btn-icon-toggle">
                            <i class="md md-eit"></i>
                        </a>
                    @endif
                        <a class="btn btn-icon-toggle btn-collapse" id="toggle-direct"><i class="fa fa-angle-down"></i></a>

                </div>
            </div>

        </div><!--end .card-head -->
        <div class="card-body card-sub" style="display:none;">

            <div class="row">
                <div class="col-md-6">
                    <div class="card card-outlined style-info">
                        <div class="card-head text-center">
                            <header>Processo / Importação</header>
                        </div><!--end .card-head -->
                        <div class="card-body text-center" style="display:none;">
                            @if($direct->count() == 0)
                                <h3>N/A</h3>
                            @else
                                {{ $direct->processo_importacao }}
                            @endif
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div>
                <div class="col-md-6">
                    <div class="card card-outlined style-info">
                        <div class="card-head text-center">
                            <header>Data do Faturamento</header>
                        </div><!--end .card-head -->
                        <div class="card-body text-center" style="display:none;">
                            @if($direct->count() == 0)
                                <h3>N/A</h3>
                            @else
                                {{ $direct->data_importacao }}
                            @endif

                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card card-outlined style-info">
                        <div class="card-head text-center">
                            <header>Processo / Exportação</header>
                        </div><!--end .card-head -->
                        <div class="card-body text-center" style="display:none;">
                            @if($direct->count() == 0)
                                <h3>N/A</h3>
                            @else
                                {{ $direct->processo_exportacao }}
                            @endif
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div>
                <div class="col-md-6">
                    <div class="card card-outlined style-info">
                        <div class="card-head text-center">
                            <header>Data do Faturamento</header>
                        </div><!--end .card-head -->
                        <div class="card-body text-center" style="display:none;">
                            @if($direct->count() == 0)
                                <h3>N/A</h3>
                            @else
                                {{ $direct->data_exportacao }}
                            @endif
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </div>
            </div>

        </div><!--end .card-body -->
    </div><!--end .card -->
</div>