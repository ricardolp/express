<table id="datatable1" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Item</th>
        <th class="hidden-xs">Vencimento</th>
        <th class="col-sm-3">Ações</th>
    </tr>
    </thead>
    <tbody>
        @foreach($credenciamentos as $credenciamento)
            <tr>
                <td>{{ $credenciamento->item->descricao }}</td>
                <td>{{ $credenciamento->vencimento }}</td>
                <td>
                    <a href="{{ route('credenciamento.edit', $credenciamento->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <button data-id="{{ $credenciamento->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-primary credenciamento-show"><i class="fa fa-eye"></i></button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@include('admin.cliente.modal.credenciamento')