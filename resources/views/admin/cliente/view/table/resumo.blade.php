<table id="datatable-servico" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Operação</th>
        <th>Ano</th>
        <th>Mês</th>
        <th>Total</th>
        <th>Quantidade</th>
    </tr>
    </thead>
    <tbody>

        @if(!is_null($resumo_cliente))
            @foreach($resumo_cliente as $resumo)
                <tr>
                    <td>{{ $resumo['OPE_CODIGO'] }}</td>
                    <td>{{ $resumo['ANO'] }}</td>
                    <td>{{ $controller->getMonth($resumo['MES']) }}</td>
                    <td>{{ $resumo['VALOR'] }}</td>
                    <td>{{ $resumo['QUANTIDADE'] }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>