<table id="datatable3" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Data da Proposta</th>
            <th class="hidden-xs">Status</th>
            <th class="col-sm-3">Ações</th>
        </tr>
    </thead>
    <tbody>
        @foreach($propostas as $proposta)
            <tr>
                <th>{{ $proposta->created_at }}</th>
                <th>{!!  $proposta->status  !!}</th>
                <th class="col-sm-3">
                    @if($proposta->is_enviado == 0 && $proposta->is_accept < 3)
                        <button type="button" data-id="{{ $proposta->id }}" data-toggle="modal" data-target="#" class="btn ink-reaction btn-floating-action btn-sm btn-info showdata"><i class="fa fa-eye"></i></button>
                        <button type="button" data-id="{{ $proposta->id }}" data-toggle="modal" data-target="#sendproposta" class="btn ink-reaction btn-floating-action btn-sm btn-success sendorder"><i class="fa fa-send"></i></button>
                    @else
                        <button type="button" data-id="{{ $proposta->id }}" data-toggle="modal" data-target="#" class="btn ink-reaction btn-floating-action btn-sm btn-info showdata"><i class="fa fa-eye"></i></button>
                    @endif
                </th>
            </tr>
        @endforeach
    </tbody>
</table>

<div class="modal fade" id="sendproposta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="form-send-contato" method="post" action="{{ route('proposta.send') }}">
                <input type="hidden" name="cliente_id" value="{{ $cliente->id }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Selecione o Contato</h4>
                </div>
                <div class="modal-body">
                    <div id="div-dados"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="send-form-proposta" class="btn btn-primary">Enviar Proposta</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="viewproposta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-dialog" role="document"style="width: 90%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Dados da Proposta</h4>
            </div>
            <div class="modal-body">
                <div id="div-dados-proposta"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>

        </div>
    </div>
</div>