<div class="col-md-12">
    <div class="panel-group">
        <div class="card">
            <div class="card-body">
                <form class="form">
                    <div class="form-group">
                        <div class="input-daterange input-group" id="demo-date-range">
                            <div class="input-group-content">
                                <input type="text" class="form-control" id="start" name="start" />
                                <label>Filtrar por Período</label>
                            </div>
                            <span class="input-group-addon">to</span>
                            <div class="input-group-content">
                                <input type="text" class="form-control" id="end" name="end" />
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="card-actionbar">
                    <div class="card-actionbar-row" style="text-align: center;">
                        <a href="javascript:void(0);" class="btn btn-flat btn-accent ink-reaction btn-filtro">Filtrar</a>
                    </div>
                </div>
            </div><!--end .card-body -->
        </div><!--end .card -->
        <div class="card">
            <div class="card-body"  id="accordion-services">
            </div><!--end .card-body -->
            <div class="card-body loader" style="display: none;">
                <div style="text-align: center;display: block;"><img src="{{ \Illuminate\Support\Facades\URL::to('/loading.gif') }}" alt=""></div>
            </div><!--end .card-body -->

        </div><!--end .card -->
    </div>
</div>



