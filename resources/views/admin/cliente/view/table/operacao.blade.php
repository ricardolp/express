<table style="margin-top:50px;" id="datatable5" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Tipo de Operacao</th>
        <th class="hidden-xs">Marítmo</th>
        <th class="hidden-xs">Aéreo</th>
        <th class="hidden-xs">Rodoviário</th>
        <th class="hidden-xs">Previsao</th>
        <th class="col-sm-3">Ações</th>
    </tr>
    </thead>
    <tbody>
        @foreach($operacoes as $operacao)
            <tr>
                <td>{{ ($operacao->tipo_operacao == 1) ? 'Importação' : 'Exportação' }}</td>
                <td>{!! ($operacao->is_maritmo == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>' !!}</td>
                <td>{!! ($operacao->is_aereo == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>' !!}</td>
                <td>{!! ($operacao->is_rodoviario == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>' !!}</td>
                <td>{{ $operacao->previsao }}</td>
                <td>
                    <button data-id="{{ $operacao->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-primary operacao-show"><i class="fa fa-eye"></i></button>
                    <a href="{{ route('operacao.edit', $operacao->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('operacao.destroy', $operacao->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-danger"><i class="fa fa-close"></i></a>

                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@include('admin.cliente.modal.operacao')