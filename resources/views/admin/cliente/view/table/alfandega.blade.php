<table id="datatable2" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Descrição</th>
        <th>Contato</th>
        <th>Acordo</th>
        <th>Visita</th>
        <th class="col-sm-3">Ações</th>
    </tr>
    </thead>
    <tbody>
        @foreach($alfandegas as $alfandega)
            <tr>
                <td>{{ $alfandega->alfandega($alfandega->id) }}</td>
                <td>{{ $alfandega->contato }}</td>
                <td>{!!  ($alfandega->is_acordo == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>'  !!}</td>
                <td>{!!  ($alfandega->is_visita == 1) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>'  !!}</td>
                <td class="col-sm-3">
{{--
                    <button data-id="{{ $alfandega->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-primary operacao-show"><i class="fa fa-eye"></i></button>
--}}
                    <a href="{{ route('alfandega.cliente.edit', $alfandega->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-info"><i class="fa fa-edit"></i></a>
                    <a href="{{ route('alfandega.cliente.destroy', $alfandega->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-danger"><i class="fa fa-close"></i></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>