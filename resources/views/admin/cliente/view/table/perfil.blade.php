@if($perfil->count() > 0)
  
<div class="col-md-12" style="margin-top:20px;">
    <div class="card card-underline">

        <div class="card-head">
            <header>Dados Bancários</header>
            <div class="tools">
                <div class="btn-group">
                    <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                </div>

            </div>
        </div><!--end .card-head -->
        <div class="card-body" style="padding: 10px;display:none;">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="width: 10px:text-align:center;">#</th>
                    <th>Title</th>
                </tr>
                </thead>
                <tbody>
                <tbody>
                <tr>
                    <th>Banco</th>
                    <th>
                        {!! $perfil->banco  !!}
                    </th>
                </tr>
                <tr>
                    <th>Agência</th>
                    <th>
                        {!! $perfil->agencia  !!}
                    </th>
                </tr>
                <tr>
                    <th>C/C</th>
                    <th>
                        {!! $perfil->cc  !!}
                    </th>
                </tr>
                <tr>
                    <th>CNPJ</th>
                    <th>
                        {!! $perfil->cnpj  !!}
                    </th>
                </tr>
                </tbody>
                </tbody>
            </table>
        </div><!--end .card-body -->
    </div><!--end .card -->
</div>

<table class="table table-striped">
    <thead>
    <tr>
        <th style="width: 10px:text-align:center;">#</th>
        <th>Descrição</th>
        <th>#</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="text-align: center;">

            {!! ($perfil->is_ncm == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}
        </td>
        <td>As descrições e NCM's serão informadas antecipadamente
            (banco de dados) ?</td>
    </tr>
    <tr>
        <td class="col-md-1" style="text-align: center;">
            {!! ($perfil->is_licenca_importacao == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}
        </td>
        <td class="col-md-6">As mercadorias importadas necessitam de Licença de
            Importação ou Tratamento Administrativo?

        </td>
        <td class="col-md-4">
            {{ $perfil->orgao_anuente }} : {!! ($perfil->orgao_anuente == 'Outros') ? $perfil->outros :'' !!}
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            {!! ($perfil->is_icms == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}
        </td>
        <td>ICMS: Utiliza algum regime especial?</td>
        <td>
            {!! ($perfil->is_icms == 1) ? $perfil->regime_icms : '' !!}
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            {!! ($perfil->is_imposto_federal == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}

        </td>
        <td>Impostos Federais : Possui algum benefício?</td>
        <td>
            {!! ($perfil->is_imposto_federal == 1) ? $perfil->beneficio_imposto_federal : '' !!}
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            {!! ($perfil->is_debito == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}

        </td>
        <td>Débitos dos Impostos serão feitos na conta da TWS?</td>
    </tr>
    <tr>
        <td style="text-align: center;">
            {!! ($perfil->is_debito_afrmm == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}

        </td>
        <td>Débito do AFRMM - Adicional de Frete para Renovação da
            Marinha Mercante, que somente pode ser pago no Banco serão feitos pela conta da TWS?</td>
    </tr>
    <tr>
        <td style="text-align: center;">
            {!! ($perfil->is_logistica == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}

        </td>
        <td>Logística - Cotação de frete internacional será pela TWS?</td>
    </tr>
    <tr>
        <td style="text-align: center;">
            {!! ($perfil->is_seguro == 1) ? '<span class="label label-primary"><i class="fa fa-check"></i></span>' : '<span class="label label-danger"><i class="fa fa-close"></i></span>'  !!}

        </td>
        <td>Seguro - Contratação responsável será feita pela TWS?</td>
    </tr>
    </tbody>
</table>

   @endif