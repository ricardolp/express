<table id="datatable4" class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Data da Visita</th>
        <th class="hidden-xs">Status</th>
        <th class="col-sm-3">Ações</th>
    </tr>
    </thead>
    <tbody>
    @foreach($visitas as $visita)
    <tr class="gradeX" id="idVisita-">
        <td>{{ $visita->data }}</td>
        <td class="hidden-xs">{!! $visita->statusVisita !!}</td>
        <td>
            @if((strtotime ( $visita->data ) <= strtotime ( date('d/m/y') )) &&  ($visita->status == 1 || $visita->status == 4))
                <a href="{{ route('visita.edit', $visita->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-info"><i class="fa fa-edit"></i></a>
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-warning remarcar"><i class="fa fa-rotate-right"></i></button>
                <a href="{{ route('visita.confirmar', $visita->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-success"><i class="fa fa-check"></i></a>
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-danger cancelar"><i class="fa fa-close"></i></button>
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-primary view-visita"><i class="fa fa-eye"></i></button>
            @elseif((strtotime ( $visita->data ) > strtotime ( date('d/m/y') )) && ($visita->status == 1 || $visita->status == 4))
                <a href="{{ route('visita.edit', $visita->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-info"><i class="fa fa-edit"></i></a>
                <a href="{{ route('visita.confirmar', $visita->id) }}" class="btn ink-reaction btn-floating-action btn-sm btn-success"><i class="fa fa-check"></i></a>
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-danger cancelar"><i class="fa fa-close"></i></button>
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-primary view-visita"><i class="fa fa-eye"></i></button>
            @elseif((strtotime ( $visita->data ) < strtotime ( date('d/m/y') )) && $visita->status == 2)
                <a href="{{ route('visita.relatorio', [$visita->id, $cliente->id]) }}" class="btn ink-reaction btn-floating-action btn-sm btn-success"><i class="fa fa-newspaper-o"></i></a>
                <button data-id="{{ $visita->id }}1" class="btn ink-reaction btn-floating-action btn-sm btn-primary view-visita"><i class="fa fa-eye"></i></button>
            @elseif((strtotime ( $visita->data ) < strtotime ( date('d/m/y') )) && $visita->status >= 6)
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-primary view-visita"><i class="fa fa-eye"></i></button>
            @else
                <button data-id="{{ $visita->id }}" class="btn ink-reaction btn-floating-action btn-sm btn-warning remarcar"><i class="fa fa-rotate-right"></i></button>
            @endif
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

@include('admin.cliente.modal.modal')