
<div class="col-md-6">
    <div class="card card-underline">
        <div class="card-head">
            <header>Endereços</header>
            <div class="tools">
                <div class="btn-group">
                    <a href="{{ route('endereco.create', $cliente->id) }}" class="btn btn-icon-toggle"><i class="md md-add"></i></a>
                    <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div><!--end .card-head -->
        <div class="card-body"  style="padding: 10px;display:none;">
            <ul class="list divider-full-bleed">
                @foreach($enderecos as $endereco)
                    <li class="tile">
                        <a class="tile-content ink-reaction">
                            <div class="tile-text">{{ $endereco->tipo_endereco }}</div>
                        </a>
                        <a href="{{ route('endereco.destroy', $endereco->id) }}" class="btn btn-flat ink-reaction">
                            <i class="fa fa-trash"></i>
                        </a>
                        <a href="{{ route('endereco.edit', $endereco->id) }}" class="btn btn-flat ink-reaction">
                            <i class="fa fa-edit"></i>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div><!--end .card-body -->
    </div><!--end .card -->
</div><!--end .md-6 -->