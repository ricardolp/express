<div class="col-md-12">
    <div class="card tabs-left style-default-light">
        <ul class="card-head nav nav-tabs text-center hidden-xs" data-toggle="tabs" >
            <li class="active"><a href="#visita"><i class="fa fa-lg fa-users"></i><br><h5>Visitas<br></h5></a></li>
            <li class=""><a href="#credenciamento"><i class="fa fa-lg fa-calendar-o "></i><br><h5>Credenciamentos<br></h5></a></li>
            <li class=""><a href="#proposta"><i class="fa fa-lg fa-envelope-square "></i><br><h5>Propostas<br></h5></a></li>
            <li class=""><a href="#operacao"><i class="fa fa-lg fa-ship"></i><br><h5>Operações<br></h5></a></li>
            <!--<li class=""><a href="#servico"><i class="fa fa-lg fa-server "></i><br><h5>Serviços<br></h5></a></li>-->
            <li class=""><a href="#infosadd"><i class="fa fa-lg fa-file-text "></i><br><h5>Outras Informações<br></h5></a></li>
            <li class=""><a href="#totalserv"><i class="fa fa-lg fa-tag "></i><br><h5>Total de Serviços<br></h5></a></li>
            <li class=""><a href="#rescliente"><i class="fa fa-lg fa-bar-chart "></i><br><h5>Resumo do Cliente<br></h5></a></li>

        </ul>
        <div class="card-body tab-content">
            <div class="tab-pane active" id="visita">
                <a href="{{ route('visita.create', $cliente->id) }}" type="button" class="btn ink-reaction btn-raised btn-primary pull-right">+ Nova Visita</a>
                @include('admin.cliente.view.table.visita')
            </div><!--end #first6 -->

            <div class="tab-pane" id="credenciamento">
                <a href="{{ route('credenciamento.create', $cliente->id) }}" type="button" class="btn ink-reaction btn-raised btn-primary pull-right">+ Novo Credenciamento</a>
                @include('admin.cliente.view.table.credenciamento')

            </div><!--end #credenciamento -->

            <div class="tab-pane" id="proposta">
                <a href="{{ route('proposta.create', $cliente->id) }}" type="button" class="btn ink-reaction btn-raised btn-primary pull-right">+ Nova Proposta</a>
                @include('admin.cliente.view.table.proposta')

            </div><!--end #proposta -->

            <div class="tab-pane" id="operacao">
                <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                    <li class="active"><a href="#operacao-tab">Operações</a></li>
                    <li><a href="#alfandega-tab">Alfândegas/Fronteiras</a></li>
                </ul>

                <div class="tab-content" style="margin-top:30px;">
                    <div class="tab-pane active" id="operacao-tab">
                        <a href="{{ route('operacao.create', $cliente->id) }}" type="button" class="btn ink-reaction btn-raised btn-primary pull-right">+ Nova Operação</a>
                        @include('admin.cliente.view.table.operacao')
                    </div>

                    <div class="tab-pane" id="alfandega-tab">
                        <a href="{{ route('alfandega.cliente.create', $cliente->id) }}" type="button" class="btn ink-reaction btn-raised btn-primary pull-right">+ Nova Alfândega/Fronteira</a>
                        @include('admin.cliente.view.table.alfandega')
                    </div>
                </div>
            </div><!--end #operacao -->
            <div class="tab-pane" id="servico">
                <a href="../Servico/insert.php?id" type="button" class="btn ink-reaction btn-raised btn-primary pull-right"><i class="md md-add"></i> Novo Servico</a>

                <table id="datatableServico" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Tipo de Serviços</th>
                        <th class="col-sm-3">Ações</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div><!--end #servico -->


            <div class="tab-pane" id="infosadd">
                <div class="row">
                    
                    @if($perfil->count() > 0)
                        <a href="{{ route('perfil.edit', $perfil->id) }}" type="button" class="btn ink-reaction btn-raised btn-primary pull-right"><i class="md md-edit"></i> Editar</a>
                    @else
                        <a href="{{ route('perfil.create', $cliente->id) }}" style="margin-left:20px;"  type="button" class="btn ink-reaction btn-raised btn-primary pull-right">+ Informações</a>
                    @endif
                </div>

                @include('admin.cliente.view.table.perfil')

            </div><!--end #infosadd -->
            <input type="hidden" id="razao_social" value="{{ $cliente->razao_social }}">
            <input type="hidden" id="current_date" value="{{ date('Y-m-d') }}">
            <div class="tab-pane" id="totalserv">
                @include('admin.cliente.view.table.servico')
            </div><!--end #servico -->

            <div class="tab-pane" id="rescliente">
                @include('admin.cliente.view.table.resumo')
            </div><!--end #servico -->
        </div>
    </div><!--end .card -->
</div><!--end .md-12 -->