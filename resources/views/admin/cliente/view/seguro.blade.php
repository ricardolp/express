<div class="col-md-6">
    <div class="card card-underline">
        <div class="card-head">
            <header>Seguro</header>
            <div class="tools">
                <div class="btn-group">
                    <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>

        </div><!--end .card-head -->
        <div class="card-body" style="padding-top:1px;display:none;">
            <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                <li class="active"><a href="#first4">Seguro</a></li>
                <li><a href="#second4">Profit</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="first4">
                    <div class="row" style="margin-top:10px;">
                        <div class="button-config pull-right">
                            @if(!$seguro->count() > 0)
                            <a href="{{ route('seguro.create', $cliente->id) }}" class="btn ink-reaction btn-floating-action btn-info btn-sm">
                                <i class="md md-add"></i>
                            </a>
                            @else
                            <a href="{{ route('seguro.edit', $seguro->id) }}" class="btn ink-reaction btn-floating-action btn-info btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                            @endif
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px;">

                        <div class="col-md-6">
                            <div class="card card-outlined style-info">

                                <div class="card-head">
                                    <header>Compra</header>
                                </div><!--end .card-head -->
                                <div class="card-body" style="display:none;padding:2px;margin-left:30%;">
                                    <h1>{{ ($seguro->count() > 0) ? $seguro->valor_compra : '0.00' }}</h1>
                                </div><!--end .card-body -->
                            </div><!--end .card -->
                        </div>
                        <div class="col-md-6">
                            <div class="card card-outlined style-info">
                                <div class="card-head">
                                    <header>Venda</header>
                                </div><!--end .card-head -->
                                <div class="card-body" style="display:none;padding:2px;margin-left:30%;">
                                    <h1>{{ ($seguro->count() > 0) ? $seguro->valor_venda : '0.00' }}</h1>
                                </div><!--end .card-body -->
                            </div><!--end .card -->
                        </div>
                    </div>


                </div>
                <div class="tab-pane" id="second4">
                    <table id="datatableVisita" class="table table-striped table-hover">
                        <div class="col-md-1 pull-right" style="margin-top:15px;">
                        </div>

                        <thead>
                        <tr>
                            <th>Mes do Seguro</th>
                            <th class="hidden-xs">Valor</th>
                            <th class="col-sm-3">Ações</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div><!--end .card-body -->
    </div><!--end .card -->
</div><!--end .md-6 -->