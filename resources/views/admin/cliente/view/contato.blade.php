<div class="col-md-6">
    <div class="card card-underline">
        <div class="card-head">
            <header>Contatos</header>
            <div class="tools">
                <div class="btn-group">
                    <a href="{{ route('contato.create', $cliente->id) }}" class="btn btn-icon-toggle"><i class="md md-add"></i></a>
                    <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div><!--end .card-head -->
        <div class="card-body"  style="padding: 10px;display:none;">
            <ul class="list divider-full-bleed">
                @foreach($contatos as $contato)
                <li class="tile">
                    <a class="tile-content ink-reaction" onclick="showContato({{ $contato->id }})">
                        <div class="tile-text">{{ strtoupper($contato->nome) }}</div>
                    </a>
                    <a class="btn btn-flat ink-reaction" onclick="setContato({{ $contato->id }})">
                        <i class="fa {{ ($contato->status == 0)? 'fa-thumbs-up' : 'fa-thumbs-down' }}"></i>
                    </a>
                    <a href="{{ route('contato.destroy', $contato->id) }}" class="btn btn-flat ink-reaction">
                        <i class="fa fa-trash"></i>
                    </a>
                    <a href="{{ route('contato.edit', $contato->id) }}" class="btn btn-flat ink-reaction">
                        <i class="fa fa-edit"></i>
                    </a>
                </li>
                @endforeach
            </ul>
        </div><!--end .card-body -->
    </div><!--end .card -->
</div>