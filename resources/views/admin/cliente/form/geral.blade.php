
<div class="form-group floating-label">
    {{ Form::text('razao_social', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('razao_social', 'Razão Social') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('nome_fantasia', null, ['class' => 'form-control']) }}
    {{ Form::label('nome_fantasia', 'Nome Fantasia') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('cnpj', null, ['class' => 'form-control', 'data-inputmask' => "'mask': '99.999.999/9999-99'"]) }}
    {{ Form::label('cnpj', 'CNPJ') }}
    <p class="help-block">CNPJ: 99.999.999/9999-99 </p>
</div>

<div class="form-group floating-label">
    {{ Form::text('inscricao_municipal', null, ['class' => 'form-control']) }}
    {{ Form::label('inscricao_municipal', 'Inscrição Municipal') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('inscricao_estadual', null, ['class' => 'form-control']) }}
    {{ Form::label('inscricao_estadual', 'Inscrição Estadual') }}
</div>

<div class="form-group floating-label">
    <div class="input-group">
        <span class="input-group-addon"><span class="glyphicon glyphicon-phone fa-lg"></span></span>
        <div class="input-group-content">
            {{ Form::text('telefone', null, ['class' => 'form-control', 'data-inputmask' => "'mask': '(99) 9999-9999'"]) }}
            {{ Form::label('telefone', 'Telefone') }}
            <p class="help-block">Telefone: (99) 9999-9999 </p>
        </div>
    </div>
</div>

<div class="form-group floating-label">
    {{ Form::textarea('CNAE', null, ['class' => 'form-control', 'style' => 'width: 608px; height: 132px;']) }}
    {{ Form::label('CNAE', 'CNAE') }}
</div>

<div class="form-group floating-label">
    {{ Form::textarea('atividade', null, ['class' => 'form-control', 'style' => 'width: 608px; height: 132px;']) }}
    {{ Form::label('atividade', 'Atividades') }}
</div>


<div class="form-group floating-label">
    <div class="input-group date" id="date-fundacao">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <div class="input-group-content">
            {{ Form::text('fundacao', null, ['class' => 'form-control']) }}
            {{ Form::label('fundacao', 'Data de Fundação') }}
        </div>
    </div>
</div>

<div class="form-group floating-label">
    <div class="input-group date" id="date-impo">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <div class="input-group-content">
            {{ Form::text('ultima_importacao', null, ['class' => 'form-control']) }}
            {{ Form::label('ultima_importacao', 'Última Importação') }}
        </div>
    </div>
</div>


<div class="form-group floating-label">
    <div class="input-group date" id="date-expo">
        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
        <div class="input-group-content">
            {{ Form::text('ultima_exportacao', null, ['class' => 'form-control']) }}
            {{ Form::label('ultima_exportacao', 'Última Exportação') }}
        </div>
    </div>
</div>
<br>
<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_radar', 1,null) }}
        <span>Possui Radar</span>
    </label>
</div>
<br>

<div class="form-group floating-label">
    {{ Form::select('tipo_indicacao', $indicadores, null ,['class' => 'form-control select2-list','id'=>"tipo_indicacao"]) }}
    {{ Form::label('tipo_indicacao', 'Indicador Por') }}
</div>

<div class="form-group floating-label cliente-lista" style="display: none;">
    {{ Form::select('indicado_to[]', $clientes, null ,['class' => 'form-control select2-list','data-placeholder'=>"Select an item",  'id' => 'clientes']) }}
    {{ Form::label('indicado_to', 'Selecione o Cliente') }}
</div>

<div class="form-group floating-label parceiro-lista"  style="display: none;">
    {{ Form::select('indicado_to[]', $parceiros, null ,['class' => 'form-control select2-list','data-placeholder'=>"Select an item", 'id' => 'parceiro']) }}
    {{ Form::label('indicado_to', 'Selecione o Parceiro') }}
</div>

<div class="form-group floating-label equipe-lista"  style="display: none;">
    {{ Form::select('indicado_to[]', $equipe, null ,['class' => 'form-control select2-list','data-placeholder'=>"Select an item", 'id' => 'equipe']) }}
    {{ Form::label('indicado_to', 'Selecione o Indicador') }}
</div>


