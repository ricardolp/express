<div class="form-group floating-label">
    {{ Form::select('frequencia_visita', ['1' => 'Quizenal', '2' => 'Mensal', '3' => 'Trimestral', '4' => 'Semestral'], null ,['class' => 'form-control select2-list','data-placeholder'=>"Select an item"]) }}
    {{ Form::label('frequencia_visita', 'Frequência da Visita') }}
</div>
<br>
<div class="form-group floating-label">
    {{ Form::select('responsavel_visita', $equipe,null ,['class' => 'form-control','data-placeholder'=>"Select an item"]) }}
    {{ Form::label('responsavel_visita', 'Responsavel pela Visita') }}
</div>
<br>
<h3>Notificação</h3>
<br>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_aviso_dia', 1,null) }}
        <span>Um Dia Antes</span>
    </label>
</div>
<br>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_aviso_semana', 1,null) }}
        <span>Uma Semana Antes</span>
    </label>
</div>
<br>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_aviso_quinzena', 1,null) }}
        <span>Quinze Dias Antes</span>
    </label>
</div>
<br>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_aviso_mes', 1,null) }}
        <span>Um Mês Antes</span>
    </label>
</div>
<br>
