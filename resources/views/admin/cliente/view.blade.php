@extends('template')

@section('content')
    <br>
    <br>

    <div class="card-head style-info">
        <div class="tools pull-left" style="margin-left:25px;">
            <a data-original-title="Voltar para lista de clientes" data-placement="right" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('cliente.index') }}"><span class="glyphicon glyphicon-arrow-left"></span> &nbsp;Voltar Para Listagem</a>
        </div><!--end .tools -->
        <div class="tools" style="margin-left:25px;">
            <a class="btn btn-floating-action btn-default" href="{{ route('cliente.edit', $cliente->id) }}" ><i class="fa fa-edit"></i></a>
            <a class="btn btn-floating-action btn-default" href="{{ route('cliente.status', $cliente->id) }}" id="status-client" data-id="{{ $cliente->id }}" >
                <i class="md {{ ($cliente->is_status ==1 || $cliente->is_status ==3) ? 'md-thumb-up' : 'md-thumb-down' }}"></i>
            </a>
            <input type="hidden" id="valueChange" value="" />
            <input type="hidden" id="current_data" value="{{ date('Y-m-d') }}">
        </div><!--end .tools -->
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card card-printable">
                <div class="card-body">
                    <div class="row">

                        @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">
                                <strong>Atenção!</strong> {{ Session::get('error') }}
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-md-12 text-center">
                                <h3 class="text-light">Dados Gerais</h3>
                            </div>
                            <table class="table table-striped no-margin">
                                <thead>
                                <tr>
                                    <td class="col-md-2"><b>Razão Social:</b></td>
                                    <td>{{ $cliente->razao_social }}</td>
                                </tr>
                                
                                <tr>
                                    <td class="col-md-2"><b>Nome Fantasia:</b></td>
                                    <td>{{ $cliente->nome_fantasia }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>Status</b></td>
                                    <td>{{ $cliente->status_desc }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>CNPJ</b></td>
                                    <td>{{ $cliente->cnpj }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>Inscrição Estadual: </b></td>
                                    <td>{{ $cliente->inscricao_estadual }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>Inscrição Municipal: </b></td>
                                    <td>{{ $cliente->inscricao_municipal }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>Telefone:  </b></td>
                                    <td>{{ $cliente->telefone }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>FAX: </b></td>
                                    <td>{{ $cliente->fax }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>CNAE: </b></td>
                                    <td>{{ $cliente->CNAE }}</td>
                                </tr>
                                <tr>
                                    <td class="col-md-2"><b>Atividades: </b></td>
                                    <td>{{ $cliente->atividade }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div><!--end .col -->
                    </div><!--end .row -->
                    <br/>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div><!--end .col -->

        @include('admin.cliente.view.contato')

        @include('admin.cliente.view.endereco')

        @include('admin.cliente.view.seguro')

        @include('admin.cliente.view.direct')

        @include('admin.cliente.view.detalhe')

    </div><!--end .row -->


@endsection