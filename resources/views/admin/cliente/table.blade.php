
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable1" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>CNPJ</th>
                <th>Razão Social</th>
                <th>Nome Fantasia</th>
                <th class="col-md-2">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($clientes as $cliente)
                <tr class="gradeX">
                    <td>{{ $cliente->cnpj }}</td>
                    <td>{{ $cliente->razao_social }}</td>
                    <td>{{ $cliente->nome_fantasia }}</td>
                    <td>
                        <a href="{{ route('cliente.edit', $cliente->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-info">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <a href="{{ route('cliente.view', $cliente->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-success">
                            <i class="fa fa-user"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>