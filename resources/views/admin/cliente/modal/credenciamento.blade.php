<div class="modal fade" id="modalCredenciamento" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="credenciamento-label"></h4>
            </div>
            <form class="form-horizontal" role="form" method="post" action="{{ route('visita.cancelar') }}">
                <div class="hidden-visita"></div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="150px"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="text-align:center;">
                                Data
                            </td>
                            <td>
                                <span id="credenciamento-data"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                Vencimento
                            </td>
                            <td>
                                <span id="credenciamento-vencimento"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                Arquivo
                            </td>
                            <td>
                                <span id="credenciamento-button"></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->