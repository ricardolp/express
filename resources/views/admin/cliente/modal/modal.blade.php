<div class="modal fade" id="visitaView" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Dados de Visita</h4>
            </div>
            <div class="modal-body">
                <div class="view-visita"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Reagendamento de Visita</h4>
            </div>
            <form class="form-horizontal" role="form" method="post" action="{{ route('visita.reagendar') }}">
                <div class="hidden"></div>
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="form-group floating-label">
                                <div class="input-group date" id="date-fundacao">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="input-group-content">
                                        {{ Form::text('data', null, ['class' => 'form-control']) }}
                                        {{ Form::label('data', 'Selecione a nova data:') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Reagendar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Cancelamento de Visita</h4>
            </div>
            <form class="form-horizontal" role="form" method="post" action="{{ route('visita.cancelar') }}">
                <div class="hidden-visita"></div>
                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group floating-label">
                            <div class="input-group-content">
                                {{ Form::textarea('justificativa', null, ['class' => 'form-control']) }}
                                {{ Form::label('justificativa', 'Justificativa:') }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Cancelar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->