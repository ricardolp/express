<div class="modal fade" id="modalOperacao" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="credenciamento-label"></h4>
            </div>
            <form class="form-horizontal" role="form" method="post" action="{{ route('visita.cancelar') }}">
                <div class="hidden-visita"></div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="text-align:left;">
                                Tipo de Operação
                            </td>
                            <td>
                                <span id="operacao-tipo"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Previsão
                            </td>
                            <td>
                                <span id="operacao-previsao"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Finalidade
                            </td>
                            <td>
                                <span id="operacao-finalidade"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Origem
                            </td>
                            <td>
                                <span id="operacao-origem"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Tempo de Importação
                            </td>
                            <td>
                                <span id="operacao-tempo"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Fornecedor
                            </td>
                            <td>
                                <span id="operacao-fornecedor"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Dificuldades
                            </td>
                            <td>
                                <span id="operacao-dificultade"></span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left;">
                                Comentários
                            </td>
                            <td>
                                <span id="operacao-comentario"></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->