<div class="form-group floating-label">
    {{ Form::text('descricao', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('descricao', 'Descrição') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('descricao_direct', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('descricao_direct', 'Descrição Direct') }}
</div>

<div class="form-group col-sm-12">

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_file', 1,null) }}
            <span>O Item possui arquivo</span>
        </label>
    </div>
</div>
