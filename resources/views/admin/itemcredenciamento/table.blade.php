
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable1" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Descrição</th>
                <th>Descrição Direct</th>
                <th>Arquivo</th>
                <th class="col-md-2">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($itemcredenciamentos as $itemcredenciamento)
                <tr class="gradeX">
                    <td>{{ $itemcredenciamento->descricao }}</td>
                    <td>{{ $itemcredenciamento->descricao_direct }}</td>
                    <td>{!! ($itemcredenciamento->is_file != 0) ? '<i class="fa fa-check"></i>' : '<i class="fa fa-close"></i>' !!}</td>
                    <td>
                        <a href="{{ route('itemcredenciamento.edit', $itemcredenciamento->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-info">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <a href="{{ route('itemcredenciamento.destroy', $itemcredenciamento->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-danger">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>