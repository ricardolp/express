<input type="hidden" name="cliente_id" value="{{ $cliente_id }}">
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('tipo_endereco', ['Financeiro' => 'Financeiro', 'Comercial' => 'Comercial'], null,['class' => 'form-control select2-list', 'id' => 'tipo_endereco']) }}
        {{ Form::label('tipo_endereco', 'Tipo de Endereço') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cep',  null,['class' => 'form-control', 'id' => 'cep']) }}
        {{ Form::label('cep', 'CEP') }}
    </div>
</div>

<div class="col-md-8">
    <div class="form-group floating-label">
        {{ Form::text('endereco',  null,['class' => 'form-control', 'id' => 'endereco']) }}
        {{ Form::label('endereco', 'Endereço') }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group floating-label">
        {{ Form::text('num_endereco',  null,['class' => 'form-control', 'id' => 'numero']) }}
        {{ Form::label('num_endereco', 'Número') }}
    </div>
</div>

<div class="col-md-8">
    <div class="form-group floating-label">
        {{ Form::text('bairro',  null,['class' => 'form-control', 'id' => 'bairro']) }}
        {{ Form::label('bairro', 'Bairro') }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group floating-label">
        {{ Form::text('complemento',  null,['class' => 'form-control', 'id' => 'complemento']) }}
        {{ Form::label('complemento', 'Complemento') }}
    </div>
</div>


<div class="col-md-8">
    <div class="form-group floating-label">
        {{ Form::text('cidade',  null,['class' => 'form-control', 'id' => 'cidade']) }}
        {{ Form::label('cidade', 'Cidade') }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group floating-label">
        {{ Form::text('uf',  null,['class' => 'form-control', 'id' => 'uf']) }}
        {{ Form::label('uf', 'UF') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('pais',  null,['class' => 'form-control', 'id' => 'pais']) }}
        {{ Form::label('pais', 'País') }}
    </div>
</div>