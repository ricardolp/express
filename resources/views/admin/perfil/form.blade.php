<input type="hidden" name="cliente_id" value="{{ $cliente->id }}">
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_ncm', 1,null ) }}
            <span>As descrições e NCM's serão informadas antecipadamente (banco de dados) ?</span>
        </label>
    </div>
</div>

<div class="col-md-12">

        <div class="checkbox checkbox-styled">
            <label>
                {{ Form::checkbox('is_licenca_importacao', 1,null, ['id' => 'is_licenca_importacao'] ) }}
                <span>As mercadorias importadas necessitam de Licença de Importação ou Tratamento Administrativo?</span>
            </label>
        </div>

    <div class="col-md-12 orgao-anuente" style="display: none">
        <div class="form-group floating-label">
            {{ Form::select('orgao_anuente',['Anvisa' => 'Anvisa','Inmetro' => 'Inmetro','Mapa' => 'Mapa','Decex' => 'Decex','Outros' => 'Outros. Quais?'] ,null, ['class' => 'form-control orgao_anuente']) }}
            {{ Form::label('orgao_anuente', 'Orgão Anuente') }}
        </div>
    </div>
    <div class="col-md-12 orgao-outro" style="display: none">
        <div class="form-group floating-label">
            {{ Form::text('outros' ,null, ['class' => 'form-control']) }}
            {{ Form::label('outros', 'Outros') }}
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_icms', 1,null, ['id' => 'icms'] ) }}
            <span>ICMS: Utiliza algum regime especial?</span>
        </label>
    </div>

    <div class="col-md-12 icms-regime" style="display: none">
        <div class="form-group floating-label">
            {{ Form::text('regime_icms',null, ['class' => 'form-control']) }}
            {{ Form::label('regime_icms', 'Regime Especial') }}
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_imposto_federal', 1,null, ['id' => 'is_imposto_federal'] ) }}
            <span>Impostos Federais : Possui algum benefício?</span>
        </label>
    </div>

    <div class="col-md-12 imposto-beneficio" style="display: none">
        <div class="form-group floating-label">
            {{ Form::text('beneficio_imposto_federal',null, ['class' => 'form-control']) }}
            {{ Form::label('beneficio_imposto_federal', 'Benefícios') }}
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_debito', 1,null ) }}
            <span>Débitos dos Impostos serão feitos na conta da TWS?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_debito_afrmm', 1,null ) }}
            <span>Débito do AFRMM - Adicional de Frete para Renovação da Marinha Mercante, que somente pode ser pago no Banco serão feitos pela conta da TWS?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_logistica', 1,null ) }}
            <span>Logística - Cotação de frete internacional será pela TWS?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_seguro', 1,null ) }}
            <span>Seguro - Contratação responsável será feita pela TWS?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <h6>Dados Bancários</h6>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('banco', null, ['class' => 'form-control']) }}
        {{ Form::label('banco', 'Banco') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('agencia', null, ['class' => 'form-control']) }}
        {{ Form::label('agencia', 'Agência') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('cc', null, ['class' => 'form-control']) }}
        {{ Form::label('cc', 'CC') }}
    </div>
</div>
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cnpj', null, ['class' => 'form-control', 'data-inputmask' => "'mask': '99.999.999/9999-99'"]) }}
        {{ Form::label('cnpj', 'CNPJ') }}
    </div>
</div>