@extends('template')

@section('content')

    <div class="col-lg-12"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('cliente.view', $cliente->id) }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar para Cliente
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-body style-default">
                        <div class="col-md-12">
                           
                            {{ Form::model($perfil, ['route' => ['perfil.update', $perfil->id], 'id'=>'form', 'enctype' => 'multipart/form-data']) }}

                            @include('admin.perfil.form')

                            <div class="col-md-12">
                                <div class="form-group floating-label is-file" style="display: none;">
                                    {{ Form::file('file', ['class' => 'form-control'])  }}
                                </div>
                            </div>

                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-info">Gravar</button>
                            </div>
                            {{ Form::close() }}
                        </div>

                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        checkPerfil()
    </script>
@endsection