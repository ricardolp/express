<div class="form-group floating-label">
    {{ Form::text('descricao', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('descricao', 'Descrição') }}
</div>

<div class="form-group col-sm-12">

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_obrigatorio', 1,null) }}
            <span>Obrigatório</span>
        </label>
    </div>

    <h6>Configuração de Notificação</h6>

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_followup', 1,null ) }}
            <span>Notificações de Follow UP</span>
        </label>
    </div>

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_kpi', 1,null ) }}
            <span>Notificações de KPI</span>
        </label>
    </div>

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_direct', 1,null ) }}
            <span>Notificações de Direct</span>
        </label>
    </div>

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_noticia',1, null) }}
            <span>Notificações de Notícias</span>
        </label>
    </div>

</div>
