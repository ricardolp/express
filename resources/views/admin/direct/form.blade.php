<input type="hidden" name="cliente_id" value="{{ $id }}">

<div class="col-md-6">
    <div class="row">
        <div class="form-group floating-label">
            <div class="input-group date" id="date-importacao">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <div class="input-group-content">
                    {{ Form::text('data_importacao', null, ['class' => 'form-control', 'required' => 'required']) }}
                    {{ Form::label('data_importacao', 'Data de Importação') }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <select class="form-control select2" name="processo_importacao" style="width: 100%;">
            <option selected="selected">Selecione...</option>
            @foreach($faturas as $fatura)
                <option value="{{ $fatura->FAT_FATURA }}">{{ $fatura->FAT_FATURA }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-5 pull-right">
    <div class="row">
        <div class="form-group floating-label">
            <div class="input-group date" id="date-exportacao">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <div class="input-group-content">
                    {{ Form::text('data_exportacao', null, ['class' => 'form-control', 'required' => 'required']) }}
                    {{ Form::label('data_exportacao', 'Data de Exportação') }}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <select class="form-control select2" name="processo_exportacao" style="width: 100%;">
            <option selected="selected">Selecione...</option>
            @foreach($faturas as $fatura)
                <option value="{{ $fatura->FAT_FATURA }}">{{ $fatura->FAT_FATURA }}</option>
            @endforeach
        </select>
    </div>
</div>


