<input type="hidden" name="cliente_id" value="{{ $cliente->id }}">
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('tipo_servico_id', $tipo_servico, null, ['class' => 'form-control select2']) }}
        {{ Form::label('tipo_servico_id', 'Tipo de Serviço') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('expectativa', null, ['class' => 'form-control']) }}
        {{ Form::label('expectativa', 'Expectativa') }}
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_before', 1,null ) }}
            <span>Já Realizou está operação antes?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_know', 1,null ) }}
            <span>Sabe o que é?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <br>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('tipo_operacao', ['1' => 'Importação' , '2' => 'Exportação'], null, ['class' => 'form-control select2 changed', 'id' => 'tipo-operacao']) }}
        {{ Form::label('tipo_operacao', 'Tipo de Operação') }}
    </div>
</div>

<div class="col-md-12">
    <h6>Modal</h6>
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_maritmo', 1, null, ['class' => 'changed', 'id' => 'is-maritmo']) }}
            <span>Marítmo</span>
        </label>
    </div>
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_aereo', 1, null , ['class' => 'changed', 'id' => 'is-aereo']) }}
            <span>Aéreo</span>
        </label>
    </div>
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_rodoviario', 1, null , ['class' => 'changed', 'id' => 'is-rodoviario']) }}
            <span>Rodoviário</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('incoterms[]', [], null, ['class' => 'form-control select2 incoterm', 'multiple' => 'multiple']) }}
        {{ Form::label('incoterms', 'Incoterms') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('previsao', ['Mensal' => 'Mensal', 'Bimestral' => 'Bimestral', 'Trimestral' => 'Trimestral', 'Semestral' => 'Semestral', 'Anual' => 'Anual'], null, ['class' => 'form-control select2']) }}
        {{ Form::label('previsao', 'Previsão') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('finalidade', null, ['class' => 'form-control']) }}
        {{ Form::label('finalidade', 'Finalidade') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('origem', null, ['class' => 'form-control']) }}
        {{ Form::label('origem', 'Origem') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('tempo_importacao', null, ['class' => 'form-control']) }}
        {{ Form::label('tempo_importacao', 'Tempo de Importação') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('fornecedor', null, ['class' => 'form-control']) }}
        {{ Form::label('fornecedor', 'Fornecedor') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('dificuldade', null, ['class' => 'form-control']) }}
        {{ Form::label('dificuldade', 'Dificuldades') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('comentario', null, ['class' => 'form-control']) }}
        {{ Form::label('comentario', 'Comentarios') }}
    </div>
</div>
