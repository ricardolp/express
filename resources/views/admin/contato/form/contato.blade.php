<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('empresa_anterior', null, ['class' => 'form-control']) }}
        {{ Form::label('empresa_anterior', 'Empresa Anterior') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        <div class="input-group date" id="date-fundacao">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <div class="input-group-content">
                {{ Form::text('tempo_empresa', null, ['class' => 'form-control']) }}
                {{ Form::label('tempo_empresa', 'Está na Empresa Desde:') }}
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('hobby', null, ['class' => 'form-control']) }}
        {{ Form::label('hobby', 'Hobby') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('politica', null, ['class' => 'form-control']) }}
        {{ Form::label('politica', 'Política') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('religiao', null, ['class' => 'form-control']) }}
        {{ Form::label('religiao', 'Religião') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('time_futebol', null, ['class' => 'form-control']) }}
        {{ Form::label('time_futebol', 'Time de futebol') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        <div class="input-group date" id="date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <div class="input-group-content">
                {{ Form::text('data_lembranca', null, ['class' => 'form-control']) }}
                {{ Form::label('data_lembranca', 'Data da Lembrança') }}
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('descricao_lembranca', null, ['class' => 'form-control']) }}
        {{ Form::label('descricao_lembranca', 'Descrição da Lembrança') }}
    </div>
</div>