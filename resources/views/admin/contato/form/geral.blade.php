<input type="hidden" name="cliente_id" value="{{ $cliente_id }}">

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('nome', null, ['class' => 'form-control', 'required' => 'required']) }}
        {{ Form::label('nome', 'Nome do Contato') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::select('funcao_id', $funcao,null ,['class' => 'form-control', 'required' => 'required']) }}
        {{ Form::label('funcao_id', 'Selecione a função') }}
       {{-- <select class="form-control select2-list" data-placeholder="Selecione" name="funcao_id" id="funcao_id">
            @foreach($funcao  as $item)
                <option value="{{ $item->id }}">{{ $item->descricao }}</option>
            @endforeach
        </select>--}}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::select('departamento_id', $departamento,null, ['class' => 'form-control', 'required' => 'required']) }}
        {{ Form::label('departamento_id', 'Selecione o departamento') }}
        {{--<select class="form-control select2-list" data-placeholder="Selecione" name="departamento_id" id="departamento_id">
            @foreach($departamento  as $item)
                <option value="{{ $item->id }}">{{ $item->descricao }}</option>
            @endforeach
        </select>--}}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('telefone', null ,['class' => 'form-control','data-inputmask' => "'mask': '(99)99999-9999'"]) }}
        {{ Form::label('telefone', 'Telefone') }}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('celular', null ,['class' => 'form-control','data-inputmask' => "'mask': '(99)99999-9999'"]) }}
        {{ Form::label('celular', 'Celular') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('fax', null ,['class' => 'form-control','data-inputmask' => "'mask': '(99)99999-9999'"]) }}
        {{ Form::label('fax', 'FAX') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('aniversario', null ,['class' => 'form-control','data-inputmask' => "'mask': '99/99'"]) }}
        {{ Form::label('aniversario', 'Aniversário') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('email', null ,['class' => 'form-control']) }}
        {{ Form::label('email', 'E-Mail') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('observacao', null ,['class' => 'form-control']) }}
        {{ Form::label('observacao', 'Observação') }}
    </div>
</div>

