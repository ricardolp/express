<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_noticia', 1,null) }}
        <span>Noticias</span>
    </label>
</div>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_followup', 1,null) }}
        <span>Follow UP</span>
    </label>
</div>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_kpi', 1,null) }}
        <span>KPI</span>
    </label>
</div>

<div class="checkbox checkbox-styled">
    <label>
        {{ Form::checkbox('is_direct', 1,null) }}
        <span>Direct</span>
    </label>
</div>