<input type="hidden" name="cliente_id" value="{{ $cliente->id }}">
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('alfandega_fronteira_id', $alfandegas, null, ['class' => 'form-control select2']) }}
        {{ Form::label('alfandega_fronteira_id', 'Alfândega/Fronteira') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('contato', null, ['class' => 'form-control']) }}
        {{ Form::label('contato', 'Contato') }}
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_acordo', 1,null ) }}
            <span>Existe Acordo?</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_visita', 1,null ) }}
            <span>Já recebeu visita?</span>
        </label>
    </div>
</div>