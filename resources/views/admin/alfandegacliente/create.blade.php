@extends('template')

@section('content')

    <div class="col-lg-6"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('cliente.view', $cliente->id) }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar para Cliente
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-body style-default">
                        <div class="row">
                            <div class="col-md-12">
                                {{ Form::open(['route' => 'alfandega.cliente.store', 'id'=>'form', 'enctype' => 'multipart/form-data']) }}

                                @include('admin.alfandegacliente.form')


                                <div class="card-actionbar-row">
                                    <button type="submit" class="btn btn-info">Gravar</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>

                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection