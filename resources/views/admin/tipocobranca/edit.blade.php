@extends('template')

@section('content')

    <div class="col-lg-12"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('tipocobranca.index') }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar para listagem
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-head style-default">
                        <header>Tipos de Cobrança </header>
                    </div><!--end .card-head -->
                    <div class="card-body style-default">
                        <div class="col-md-12">
                            {{ Form::model($tipocobranca, ['route' => ['tipocobranca.update', $tipocobranca->id], 'id'=>'form']) }}

                            @include('admin.tipocobranca.form')

                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-info">Gravar</button>
                            </div>
                            {{ Form::close() }}
                        </div>

                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection