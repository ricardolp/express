<div class="form-group floating-label">
    {{ Form::text('descricao', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('descricao', 'Descrição') }}
</div>

<div class="form-group col-sm-12">

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_value', 1,null) }}
            <span>Possui valor mínimo/máximo</span>
        </label>
    </div>
</div>
