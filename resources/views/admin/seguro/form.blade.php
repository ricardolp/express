<input type="hidden" name="cliente_id" value="{{ $id }}">

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('valor_compra', null, ['class' => 'form-control', 'required' => 'required' ,'data-inputmask' => "'mask': '9.99'"]) }}
        {{ Form::label('valor_compra', 'Valor de Compra') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('valor_venda', null, ['class' => 'form-control', 'required' => 'required','data-inputmask' => "'mask': '9.99'"]) }}
        {{ Form::label('valor_venda', 'Valor de Venda') }}
    </div>
</div>
