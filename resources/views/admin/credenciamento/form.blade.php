<input type="hidden" name="cliente_id" value="{{ $cliente->id }}">
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('item_credenciamento_id', $itens_credenciamentos, null, ['class' => 'form-control item-credenciamento']) }}
        {{ Form::label('item_credenciamento_id', 'Tipo de Credênciamento') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        <div class="input-group date" id="date-fundacao">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <div class="input-group-content">
                {{ Form::text('vencimento', null, ['class' => 'form-control']) }}
                {{ Form::label('vencimento', 'Vencimento:') }}
            </div>
        </div>
    </div>
</div>
