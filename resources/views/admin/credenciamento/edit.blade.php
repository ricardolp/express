@extends('template')

@section('content')

    <div class="col-lg-12"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('cliente.view', $credenciamento->cliente_id) }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar para Cliente
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-body style-default">
                        <div class="col-md-12">
                            {{ Form::model($credenciamento, ['route' => ['credenciamento.update', $credenciamento->id], 'id'=>'form', 'enctype' => 'multipart/form-data']) }}

                            @include('admin.credenciamento.form')

                            @if($credenciamento->path_file != null && $credenciamento->path_file != '')
                                <div class="col-md-12">
                                    <div class="form-group floating-label">
                                        <div class="input-group">
                                            <div class="input-group-content">
                                                <input class="form-control" id="groupbutton9" type="text" value="{{ $credenciamento->item->descricao }}" disabled>
                                                <label for="groupbutton9">Arquivo Atual</label>
                                            </div>
                                            <div class="input-group-btn">
                                                <a href="{{ URL::to('files/credenciamento/'.$credenciamento->path_file) }}" target="_blank" class="btn btn-primary" type="button"><i class="fa fa-eye"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            <div class="col-md-12">
                                <div class="form-group floating-label is-file" style="display: none;">
                                    {{ Form::file('file', ['class' => 'form-control'])  }}
                                </div>
                            </div>

                            <div class="card-actionbar-row">
                                <button type="submit" class="btn btn-info">Gravar</button>
                            </div>
                            {{ Form::close() }}
                        </div>

                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>
        $(document).ready(function (){
           $(".item-credenciamento").val({{ $credenciamento->item_credenciamento_id }}).trigger('change');
        });
    </script>
@endsection