<input type="hidden" name="parceiro_id" value="{{ $parceiro->id }}">
<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('razao_social', $parceiro->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('razao_social', 'Razão Social') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('nome', null, ['class' => 'form-control', 'required' => 'required']) }}
        {{ Form::label('nome', 'Nome') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) }}
        {{ Form::label('email', 'E-mail') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        <select class="form-control select2-list" data-placeholder="Selecione" name="funcao_id">
            @foreach($funcao  as $item)
                <option value="{{ $item->id }}">{{ $item->descricao }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        <select class="form-control select2-list" data-placeholder="Selecione" name="departamento_id">
            @foreach($departamento  as $item)
                <option value="{{ $item->id }}">{{ $item->descricao }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('aniversario', null, ['class' => 'form-control', 'required' => 'required','data-inputmask' => "'mask': '99/99'"]) }}
        {{ Form::label('aniversario', 'Aniversário') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::textarea('observacao', null, ['class' => 'form-control']) }}
        {{ Form::label('observacao', 'Observações') }}
    </div>
</div>
