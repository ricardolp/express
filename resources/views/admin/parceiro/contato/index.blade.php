
<div class="col-md-6">
    <div class="card card-underline card-collapsed">
        <div class="card-head">
            <header>Contatos</header>
            <div class="tools">
                <div class="btn-group">
                    <a href="{{ route('contato.parceiro.create', $parceiro->id) }}" class="btn btn-icon-toggle"><i class="md md-add"></i></a>
                </div>
            </div>
        </div><!--end .card-head -->
        <div class="card-body" style="padding: 10px;">

            @include('errors.messages')

            <ul class="list divider-full-bleed">
                @foreach($contatos as $contato)
                    <li class="tile">
                        <a class="tile-contentink-reaction">
                            <div class="tile-text">{{ $contato->nome }}</div>
                        </a>
                        <a href="{{ route('contato.parceiro.destroy', $contato->id) }}" class="btn btn-flat ink-reaction">
                            <i class="fa fa-trash"></i>
                        </a>
                        <a href="{{ route('contato.parceiro.edit', $contato->id) }}" class="btn btn-flat ink-reaction">
                            <i class="fa fa-edit"></i>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div><!--end .card-body -->
    </div><!--end .card -->
</div>
