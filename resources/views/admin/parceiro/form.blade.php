<div class="form-group floating-label">
    {{ Form::text('razao_social', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('razao_social', 'Razão Social') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('cnpj', null, ['class' => 'form-control', 'required' => 'required' , 'data-inputmask' => "'mask': '99.999.999/9999-99'"]) }}
    {{ Form::label('cnpj', 'CNPJ') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('telefone', null, ['class' => 'form-control', 'required' => 'required', 'data-inputmask' => "'mask': '(99) 9999-9999'"]) }}
    {{ Form::label('telefone', 'Telefone') }}
</div>

<div class="form-group floating-label">
    {{ Form::text('endereco', null, ['class' => 'form-control', 'required' => 'required']) }}
    {{ Form::label('endereco', 'Endereço') }}
</div>
