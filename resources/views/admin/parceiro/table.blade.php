
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable1" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Razão Social</th>
                <th>CNPJ</th>
                <th>Telefone</th>
                <th>Endereço</th>
                <th class="col-md-2">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($parceiros as $parceiro)
                <tr class="gradeX">
                    <td>{{ $parceiro->razao_social }}</td>
                    <td>{{ $parceiro->cnpj }}</td>
                    <td>{{ $parceiro->telefone }}</td>
                    <td>{{ $parceiro->endereco }}</td>
                    <td>
                        <a href="{{ route('parceiro.edit', $parceiro->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-info">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <a href="{{ route('parceiro.destroy', $parceiro->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-danger">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>