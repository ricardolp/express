@extends('template')

@section('content')
    <br>
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="card">
                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-success no-margin">
                        <h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
                        <strong class="text-xl">{{ $ativos }}</strong><br>
                        <span class="opacity-50">Clientes Ativos</span>
                    </div>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="card">
                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-warning no-margin">
                        <h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
                        <strong class="text-xl">{{ $prospect }}</strong><br>
                        <span class="opacity-50">Clientes Prospect</span>
                    </div>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="card">
                <div class="card-body no-padding">
                    <div class="alert alert-callout alert-danger no-margin">
                        <h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
                        <strong class="text-xl">{{ $inativos }}</strong><br>
                        <span class="opacity-50">Clientes Inativos</span>
                    </div>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div>

    </div>
    <div class="row">
        <!-- BEGIN CALENDAR -->
        <div class="col-sm-6">
            <div class="card">
                <div class="card-head style-info">
                    <header>
                        <span class="selected-day">&nbsp;</span> &nbsp;<small class="selected-date">&nbsp;</small>
                    </header>
                    <div class="tools">
                        <div class="btn-group">
                            <a id="calender-prev" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-angle-left"></i></a>
                            <a id="calender-next" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-angle-right"></i></a>
                        </div>
                        <div class="btn-group pull-right">
                        </div>
                    </div>
                    <ul class="nav nav-tabs tabs-text-contrast tabs-accent" data-toggle="tabs">
                        <li data-mode="month" class="active"><a href="#">Month</a></li>
                        <li data-mode="agendaWeek"><a href="#">Week</a></li>
                        <li data-mode="agendaDay"><a href="#">Day</a></li>
                    </ul>
                </div><!--end .card-head -->
                <div class="card-body no-padding">
                    <br>
                    <br>
                    <div class="col-md-12">
                        <div id="calendar"></div>

                    </div>
                </div><!--end .card-body -->
            </div><!--end .card -->
        </div><!--end .col -->

        <!-- END CALENDAR -->
    </div>

    <!-- BEGIN SIMPLE MODAL MARKUP -->
    <div class="modal fade" id="modalVisita" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="simpleModalLabel">Informações da Visita</h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <th class="col-md-3"> <strong>Cliente: </strong></th>
                            <th class="cliente-visita"></th>
                        </tr>
                        <tr>
                            <th class="col-md-3"> <strong>Data/Hora: </strong></th>
                            <th class="data-visita"></th>
                        </tr>
                        <tr>
                            <th class="col-md-3"> <strong>Local: </strong></th>
                            <th class="local-visita"></th>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- BEGIN SIMPLE MODAL MARKUP -->
    <div class="modal fade" id="modalCredenciamento" tabindex="-1" role="dialog" aria-labelledby="simpleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="simpleModalLabel">Informações da Visita</h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <th class="col-md-3"> <strong>Cliente/Documento: </strong></th>
                            <th class="cliente-credenciamento"></th>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- END SIMPLE MODAL MARKUP -->
@endsection