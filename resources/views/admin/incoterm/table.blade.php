
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable1" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Descrição</th>
                <th class="col-md-2">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($incoterms as $incoterm)
                <tr class="gradeX">
                    <td>{{ $incoterm->descricao }}</td>
                    <td>
                        <a href="{{ route('incoterm.edit', $incoterm->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-info">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <a href="{{ route('incoterm.destroy', $incoterm->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-danger">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>