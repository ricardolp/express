<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('descricao', null, ['class' => 'form-control', 'required' => 'required']) }}
        {{ Form::label('descricao', 'Descrição') }}
    </div>
</div>


<div class="col-md-12">
    <h6>Tipo de Serviço</h6>
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_importacao', 1,null) }}
            <span>Importação</span>
        </label>
    </div>
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_exportacao', 1,null) }}
            <span>Exportação</span>
        </label>
    </div>
</div>

<div class="col-md-12">
    <h6>Tipo de Modal</h6>
    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_maritmo', 1,null) }}
            <span>Máritmo</span>
        </label>
    </div>

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_aereo', 1,null) }}
            <span>Aéreo</span>
        </label>
    </div>

    <div class="checkbox checkbox-styled">
        <label>
            {{ Form::checkbox('is_rodoviario', 1,null) }}
            <span>Rodoviário</span>
        </label>
    </div>

</div>
