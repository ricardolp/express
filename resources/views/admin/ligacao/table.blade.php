
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable1" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Data</th>
                <th>Hora</th>
                <th>Duração</th>
                <th class="col-md-2">Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ligacoes as $ligacao)
                <tr class="gradeX">
                    <td>{{ $ligacao->razao_social }}</td>
                    <td>{{ $ligacao->data }}</td>
                    <td>{{ $ligacao->hora }}</td>
                    <td>{{ $ligacao->duracao }}</td>
                    <td>
                        <a href="{{ route('ligacao.edit', $ligacao->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-info">
                            <i class="fa fa-pencil-square-o"></i>
                        </a>
                        <a href="{{ route('ligacao.destroy', $ligacao->id) }}" type="button" class="btn ink-reaction btn-floating-action btn-sm btn-danger">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>