<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::select('cliente_id', $clientes, null, ['class' => 'form-control select2', 'required' => 'required']) }}
        {{ Form::label('cliente_id', 'Cliente') }}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        <div class="input-group date" id="date-expo">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <div class="input-group-content">
                {{ Form::text('data', null, ['class' => 'form-control']) }}
                {{ Form::label('data', 'Data da Ligação') }}
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('hora', null, ['class' => 'form-control time12-mask']) }}
        <label>Hora da Ligação</label>
        <p class="help-block">am/pm</p>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('duracao', null, ['class' => 'form-control time-mask']) }}
        <label>Duração da Ligação</label>
        <p class="help-block">em hh:mm</p>
    </div>
</div>

<div class="col-md-12">
    {{ Form::textarea('assunto', null, ['class' => 'form-control']) }}
    {{ Form::label('assunto', 'Assunto da Ligação') }}
</div>