@extends('template')

@section('content')



    <div class="col-lg-12"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('index') }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar ao Inicio
                </a>
            </div><!--end .tools -->
            <div class="tools" style="margin-left:25px;">
                <a href="{{ route('alfandegafronteira.create') }}" type="button" data-original-title="Adicionar Nova Alfandega/Fronteira " data-placement="left" data-toggle="tooltip" class="btn ink-reaction btn-floating-action btn-md btn-default">
                    <i class="md md-add"></i>
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-head">
                        <header>Alfandegas e Fronteiras </header>
                    </div><!--end .card-head -->
                    <div class="card-body">

                        @include('errors.messages')

                        @include('admin.alfandegafronteira.table')
                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection