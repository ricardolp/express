@extends('proposta')


@section('content')
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-printable style-default-light">
                    <div class="card-head">
                        <div class="tools">
                            <div class="btn-group">
                                <a class="btn btn-floating-action btn-primary" href="javascript:void(0);" onclick="javascript:window.print();"><i class="md md-print"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body style-default-bright">
                        <div class="row">
                            <div class="col-xs-8">
                                <h1 class="text-light"><i class="fa fa-microphone fa-fw fa-2x text-accent-dark"> </i> <strong class="text-accent-dark">TWS</strong> Acessoria em Comércio Exterior e Despacho Aduaneiro LTD</h1>
                            </div>
                            <div class="col-xs-4 text-right">
                                <h1 class="text-light text-default-light">Proposta de Serviços </h1>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-4">
                                <h4 class="text-light" id="data-client">Dados do Cliente</h4>
                                <address>
                                    <strong>{{ $proposta->razao_social }}</strong><br>
                                    {{ $proposta->endereco }} -
                                    <abbr title="Cep">CEP:</abbr> {{ $proposta->cep }} <br>
                                    {{ $proposta->bairro }} - {{ $proposta->cidade }}<br>
                                </address>
                            </div>
                            <div class="col-xs-4">
                                <h4 class="text-light">Dados de Contato</h4>
                                <address>
                                    <strong>{{ $proposta->nome }}</strong><br>
                                    {{ $proposta->email }}<br>
                                    <abbr title="Phone">P:</abbr> {{ $proposta->telefone }}
                                </address>
                            </div>
                            <div class="col-xs-4">
                                <div class="well" id="data-proposta">
                                    <div class="clearfix">
                                        <div class="pull-left"> Proposta N° : </div>
                                        <div class="pull-right"> #{{ str_pad($proposta->proposta_id, 8, "0", STR_PAD_LEFT) }} </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="pull-left"> Data da Proposta : </div>
                                        <div class="pull-right"> {{ date('d/m/Y') }} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="{{ route('cliente.proposta.submit') }}" method="post" id="form">
                                    <input type="hidden" name="proposta_id" value="{{ $proposta->proposta_id }}">

                                    <table class="table" id="table-tour">
                                        <thead>
                                        <tr>
                                            <th style="width:60px" class="text-center">#</th>
                                            <th class="text-left">Serviço</th>
                                            <th style="width:140px" class="text-right">Tipo de Cobrança</th>
                                            <th style="width:140px" class="text-right">Valor </th>
                                            <th style="width:140px" class="text-right">Valor Minimo</th>
                                            <th style="width:140px" class="text-right">Valor Máximo</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($items as $item)
                                            @if($item->is_escalonado == 0)
                                                <tr>
                                                    <td class="text-center">
                                                        <input name="{{ $item->id }}" type="checkbox">
                                                    </td>
                                                    <td>{{ $item->operacao_aberta }}</td>
                                                    <td class="text-right">{{ $item->descricao }}</td>
                                                    <td class="text-right">{{ $item->valor }}</td>
                                                    <td class="text-right"></td>
                                                    <td class="text-right"></td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="text-center">
                                                        <input name="{{ $item->id }}" type="checkbox">
                                                    </td>
                                                    <td>{{ $item->operacao_aberta }}</td>
                                                    <td class="text-right">{{ $item->descricao }}</td>
                                                    <td class="text-right"></td>
                                                    <td class="text-right"></td>
                                                    <td class="text-right"></td>
                                                </tr>

                                                @foreach($controller->escalonado($item->id)['escalonados'] as $escalonado)
                                                    <tr>
                                                        <td style="text-align:center;"></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>{{ $escalonado->valor }}</td>
                                                        <td>{{ $escalonado->valor_minimo }}</td>
                                                        <td>{{ $escalonado->valor_maximo }}</td>
                                                    </tr>
                                                @endforeach

                                            @endif

                                        @endforeach
                                        <tr>
                                            <td colspan="4" rowspan="4" id="data-obs">
                                                <h3 class="text-light opacity-50">Observaçõies</h3>
                                                <p><small>{{ $proposta->obs }}</small></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" class="btn ink-reaction btn-raised btn-primary">Confirmar Proposta</button> &nbsp;
                                        <button onclick="$('#form').submit();" type="button" class="btn ink-reaction btn-raised btn-primary">Cancelar Proposta</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection