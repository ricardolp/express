@extends('template')

@section('content')
{{ Form::open(['route' => 'proposta.store', 'id'=>'form', 'enctype' => 'multipart/form-data']) }}

<div class="col-md-12">
    <div class="card-head style-info">
        <div class="tools pull-left" style="margin-left:25px;">
            <a data-original-title="Voltar para lista de clientes" data-placement="right" data-toggle="tooltip" class="btn btn-flat hidden-xs">
                <span class="glyphicon glyphicon-arrow-left"></span>
                &nbsp;Voltar
            </a>
        </div><!--end .tools -->
    </div>
    <div class="card">
    <form role="form" action="save.php" method="post">
        <div class="card-body style-default">
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group floating-label">

                        <label for="edtCliente">Cliente:</label>
                        <input placeholder="Cliente" class="form-control input-lg static dirty" id="edtCliente"  value="{{ $cliente->razao_social }}" disabled type="text">
                        <input type="hidden" name="cliente_id" value="{{ $cliente->id }}">

                    </div>
                    <div class="form-group">
                        <div class="row col-md-12" style="margin-bottom:25px;">
                            <div class="col-md-11" style="min-width:160px;">
                                <label>Tipo de Proposta:</label>
                            </div>
                            <div class="col-md-11" style="vertical-align:middle">
                                <select class="form-control select2" id="tipo_proposta" name="tipo_proposta" value="" style="width: 100%;">
                                    <option value="Contrato">Contrato</option>
                                    <option value="Serviço Eventual">Serviço Eventual</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row col-md-12 gbtLineForm" style="padding-top:25px;margin-bottom:15px;">
                            <div class="col-md-1" style="min-width:160px;">
                                <label>Observações:</label>
                            </div>
                            <div class="col-md-11" style="vertical-align:middle">
                                <textarea name="obs" rows="5" class="form-control" placeholder="Observações" value=""></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-head style-primary">
            <ul class="nav nav-tabs tabs-text-contrast tabs-accent" data-toggle="tabs">
                <li class="active"><a href="#contact">Itens da Proposta</a></li>
            </ul>
        </div>

        <div class="card-body tab-content">
            <div class="tab-pane active" id="contact">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Opções de Serviço</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th style="width:500px;">Serviço</th>
                            <th class="col-md-3">Tipo de Cobrança</th>
                            <th style="width:100px;">Valor</th>
                            <th class="ocult-title-table">%</th>
                        </tr>
                        @foreach($servicos as $servico)
                            <tr id="row" data-id="{{ $servico->id }}">
                                <td>
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input name="operacao[]" value='{{ $servico->id }}' id='{{ $servico->id }}'  type="checkbox">
                                            <span>{{ $servico->descricao }} </span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="row col-md-12">
                                        <select class="form-control select2 operacao" id='{{ $servico->id }}-operacao'  name='{{ $servico->id }}-operacao'>
                                            <option value="0">Selecione</option>
                                            @foreach($cobrancas as $cobranca)
                                                <option value="{{ $cobranca->id }}">{{ $cobranca->descricao }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <input id='{{ $servico->id }}_valor'  name='{{ $servico->id }}_valor'  class='form-control' placeholder='Valor' >
                                        <input id='{{ $servico->id }}_escalonado'  name='{{ $servico->id }}_escalonado'  class='form-control' type='hidden'>
                                    </div>
                                </td>
                                <td class='{{ $servico->id }}-cls' style="display: table-cell;">

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!--end .tab-pane -->
            <div class="tab-pane" id="experience">
            </div><!--end .tab-pane -->
        </div><!--end .card-body.tab-content -->
        <div class="card-actionbar">
            <div class="card-actionbar-row">
                <button type="button" class="btn btn-outline btn-default">Voltar</button>
                <button type="submit" class="btn btn-info">Gravar</button>
            </div><!--end .card-actionbar-row -->
        </div><!--end .card-actionbar -->
        </form>
    </div><!--end .card -->
</div>
{{ Form::close() }}

<!-- BEGIN FORM MODAL MARKUP -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="formModalLabel">Valor Escalonado</h4>
            </div>
            <form class="form-horizontal" class="form-modal-escalonado" role="form">
                <input type="hidden" name="idEscalonado" id="idEscalonadoForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="inicioescalonado" class="control-label">Faixa Inicial</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="finicial" id="inicioescalonado" class="form-control" placeholder="Faixa Inicial">
                                </div>
                            </div>

                            <br/>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="finalescalonado" class="control-label">Faixa Final</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="ffinal" id="finalescalonado" class="form-control" placeholder="Faixa Final">
                                </div>
                            </div>

                            <br/>
                            <div class="form-group">
                                <div class="col-sm-3">
                                    <label for="valorescalonado" class="control-label">Valor</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" name="valor" id="valorescalonado" class="form-control" placeholder="Valor">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-add-escalonamento">Adicionar</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END FORM MODAL MARKUP -->
@endsection

@section('script')
    <script>
        $(".btn-add-escalonamento").click(function () {

            var id = $("#idEscalonadoForm").val();
            var tokenizer = "{'valorInicial':'" +$("#inicioescalonado").val()+"','valorFinal':'"+ $("#finalescalonado").val()+"','valor':'"+$("#valorescalonado").val()+"'},";
            var tokenizerView = "De:" +$("#inicioescalonado").val()+" - A:"+ $("#finalescalonado").val()+"- Cobrar:"+$("#valorescalonado").val()+";";


            var old = $("#"+id+"_escalonado").val();
            $("#"+id+"_escalonado").val(old + "" + tokenizer);

            var oldView = $("#"+id+"_escalonadoView").val();
            $("#"+id+"_escalonadoView").val(oldView + tokenizerView + "\n");

            $("#inicioescalonado").val("");
            $("#finalescalonado").val("");
            $("#valorescalonado").val("");

            alert("Adicionado");
            $("#formModal").modal("toggle");

        });

        $('.operacao').on('change', function(){
            var id = $(this).parent().parent().parent().data('id');
            $("."+id+"-cls").children().remove();
            var compare = $(this).val();

            if(compare != 10){
                $.get("/tipocobranca/show/"+compare)
                        .done(function( data ) {
                            console.log(data.is_value);
                            if(data.is_value == 1){
                                var html = '<div class="form-group">\
                                    <div class="col-md-6" style="vertical-align:middle">\
                                        <input id="'+id+'-ValorMinimo" name="'+id+'-ValorMinimo"  class="form-control" > \
                                        <label>Mini</label>\
                                    </div>\
                                    <div class="col-md-6" style="vertical-align:middle">\
                                        <input id="'+id+'-ValorMaximo" name="'+id+'-ValorMaximo" class="form-control" >\
                                        <label>Máx</label>\
                                    </div>\
                                </div>';
                                $("."+id+"-cls").append(html);
                            }
                        });
            } else {

                var html = '<script>' +
                                '$(".'+id+'-cls").append("<a href=\'#\' data-toggle=\'modal\' data-target=\'#formModal\'><i class=\'fa fa-plus\'></i></a>");\
                                $("#idEscalonadoForm").val('+id+');\
                            <\/script>\
                            <div class="row col-md-12">\
                                <textarea rows="2"  cols="30" id="'+id+'_escalonadoView"  name="'+id+'_escalonadoView"  class="form-control" readonly></textarea>\
                            </div>';

                $("."+id+"-cls").append(html);
            }
        });
    </script>
@endsection