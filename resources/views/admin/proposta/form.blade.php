<input type="hidden" name="cliente_id" value="{{ $cliente->id }}">

<div class="form-group floating-label">
    {{ Form::select('tipo_proposta', $tipos_proposta, null ,['class' => 'form-control','id'=>"tipo_proposta"]) }}
    {{ Form::label('tipo_proposta', 'Tipo do Proposta') }}
</div>

<div class="form-group floating-label">
    {{ Form::textarea('obs', null ,['class' => 'form-control','style'=>"height: 117px;"]) }}
    {{ Form::label('obs', 'Oservações') }}
</div>

