<input type="hidden" name="cliente_id" id="cliente_id" value="{{ $cliente->id }}">

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('cliente', $cliente->razao_social, ['class' => 'form-control', 'disabled' => 'disabled']) }}
        {{ Form::label('cliente', 'Cliente') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        <label for="">Responsável</label>
        <select name="responsavel" id="responsavel" class="select2">
            <option></option>
            @foreach($users as $user)
                <option value="{{ $user->name }}">{{ $user->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::text('local', null, ['class' => 'form-control']) }}
        {{ Form::label('local', 'local') }}
    </div>
</div>


<div class="col-md-6">
    <div class="form-group floating-label">
        <div class="input-group date" id="date-fundacao">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <div class="input-group-content">
                {{ Form::text('data', null, ['class' => 'form-control']) }}
                {{ Form::label('data', 'Data:') }}
            </div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="form-group floating-label">
        {{ Form::text('hora', null, ['class' => 'form-control']) }}
        {{ Form::label('hora', 'Hora') }}
    </div>
</div>

<div class="col-md-12">
    <div class="form-group floating-label">
        {{ Form::textarea('pauta', null, ['class' => 'form-control', "style" =>"width: 508px; height: 124px;"]) }}
        {{ Form::label('pauta', 'Pauta') }}
    </div>
</div>
