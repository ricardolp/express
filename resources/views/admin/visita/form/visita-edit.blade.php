<ul class="list-unstyled" id="experienceList">
    @foreach($participantes as $participante)
        <li class="clearfix">
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-md-8">
                        <div class="form-group" >
                            <select id="select13" class="form-control select2-list" disabled>
                                <option selected>{{ $participante->participante_visita }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <a href="{{ url('/visita/remove-participante/'.$participante->id) }}" style="color:red;">
                        <i class="fa fa-close"></i>
                        </a>
                    </div>

                </div>
            </div>
        </li>
    @endforeach
</ul>
<div class="form-group">
    <a class="btn btn-raised btn-default-bright btn-duplicated">Adicionar Participante</a>
</div><!--end .form-group -->
