@extends('template')

@section('content')
    <div class="col-lg-12"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('index') }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar ao Inicio
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-body">

                        @include('errors.messages')

                        @include('admin.relatorio.table.credenciamento')
                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script>

        var oTables = $("#datatable-columns-cliente").DataTable({
            "dom": 'lCfrtip',
            "order": [],
            columnDefs: [
            @if($table->count() > 0)
                @if($table->column_0 == 1)
                    { visible: true, targets: 0 },
                @else
                    { visible: false, targets:0 },
                @endif

                @if($table->column_1 == 1)
                    { visible: true, targets: 1 },
                @else
                    { visible: false, targets:1 },
                @endif

                @if($table->column_2 == 1)
                    { visible: true, targets: 2 },
                @else
                    { visible: false, targets:2 },
                @endif
            @endif
            ],
            "colVis": {
                "buttonText": "Colunas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable-columns-cliente').on( 'column-visibility.dt', function ( e, settings, column, state ) {

            $.post( "/table/credenciamento", { column: column, state: state })
                    .done(function( data ) {

                    });

        });

        function compare(dateTimeA, dateTimeB) {
            var momentA = moment(dateTimeA,"DD/MM/YYYY");
            var momentB = moment(dateTimeB,"DD/MM/YYYY");
            if (momentA > momentB) return 1;
            else if (momentA < momentB) return -1;
            else return 0;
        }

        $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    var vencimento = $("#data-vencimento").val();
                    var tipo       = $("#select-tipo").select2().select2('val');
                    var d1 = new Date();

                    if((vencimento == ''  || vencimento == null) && tipo == 0){
                        return true;
                    }

                    if((vencimento == ''  || vencimento == null) && tipo != 0){
                        if(tipo == data[2]){
                            return true;
                        }
                    }

                    if((vencimento != '' && vencimento != null) && tipo != 0){
                        if(tipo == data[2] && compare(vencimento,data[1]) == 1){
                            return true;
                        }
                    }

                    if((vencimento != '' && vencimento != null) && tipo == 0){
                        if(compare(vencimento,data[1]) == 1){
                            return true;
                        }
                    }

                    return false;

                }
        );

        $('.click').change( function() {
            oTables.draw();
        } );
    </script>
@endsection