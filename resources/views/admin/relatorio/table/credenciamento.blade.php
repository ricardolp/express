<div class="col-md-4">
    <div class="form-group floating-label">
        {{ Form::select('tipos', $tipos, null, ['class' => 'form-control single click', 'id' => 'select-tipo']) }}
        {{ Form::label('tipos', 'Tipo de Credenciamento') }}
    </div>
</div>

<div class="col-md-4">
    <div class="form-group floating-label">
        <div class="input-group date" id="date-expo">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <div class="input-group-content">
                {{ Form::text('vencimento', null, ['class' => 'form-control click', 'id' => 'data-vencimento']) }}
                {{ Form::label('vencimento', 'Data de Vencimento') }}
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <button type="button" class="btn ink-reaction btn-success pull-right generate-excel-credenciamento">Excel</button>
</div>

<input type="hidden" value="{{ Session::get('usuario_id') }}" id="user_id">
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable-columns-cliente" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Tipo</th>
                <th>Vencimento</th>
            </tr>
            </thead>
            <tbody>
            @foreach($credenciamentos as $credenciamento)
                <tr class="target {{ $credenciamento->id }}" data-attr="{{ $credenciamento->id }}">
                    <td>{{ $credenciamento->razao_social }}</td>
                    <td>{{ $credenciamento->vencimento }}</td>
                    <td>{{ \App\Credenciamento::itemCredenciamento($credenciamento->item_credenciamento_id) }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

