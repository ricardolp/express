
<div class="row">
    <div class="col-md-4">
        <div class="form-group floating-label">
            {{ Form::select('cliente', $clientes, null, ['class' => 'form-control single click', 'id' => 'select-clientes']) }}
            {{ Form::label('cliente', 'Cliente') }}
        </div>
    </div>
    <div class="col-md-8">
        <button type="button" class="btn ink-reaction btn-success pull-right generate-excel-ligacao">Excel</button>

    </div>
</div>
<br>
<input type="hidden" value="{{ Session::get('usuario_id') }}" id="user_id">
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable-columns-cliente" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Data</th>
                <th>Hora</th>
                <th>Duracao</th>
                <th>Assunto</th>
            </tr>
            </thead>
            <tbody>
            @foreach($ligacaos as $ligacao)
                <tr class="target" data-attr="{{ $ligacao->id }}">
                    <td>{{ $ligacao->razao_social }}</td>
                    <td>{{ $ligacao->data }}</td>
                    <td>{{ $ligacao->hora }}</td>
                    <td>{{ $ligacao->duracao }}</td>
                    <td>{{ $ligacao->assunto }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

