
<div class="row">
    <div class="col-md-12 text-center">

        <div class="btn-group" data-toggle="buttons">
            <label class="btn ink-reaction btn-primary click active">
                <input id="status" type="radio"><i class="fa fa-male fa-fw"></i> Todos
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status1" type="radio"><i class="fa fa-female fa-fw"></i> Agendadas
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status3" type="radio"><i class="fa fa-female fa-fw"></i> Realizadas
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status2" type="radio"><i class="fa fa-female fa-fw"></i> Concluídas
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status2" type="radio"><i class="fa fa-female fa-fw"></i> Canceladas
            </label>

        </div>

        <button type="button" class="btn ink-reaction btn-success pull-right generate-excel-cliente">Excel</button>
    </div>
</div>
<br>
<input type="hidden" value="{{ Session::get('usuario_id') }}" id="user_id">
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable-columns-cliente" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Responsavel</th>
                <th>Local</th>
                <th>Data</th>
                <th>Hora</th>
                <th>Pauta</th>
                <th>Participantes</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
                @foreach($visitas as $visita)
                    <tr class="target {{ $visita->id }}" data-attr="{{ $visita->id }}">
                        <td>{{ $visita->razao_social }}</td>
                        <td>{{ $visita->responsavel }}</td>
                        <td>{{ $visita->local }}</td>
                        <td>{{ $visita->data }}</td>
                        <td>{{ $visita->hora }}</td>
                        <td>{{ $visita->pauta }}</td>
                        <td>{{ $visita->participantes }}</td>
                        <td>{{ ($visita->status == 0) ? 'Agendada' : (($visita->status == 1) ? '' : 'z') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

