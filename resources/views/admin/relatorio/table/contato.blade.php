
<div class="row">
    <div class="col-md-5">
        <div class="col-md-6">
            <div class="form-group floating-label">
                {{ Form::text('aniversario', null, ['class' => 'form-control click', 'data-inputmask' => "'mask': '99/99'", 'id' => 'aniversario']) }}
                <label>Aniversário</label>
            </div>
        </div>

    </div>
    <div class="col-md-7 text-left">

        <div class="btn-group" data-toggle="buttons">
            <label class="btn ink-reaction btn-primary click active">
                <input id="status" type="radio"><i class="fa fa-male fa-fw"></i> Todos
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status1" type="radio"><i class="fa fa-female fa-fw"></i> Ativos
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status2" type="radio"><i class="fa fa-female fa-fw"></i> Inativos
            </label>
        </div>

        <button type="button" class="btn ink-reaction btn-success pull-right generate-excel-contato">Excel</button>
    </div>
</div>
<br>
<input type="hidden" value="{{ Session::get('usuario_id') }}" id="user_id">
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable-columns-cliente" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Nome</th>
                <th>Departamento</th>
                <th>Função</th>
                <th>Telefone</th>
                <th>Celular</th>
                <th>Aniversário</th>
                <th>E-Mail</th>
                <th>Status do Cliente</th>
                <th>Status do Contato</th>
                <th>Razão Social</th>
                <th>CNPJ</th>
            </tr>
            </thead>
            <tbody>
            @foreach($contatos as $contato)
                <tr class="target" data-attr="{{ $contato->id }}">
                    <td>{{ $contato->nome }}</td>
                    <td>{{ $contato->departamento_id }}</td>
                    <td>{{ $contato->funcao_id }}</td>
                    <td>{{ $contato->telefone }}</td>
                    <td>{{ $contato->celular }}</td>
                    <td>{{ $contato->aniversario }}</td>
                    <td>{{ $contato->email }}</td>
                    <td>{{  ($contato->is_status == 0) ? 'Inativo' : (($contato->is_status == 1) ? 'Prospect' : 'Ativo') }}</td>
                    <td>{{ ($contato->status == 1) ? 'Ativo' : 'Inativo' }}</td>
                    <td>{{ $contato->razao_social }}</td>
                    <td>{{ $contato->cnpj }}</td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
</div>

