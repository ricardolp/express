
<div class="row">
    <div class="col-md-12 text-center">

        <div class="btn-group" data-toggle="buttons">
            <label class="btn ink-reaction btn-primary click active">
                <input id="status" type="radio"><i class="fa fa-male fa-fw"></i> Todos
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status1" type="radio"><i class="fa fa-female fa-fw"></i> Ativos
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status3" type="radio"><i class="fa fa-female fa-fw"></i> Inativo
            </label>
            <label class="btn ink-reaction btn-primary click">
                <input id="status2" type="radio"><i class="fa fa-female fa-fw"></i> Prospect
            </label>

        </div>

        <button type="button" class="btn ink-reaction btn-success pull-right generate-excel-cliente">Excel</button>
    </div>
</div>
<br>
<input type="hidden" value="{{ Session::get('usuario_id') }}" id="user_id">
<div class="col-lg-12">
    <div class="table-responsive">
        <table id="datatable-columns-cliente" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Razão Social</th>
                <th>CNPJ </th>
                <th>Telefone </th>
                <th>Fundação </th>
                <th>Status </th>
            </tr>
            </thead>
            <tbody>
            @foreach($clientes as $cliente)
                <tr class="gradeX target" data-attr="{{ $cliente->id }}">
                    <td>{{ $cliente->razao_social }}</td>
                    <td>{{ $cliente->cnpj }}</td>
                    <td>{{ $cliente->telefone }}</td>
                    <td>{{ $cliente->fundacao }}</td>
                    <td>{{ ($cliente->is_status == 1) ? 'Prospect' : (($cliente->is_status == 2) ? 'Ativo' : 'Inativo') }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

