
@extends('template')

@section('content')
    <div class="col-lg-12"><br><br>
        <div class="card-head style-info">
            <div class="tools pull-left" style="margin-left:25px;">
                <a data-original-title="Voltar ao inicio da dashboard" data-placement="left" data-toggle="tooltip" class="btn btn-flat hidden-xs" href="{{ route('index') }}">
                    <span class="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; Voltar ao Inicio
                </a>
            </div><!--end .tools -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-bordered">
                    <div class="card-body">

                        @include('errors.messages')

                        @include('admin.relatorio.table.contato')
                    </div><!--end .card-body -->
                </div>
            </div>
        </div>
    </div>

@endsection


@section('script')
    <script>
        var $v = $(".select2").select2();
        $v.val(null).trigger("change");
        var oTables = $("#datatable-columns-cliente").DataTable({
            "dom": 'lCfrtip',
            "order": [],
            columnDefs: [
                @if($table->count() > 0)
                    @if($table->column_0 == 1)
                        { visible: true, targets: 0 },
                    @else
                        { visible: false, targets:0 },
                    @endif

                    @if($table->column_1 == 1)
                        { visible: true, targets: 1 },
                    @else
                        { visible: false, targets:1 },
                    @endif

                    @if($table->column_2 == 1)
                        { visible: true, targets: 2 },
                    @else
                        { visible: false, targets:2 },
                    @endif

                    @if($table->column_3 == 1)
                        { visible: true, targets: 3 },
                    @else
                        { visible: false, targets:3 },
                    @endif

                    @if($table->column_4 == 1)
                        { visible: true, targets: 4 },
                    @else
                        { visible: false, targets:4 },
                    @endif

                    @if($table->column_5 == 1)
                        { visible: true, targets: 5 },
                    @else
                        { visible: false, targets:5 },
                    @endif

                    @if($table->column_6 == 1)
                        { visible: true, targets: 6 },
                    @else
                        { visible: false, targets:6 },
                    @endif

                    @if($table->column_7 == 1)
                        { visible: true, targets: 7 },
                    @else
                        { visible: false, targets:7 },
                    @endif

                    @if($table->column_8 == 1)
                        { visible: true, targets: 8 },
                    @else
                        { visible: false, targets:8 },
                    @endif

                    @if($table->column_9 == 1)
                        { visible: true, targets: 9 },
                    @else
                        { visible: false, targets:9 },
                    @endif

                    @if($table->column_10 == 1)
                        { visible: true, targets: 10 },
                    @else
                        { visible: false, targets:10 },
                    @endif
                @endif
            ],
            "colVis": {
                "buttonText": "Colunas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": '_MENU_ entries per page',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable-columns-cliente').on( 'column-visibility.dt', function ( e, settings, column, state ) {

            $.post( "/table/contato", { column: column, state: state })
                    .done(function( data ) {

                    });

        });


        $.fn.dataTable.ext.search.push(
                function( settings, data, dataIndex ) {
                    var data_an = $("#aniversario").val();
                    if($("#status2").parent().hasClass('active')){
                        status = 'Inativo';
                    }

                    if($("#status1").parent().hasClass('active')){
                        status = 'Ativo';
                    }

                    if($("#status").parent().hasClass('active')){
                        status = '';
                    }

                    if (( status == data[8] || status == '' || status == null) && (data_an == '' || data_an == null)){
                        return true;
                    }

                    if(( status == data[8] || status == '' || status == null) && data_an == data[5]){
                        return true;
                    }

                    return false;
                }
        );

        $('.click').on('change click', function() {
            oTables.draw();
        });
    </script>
@endsection