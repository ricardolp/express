<!DOCTYPE html>
<html lang="en">
<head>
    <title> TWS - Express</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/bootstrap.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/materialadmin.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/font-awesome.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/material-design-iconic-font.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/rickshaw/rickshaw.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/morris/morris.core.css') }}" />
    <!-- END STYLESHEETS -->
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/select2/select2.css') }}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/DataTables/jquery.dataTables.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/DataTables/extensions/dataTables.colVis.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/DataTables/extensions/dataTables.tableTools.css') }}" />

    <link type="text/css" rel="stylesheet" href="{{ asset('/assets/css/theme-default/libs/fullcalendar/fullcalendar.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('/tour/css/bootstrap-tour.min.css') }}" />


    <!-- BEGIN JAVASCRIPT -->
    <script src="{{ asset('/assets/js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('/assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
    <script src="{{ asset('/tour/js/bootstrap-tour-standalone.min.js') }}"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <![endif]-->
</head>
<body class="menubar-hoverable header-fixed ">

<!-- BEGIN HEADER-->
<header id="header" >
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li>

                </li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="headerbar-right">
            <ul class="header-nav header-nav-profile">
                <li class="dropdown">

                </li><!--end .dropdown -->
            </ul><!--end .header-nav-profile -->
        </div><!--end #header-navbar-collapse -->
    </div>
</header>
<!-- END HEADER-->

<!-- BEGIN BASE-->
<div id="base">
    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
            @yield('content')
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->
</div><!--end #base-->
<!-- END BASE -->

<script src="{{ asset('/assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>

<script>


    $(document).ready(function(){
        // Instance the tour
        var tour = new Tour({
            steps: [
                {
                    element: "#data-client",
                    title: "Seus Dados",
                    content: "Nesta Área você visualiza os seus dados e o contato no qual foi direcionado a proposta"
                },
                {
                    element: "#table-tour",
                    title: "Tabela de Serviços",
                    content: "Tabela contento os itens da proposta, nela, você irá selecionar os itens da proposta que está de acordo. Não esqueçã de confirmar os valores."
                },
                {
                    element: "#data-obs",
                    title: "Observações",
                    content: "Fique Atendo tambem as observações, aqui podem conter informações importantes da proposta"
                },
                {
                    element: "#button-tour",
                    title: "Finalizar",
                    content: "Após confirmar as informações é só finalizar sua proposta clicando em Aceitar ou REcusar."
                }
            ]});

        // Initialize the tour
        tour.init();

        tour.start(true);
    });

</script>
</body>
</html>
