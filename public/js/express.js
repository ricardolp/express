$('#cep').change(function(){
    var cep = $('#cep').val();
    $.ajax({
        type      : 'get',
        url       : 'http://apps.widenet.com.br/busca-cep/api/cep.json?',
        data      : 'code='+cep,
        beforeSend : function (){
            $('.loader').toggle();
            setTimeout(function(){
                $('.loader').toggle();
            }, 500);
        },
        success : function(txt){
            if(txt.status == 1){
                $('#endereco').val(txt.address);
                $('#bairro').val(txt.district);
                $('#cidade').val(txt.city);
                $('#uf').val(txt.state);
                $('#pais').val('Brasil');
            } else {
                alert(txt.message);
            }
        }
    });
});

$(".item-credenciamento").on('change', function (){
    itemFile();
});

$(".remarcar").on('click', function(){
    var id = $(this).data('id');
    $(".hidden").append('<input type="hidden" name="id" value="'+id+'">');
    $("#formModal").modal('toggle');
});

$(".cancelar").on('click', function(){
    var id = $(this).data('id');
    $(".hidden-visita").append('<input type="hidden" name="id" value="'+id+'">');
    $("#modalCancel").modal('toggle');
});

$('.credenciamento-show').on('click', function (){
    var id = $(this).data('id');
    $.get( "/credenciamento/show/"+id)
        .done(function( data ) {
            $("#credenciamento-button").children().remove();
            console.log(data)
            $("#credenciamento-data").text(data.data)
            $("#credenciamento-vencimento").text(data.vencimento)
            $("#credenciamento-button").append('<a href="'+data.file+'" target="_blank" class="btn btn-primary"><i class="fa fa-eye"></i> Visualizar</a>');
            $("#credenciamento-label").text(data.label)
            $("#modalCredenciamento").modal('toggle');
        });
});

$('.operacao-show').on('click', function (){
    var id = $(this).data('id');
    $.get( "/operacao/show/"+id)
        .done(function( data ) {
            console.log(data)
            $("#modalOperacao").modal('toggle');

            $("#operacao-tipo").text((data.tipo_operacao == 1) ? 'Importação' : 'Exportação');
            $("#operacao-previsao").text(data.previsao);
            $("#operacao-finalidade").text(data.finalidade);
            $("#operacao-origem").text(data.origem);
            $("#operacao-tempo").text(data.tempo_importacao);
            $("#operacao-fornecedor").text(data.fornecedor);
            $("#operacao-dificultade").text(data.dificuldade);
            $("#operacao-comentario").text(data.comentario);
        });
});

$(".changed").on('change', function (){
    $(".incoterm").select2("destroy");
    $(".incoterm").children().remove();
    var operacao = $("#tipo-operacao").val();
    var maritmo = ($("#is-maritmo").attr('checked') == 'checked') ? 1 : 0;
    var aereo = ($("#is-aereo").attr('checked') == 'checked') ? 1 : 0;
    var rodoviario = ($("#is-rodoviario").attr('checked') == 'checked') ? 1 : 0;

    $.post( "/incoterm/show", { operacao: operacao, maritmo: maritmo, aereo: aereo, rodoviario: rodoviario})
        .done(function( data ) {
            $.each(data, function( index, value ) {
                $(".incoterm").append('<option value="'+value.id+'">'+value.descricao+'</option>');
            });


            $(".incoterm").select2();
        });
});

function itemFile()
{
    var id = $(".item-credenciamento").val();
    $.get( "/itemcredenciamento/isfile/"+id)
        .done(function( data ) {
            if(data.success == true){
                $(".is-file").css('display', 'block');
            } else {
                $(".is-file").css('display', 'none');
            }
        });
}

function checkPerfil()
{
    var val = $("#icms").attr('checked');

    if(val == 'checked'){
        $(".icms-regime").css('display', 'block');
    } else {
        $(".icms-regime").css('display', 'none');
    }

    var val = $(".orgao_anuente").val();

    if(val == 'Outros'){
        $(".orgao-outro").css('display', 'block');
    } else {
        $(".orgao-outro").css('display', 'none');
    }

    var val = $("#is_licenca_importacao").attr('checked');

    if(val == 'checked'){
        $('.orgao-anuente').css('display', 'block');
    } else {
        $('.orgao-anuente').css('display', 'none');
    }

    var val = $("#is_imposto_federal").attr('checked');

    if(val == 'checked'){
        $('.imposto-beneficio').css('display', 'block');
    } else {
        $('.imposto-beneficio').css('display', 'none');
    }

}

$(document).ready(function (){
    itemFile()
});

$(".select2").select2();

$(".orgao_anuente").on('change', function () {
    var val = $(".orgao_anuente").val();

    if(val == 'Outros'){
        $(".orgao-outro").css('display', 'block');
    } else {
        $(".orgao-outro").css('display', 'none');
    }

});


$("#icms").on('change', function () {
    var val = $("#icms").attr('checked');

    if(val == 'checked'){
        $(".icms-regime").css('display', 'block');
    } else {
        $(".icms-regime").css('display', 'none');
    }

});

$("#is_licenca_importacao").on('change', function (){
    var val = $("#is_licenca_importacao").attr('checked');

    if(val == 'checked'){
        $('.orgao-anuente').css('display', 'block');
    } else {
        $('.orgao-anuente').css('display', 'none');
    }

});

$("#is_imposto_federal").on('change', function (){
    var val = $("#is_imposto_federal").attr('checked');

    if(val == 'checked'){
        $('.imposto-beneficio').css('display', 'block');
    } else {
        $('.imposto-beneficio').css('display', 'none');
    }
});

$(".generate-excel-cliente").on('click', function(){
    if($("#status3").parent().hasClass('active')){
        status_par = '0';
    }

    if($("#status2").parent().hasClass('active')){
        status_par = '1';
    }

    if($("#status1").parent().hasClass('active')){
        status_par = '2';
    }

    if($("#status").parent().hasClass('active')){
        status_par = 'x';
    }

    var param = '';

    $(".target").each(function() {
        param += '-'+$(this).data("attr");
    });

    console.log(param)
    var id = $("#user_id").val();

    window.open("/relatorio/cliente/excel/"+id+"/"+status_par+'/'+param,'_blank');

});

$(".generate-excel-contato").on('click', function(){
    if($("#status2").parent().hasClass('active')){
        status_par = '2';
    }

    if($("#status1").parent().hasClass('active')){
        status_par = '1';
    }

    if($("#status").parent().hasClass('active')){
        status_par = 'x';
    }

    var param = '';

    $(".target").each(function() {
        param += '-'+$(this).data("attr");
    });

    console.log(param)
    var id = $("#user_id").val();

    window.open("/relatorio/contato/excel/"+id+"/"+status_par+'/'+param,'_blank');

});

$(".generate-excel-credenciamento").on('click', function(){

    var param = '';

    $(".target").each(function() {
        param += '-'+$(this).data("attr");
    });

    console.log(param)
    var id = $("#user_id").val();

    window.open("/relatorio/credenciamento/excel/"+id+'/'+param,'_blank');
});

$(".generate-excel-ligacao").on('click', function(){

    var param = '';

    $(".target").each(function() {
        param += '-'+$(this).data("attr");
    });

    console.log(param)
    var id = $("#user_id").val();

    window.open("/relatorio/ligacao/excel/"+id+'/'+param,'_blank');
});

$(".showdata").on('click', function(){
    var proposta_id = $(this).data('id');
    $("#div-dados-proposta").children().remove();
    $.get( "/proposta/view/"+proposta_id)
        .done(function( data ) {
            $("#viewproposta").modal('toggle');

            $("#div-dados-proposta").append(data.html);
        });
});


$(".sendorder").on('click', function(){
    var proposta_id = $(this).data('id');

    $.get( "/contato/show/"+proposta_id)
        .done(function( data ) {
            html = '<input type="hidden" name="id" value="'+proposta_id+'">';
            html += ' <div class="form-group"><select class="form-control select2" name="contato_id">';

            $.each(data, function( index, value ) {
                html += '<option value="'+value.id+'">'+value.nome+'</option>';
            });

            html += '</select></div>';

            $("#div-dados").append(html);
            $(".select2").select2();
        });
});



function setContato(id)
{
    $.get( "/contato/set/"+ id )
        .done(function( data ) {
           location.reload();
        });
}

$(".btn-duplicated").on('click', function(){
    var id = $("#cliente_id").val();

    var html = '<li class="clearfix">\
                <div class="row">\
                <div class="col-sm-12">\
                <div class="form-group" >\
                <select id="select13" name="participantes[]" class="form-control select2">';

    $.get( "/contato/all/"+id)
        .done(function( data ) {
            $("#experienceList").append(data.html);
        });
});

$(".view-visita").on('click', function(){
    $(".view-visita").children().remove();
    var id = $(this).data('id');

    $.get( "/visita/view/"+id)
        .done(function( data ) {
            $("#visitaView").modal('toggle');
            $(".view-visita").append(data.html);
        });

});

$(".btn-filtro").on('click', function(){
    $("#accordion-services").children().remove();
    $(".loader").css('display', 'block');
    var start = $("#start").val();
    var end = $('#end').val();
    var razao_social = $("#razao_social").val();

    $.post( "/relatorio/get/cliente", {start:start, end:end, razao_social:razao_social})
        .done(function( data ) {
            $(".loader").css('display', 'none');
            $("#accordion-services").append(data.html);

        });
});

$("#tipo_indicacao").on('change', function(){
    var val = $("#tipo_indicacao").val();

    if(val == 0) {
      $(".cliente-lista").css('display', 'block');
      $(".parceiro-lista").css('display', 'none');
      $(".equipe-lista").css('display', 'none');
    }
    
    if(val == 1) {
        $(".cliente-lista").css('display', 'none');
        $(".parceiro-lista").css('display', 'block');
        $(".equipe-lista").css('display', 'none');
    }

    if(val == 2) {
        $(".cliente-lista").css('display', 'none');
        $(".parceiro-lista").css('display', 'none');
        $(".equipe-lista").css('display', 'block');
    }

    if(val == 3) {
        $(".cliente-lista").css('display', 'none');
        $(".parceiro-lista").css('display', 'none');
        $(".equipe-lista").css('display', 'none');
    }
    
});