<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscalonadoItemPropostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('escalonado_item_propostas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('valor');
            $table->string('valor_maximo');
            $table->string('valor_minimo');
            $table->integer('id_item_proposta')->unsigned();
            $table->foreign('id_item_proposta')->references('id')->on('item_propostas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('escalonado_item_propostas');
    }
}
