<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::post("/login", ['as' => 'express.login', 'uses'=>'ExpressController@login']);
Route::get("/login", ['as' => 'login', 'uses'=>'ExpressController@getView']);
Route::get('/get-events', ['as' => 'events', 'uses'=>'ExpressController@events']);
Route::get('/get/event/visita/{id}', ['as' => 'events.visita', 'uses'=>'ExpressController@visita']);
Route::get('/get/event/credenciamento/{id}', ['as' => 'events.visita', 'uses'=>'ExpressController@credenciamento']);

Route::group(['middleware' => 'check'], function (){

    Route::get('/', ['as' => 'index', 'uses'=>'ExpressController@index']);
	
	    Route::get('/;', ['as' => 'index', 'uses'=>'ExpressController@index']);



    Route::get("/funcao", ['as' => 'funcao.index', 'uses'=>'FuncaoController@index']);
    Route::get("/funcao/create", ['as' => 'funcao.create', 'uses'=>'FuncaoController@create']);
    Route::get("/funcao/edit/{id}", ['as' => 'funcao.edit', 'uses'=>'FuncaoController@edit']);
    Route::post("/funcao/store", ['as' => 'funcao.store', 'uses'=>'FuncaoController@store']);
    Route::post("/funcao/update/{id}", ['as' => 'funcao.update', 'uses'=>'FuncaoController@update']);
    Route::get("/funcao/destroy/{id}", ['as' => 'funcao.destroy', 'uses'=>'FuncaoController@destroy']);

    Route::get("/departamento", ['as' => 'departamento.index', 'uses'=>'DepartamentoController@index']);
    Route::get("/departamento/create", ['as' => 'departamento.create', 'uses'=>'DepartamentoController@create']);
    Route::get("/departamento/edit/{id}", ['as' => 'departamento.edit', 'uses'=>'DepartamentoController@edit']);
    Route::post("/departamento/store", ['as' => 'departamento.store', 'uses'=>'DepartamentoController@store']);
    Route::post("/departamento/update/{id}", ['as' => 'departamento.update', 'uses'=>'DepartamentoController@update']);
    Route::get("/departamento/destroy/{id}", ['as' => 'departamento.destroy', 'uses'=>'DepartamentoController@destroy']);

    Route::get("/alfandegafronteira", ['as' => 'alfandegafronteira.index', 'uses'=>'AlfandegaFronteiraController@index']);
    Route::get("/alfandegafronteira/create", ['as' => 'alfandegafronteira.create', 'uses'=>'AlfandegaFronteiraController@create']);
    Route::get("/alfandegafronteira/edit/{id}", ['as' => 'alfandegafronteira.edit', 'uses'=>'AlfandegaFronteiraController@edit']);
    Route::post("/alfandegafronteira/store", ['as' => 'alfandegafronteira.store', 'uses'=>'AlfandegaFronteiraController@store']);
    Route::post("/alfandegafronteira/update/{id}", ['as' => 'alfandegafronteira.update', 'uses'=>'AlfandegaFronteiraController@update']);
    Route::get("/alfandegafronteira/destroy/{id}", ['as' => 'alfandegafronteira.destroy', 'uses'=>'AlfandegaFronteiraController@destroy']);

    Route::get("/tipocobranca", ['as' => 'tipocobranca.index', 'uses'=>'TipoCobrancaController@index']);
    Route::get("/tipocobranca/create", ['as' => 'tipocobranca.create', 'uses'=>'TipoCobrancaController@create']);
    Route::get("/tipocobranca/edit/{id}", ['as' => 'tipocobranca.edit', 'uses'=>'TipoCobrancaController@edit']);
    Route::get("/tipocobranca/show/{id}", ['as' => 'tipocobranca.show', 'uses'=>'TipoCobrancaController@show']);
    Route::post("/tipocobranca/store", ['as' => 'tipocobranca.store', 'uses'=>'TipoCobrancaController@store']);
    Route::post("/tipocobranca/update/{id}", ['as' => 'tipocobranca.update', 'uses'=>'TipoCobrancaController@update']);
    Route::get("/tipocobranca/destroy/{id}", ['as' => 'tipocobranca.destroy', 'uses'=>'TipoCobrancaController@destroy']);

    Route::get("/tiposervico", ['as' => 'tiposervico.index', 'uses'=>'TipoServicoController@index']);
    Route::get("/tiposervico/create", ['as' => 'tiposervico.create', 'uses'=>'TipoServicoController@create']);
    Route::get("/tiposervico/edit/{id}", ['as' => 'tiposervico.edit', 'uses'=>'TipoServicoController@edit']);
    Route::post("/tiposervico/store", ['as' => 'tiposervico.store', 'uses'=>'TipoServicoController@store']);
    Route::post("/tiposervico/update/{id}", ['as' => 'tiposervico.update', 'uses'=>'TipoServicoController@update']);
    Route::get("/tiposervico/destroy/{id}", ['as' => 'tiposervico.destroy', 'uses'=>'TipoServicoController@destroy']);

    Route::get("/incoterm", ['as' => 'incoterm.index', 'uses'=>'IncotermController@index']);
    Route::get("/incoterm/create", ['as' => 'incoterm.create', 'uses'=>'IncotermController@create']);
    Route::get("/incoterm/edit/{id}", ['as' => 'incoterm.edit', 'uses'=>'IncotermController@edit']);
    Route::get("/incoterm/destroy/{id}", ['as' => 'incoterm.destroy', 'uses'=>'IncotermController@destroy']);
    Route::post("/incoterm/store", ['as' => 'incoterm.store', 'uses'=>'IncotermController@store']);
    Route::post("/incoterm/update/{id}", ['as' => 'incoterm.update', 'uses'=>'IncotermController@update']);
    Route::post("/incoterm/show", ['as' => 'incoterm.show', 'uses'=>'IncotermController@show']);

    Route::get("/parceiro", ['as' => 'parceiro.index', 'uses'=>'ParceiroController@index']);
    Route::get("/parceiro/create", ['as' => 'parceiro.create', 'uses'=>'ParceiroController@create']);
    Route::get("/parceiro/edit/{id}", ['as' => 'parceiro.edit', 'uses'=>'ParceiroController@edit']);
    Route::get("/parceiro/destroy/{id}", ['as' => 'parceiro.destroy', 'uses'=>'ParceiroController@destroy']);
    Route::post("/parceiro/store", ['as' => 'parceiro.store', 'uses'=>'ParceiroController@store']);
    Route::post("/parceiro/update/{id}", ['as' => 'parceiro.update', 'uses'=>'ParceiroController@update']);
    Route::post("/parceiro/show", ['as' => 'parceiro.show', 'uses'=>'ParceiroController@show']);

    Route::get("/itemcredenciamento", ['as' => 'itemcredenciamento.index', 'uses'=>'ItemCredenciamentoController@index']);
    Route::get("/itemcredenciamento/create", ['as' => 'itemcredenciamento.create', 'uses'=>'ItemCredenciamentoController@create']);
    Route::get("/itemcredenciamento/edit/{id}", ['as' => 'itemcredenciamento.edit', 'uses'=>'ItemCredenciamentoController@edit']);
    Route::post("/itemcredenciamento/store", ['as' => 'itemcredenciamento.store', 'uses'=>'ItemCredenciamentoController@store']);
    Route::post("/itemcredenciamento/update/{id}", ['as' => 'itemcredenciamento.update', 'uses'=>'ItemCredenciamentoController@update']);
    Route::get("/itemcredenciamento/destroy/{id}", ['as' => 'itemcredenciamento.destroy', 'uses'=>'ItemCredenciamentoController@destroy']);
    Route::get("/itemcredenciamento/isfile/{id}", ['as' => 'itemcredenciamento.isfile', 'uses'=>'ItemCredenciamentoController@isFile']);

    Route::get("/seguro/create/{id}", ['as' => 'seguro.create', 'uses'=>'SeguroController@create']);
    Route::get("/seguro/edit/{id}", ['as' => 'seguro.edit', 'uses'=>'SeguroController@edit']);
    Route::post("/seguro/store", ['as' => 'seguro.store', 'uses'=>'SeguroController@store']);
    Route::post("/seguro/update/{id}", ['as' => 'seguro.update', 'uses'=>'SeguroController@update']);

    Route::get("/direct/create/{id}", ['as' => 'direct.create', 'uses'=>'DirectController@create']);
    Route::get("/direct/edit/{id}", ['as' => 'direct.edit', 'uses'=>'DirectController@edit']);
    Route::post("/direct/store", ['as' => 'direct.store', 'uses'=>'DirectController@store']);
    Route::post("/direct/update/{id}", ['as' => 'direct.update', 'uses'=>'DirectController@update']);

    Route::get("/proposta/create/{id}", ['as' => 'proposta.create', 'uses'=>'PropostaController@create']);
    Route::get("/proposta/edit/{id}", ['as' => 'proposta.edit', 'uses'=>'PropostaController@edit']);
    Route::get("/proposta/view/{id}", ['as' => 'proposta.view', 'uses'=>'PropostaController@view']);
    Route::get("/proposta/view/itens/{id}", ['as' => 'proposta.view', 'uses'=>'PropostaController@itens']);
    Route::get("/proposta/view/escalonado/{id}", ['as' => 'proposta.escalonado', 'uses'=>'PropostaController@escalonado']);
    Route::post("/proposta/send/", ['as' => 'proposta.send', 'uses'=>'PropostaController@send']);
    Route::post("/proposta/store", ['as' => 'proposta.store', 'uses'=>'PropostaController@store']);
    Route::post("/proposta/update/{id}", ['as' => 'proposta.update', 'uses'=>'PropostaController@update']);

    Route::get("/cliente", ['as' => 'cliente.index', 'uses'=>'ClienteController@index']);
    Route::get("/cliente/create", ['as' => 'cliente.create', 'uses'=>'ClienteController@create']);
    Route::get("/cliente/edit/{id}", ['as' => 'cliente.edit', 'uses'=>'ClienteController@edit']);
    Route::get("/cliente/status/{id}", ['as' => 'cliente.status', 'uses'=>'ClienteController@status']);
    Route::get("/cliente/view/{id}", ['as' => 'cliente.view', 'uses'=>'ClienteController@view']);
    Route::post("/cliente/store", ['as' => 'cliente.store', 'uses'=>'ClienteController@store']);
    Route::post("/cliente/update/{id}", ['as' => 'cliente.update', 'uses'=>'ClienteController@update']);
    Route::get("/cliente/destroy/{id}", ['as' => 'cliente.destroy', 'uses'=>'ClienteController@destroy']);
    Route::get("/cliente/proposta/{id}", ['as' => 'cliente.proposta', 'uses'=>'PropostaController@show']);
    Route::post("/cliente/proposta/submit", ['as' => 'cliente.proposta.submit', 'uses'=>'PropostaController@submit']);

    Route::get("/contato", ['as' => 'contato.index', 'uses'=>'ContatoController@index']);
    Route::get("/contato/create/{cliente_id}", ['as' => 'contato.create', 'uses'=>'ContatoController@create']);
    Route::get("/contato/edit/{id}", ['as' => 'contato.edit', 'uses'=>'ContatoController@edit']);
    Route::get("/contato/set/{id}", ['as' => 'contato.set', 'uses'=>'ContatoController@set']);
    Route::get("/contato/all/{id}", ['as' => 'contato.all', 'uses'=>'ContatoController@all']);
    Route::get("/contato/show/{id}", ['as' => 'contato.set', 'uses'=>'ContatoController@show']);
    Route::post("/contato/store", ['as' => 'contato.store', 'uses'=>'ContatoController@store']);
    Route::post("/contato/update/{id}", ['as' => 'contato.update', 'uses'=>'ContatoController@update']);
    Route::get("/contato/destroy/{id}", ['as' => 'contato.destroy', 'uses'=>'ContatoController@destroy']);

    Route::get("/endereco", ['as' => 'endereco.index', 'uses'=>'EnderecoController@index']);
    Route::get("/endereco/create/{cliente_id}", ['as' => 'endereco.create', 'uses'=>'EnderecoController@create']);
    Route::get("/endereco/edit/{id}", ['as' => 'endereco.edit', 'uses'=>'EnderecoController@edit']);
    Route::post("/endereco/store", ['as' => 'endereco.store', 'uses'=>'EnderecoController@store']);
    Route::post("/endereco/update/{id}", ['as' => 'endereco.update', 'uses'=>'EnderecoController@update']);
    Route::get("/endereco/destroy/{id}", ['as' => 'endereco.destroy', 'uses'=>'EnderecoController@destroy']);

    Route::get("/credenciamento", ['as' => 'credenciamento.index', 'uses'=>'CredenciamentoController@index']);
    Route::get("/credenciamento/create/{cliente_id}", ['as' => 'credenciamento.create', 'uses'=>'CredenciamentoController@create']);
    Route::get("/credenciamento/edit/{id}", ['as' => 'credenciamento.edit', 'uses'=>'CredenciamentoController@edit']);
    Route::post("/credenciamento/store", ['as' => 'credenciamento.store', 'uses'=>'CredenciamentoController@store']);
    Route::post("/credenciamento/update/{id}", ['as' => 'credenciamento.update', 'uses'=>'CredenciamentoController@update']);
    Route::get("/credenciamento/show/{id}", ['as' => 'credenciamento.show', 'uses'=>'CredenciamentoController@show']);

    Route::get("/visita", ['as' => 'visita.index', 'uses'=>'VisitaController@index']);
    Route::get("/visita/view/{id}", ['as' => 'visita.view', 'uses'=>'VisitaController@view']);
    Route::get("/visita/create/{cliente_id}", ['as' => 'visita.create', 'uses'=>'VisitaController@create']);
    Route::get("/visita/edit/{id}", ['as' => 'visita.edit', 'uses'=>'VisitaController@edit']);
    Route::post("/visita/store", ['as' => 'visita.store', 'uses'=>'VisitaController@store']);
    Route::post("/visita/reagendar", ['as' => 'visita.reagendar', 'uses'=>'VisitaController@reagendar']);
    Route::get("/visita/confirmar/{id}", ['as' => 'visita.confirmar', 'uses'=>'VisitaController@confirmar']);
    Route::post("/visita/cancelar", ['as' => 'visita.cancelar', 'uses'=>'VisitaController@cancelar']);
    Route::post("/visita/update/{id}", ['as' => 'visita.update', 'uses'=>'VisitaController@update']);
    Route::get("/visita/destroy/{id}", ['as' => 'visita.destroy', 'uses'=>'VisitaController@destroy']);
    Route::get("/visita/relatorio/{id}/{cliente}", ['as' => 'visita.relatorio', 'uses'=>'VisitaController@relatorio']);
    Route::post("/visita/submit/relatorio", ['as' => 'visita.submit.relatorio', 'uses'=>'VisitaController@submit']);
    Route::get("/visita/remove-participante/{id}", ['as' => 'visita.remove.participante', 'uses'=>'VisitaController@removeParticipante']);

    Route::get("/operacao", ['as' => 'operacao.index', 'uses'=>'OperacaoController@index']);
    Route::get("/operacao/create/{cliente_id}", ['as' => 'operacao.create', 'uses'=>'OperacaoController@create']);
    Route::get("/operacao/edit/{id}", ['as' => 'operacao.edit', 'uses'=>'OperacaoController@edit']);
    Route::post("/operacao/store", ['as' => 'operacao.store', 'uses'=>'OperacaoController@store']);
    Route::post("/operacao/update/{id}", ['as' => 'operacao.update', 'uses'=>'OperacaoController@update']);
    Route::get("/operacao/show/{id}", ['as' => 'operacao.show', 'uses'=>'OperacaoController@show']);
    Route::get("/operacao/destroy/{id}", ['as' => 'operacao.destroy', 'uses'=>'OperacaoController@destroy']);

    Route::get("/operacao/incoterm/{id}", ['as' => 'operacao.incoterm', 'uses'=>'OperacaoIncotermController@incoterm']);

    Route::get("/alfandega/cliente", ['as' => 'alfandega.cliente.index', 'uses'=>'ClienteAlfandegaController@index']);
    Route::get("/alfandega/cliente/create/{cliente_id}", ['as' => 'alfandega.cliente.create', 'uses'=>'ClienteAlfandegaController@create']);
    Route::get("/alfandega/cliente/edit/{id}", ['as' => 'alfandega.cliente.edit', 'uses'=>'ClienteAlfandegaController@edit']);
    Route::post("/alfandega/cliente/store", ['as' => 'alfandega.cliente.store', 'uses'=>'ClienteAlfandegaController@store']);
    Route::post("/alfandega/cliente/update/{id}", ['as' => 'alfandega.cliente.update', 'uses'=>'ClienteAlfandegaController@update']);
    Route::get("/alfandega/cliente/show/{id}", ['as' => 'alfandega.cliente.show', 'uses'=>'ClienteAlfandegaController@show']);
    Route::get("/alfandega/cliente/destroy/{id}", ['as' => 'alfandega.cliente.destroy', 'uses'=>'ClienteAlfandegaController@destroy']);

    Route::get("/perfil", ['as' => 'perfil.index', 'uses'=>'InformacaoPerfilController@index']);
    Route::get("/perfil/create/{cliente_id}", ['as' => 'perfil.create', 'uses'=>'InformacaoPerfilController@create']);
    Route::get("/perfil/edit/{id}", ['as' => 'perfil.edit', 'uses'=>'InformacaoPerfilController@edit']);
    Route::post("/perfil/store", ['as' => 'perfil.store', 'uses'=>'InformacaoPerfilController@store']);
    Route::post("/perfil/update/{id}", ['as' => 'perfil.update', 'uses'=>'InformacaoPerfilController@update']);
    Route::get("/perfil/show/{id}", ['as' => 'perfil.show', 'uses'=>'InformacaoPerfilController@show']);
    Route::get("/perfil/destroy/{id}", ['as' => 'perfil.destroy', 'uses'=>'InformacaoPerfilController@destroy']);

    Route::get("/ligacao", ['as' => 'ligacao.index', 'uses'=>'LigacaoController@index']);
    Route::get("/ligacao/create", ['as' => 'ligacao.create', 'uses'=>'LigacaoController@create']);
    Route::get("/ligacao/edit/{id}", ['as' => 'ligacao.edit', 'uses'=>'LigacaoController@edit']);
    Route::post("/ligacao/store", ['as' => 'ligacao.store', 'uses'=>'LigacaoController@store']);
    Route::post("/ligacao/update/{id}", ['as' => 'ligacao.update', 'uses'=>'LigacaoController@update']);
    Route::get("/ligacao/show/{id}", ['as' => 'ligacao.show', 'uses'=>'LigacaoController@show']);
    Route::get("/ligacao/destroy/{id}", ['as' => 'ligacao.destroy', 'uses'=>'LigacaoController@destroy']);

    Route::get("/contato/parceiro", ['as' => 'contato.parceiro.index', 'uses'=>'ContatoParceiroController@index']);
    Route::get("/contato/parceiro/create/{cliente_id}", ['as' => 'contato.parceiro.create', 'uses'=>'ContatoParceiroController@create']);
    Route::get("/contato/parceiro/edit/{id}", ['as' => 'contato.parceiro.edit', 'uses'=>'ContatoParceiroController@edit']);
    Route::post("/contato/parceiro/store", ['as' => 'contato.parceiro.store', 'uses'=>'ContatoParceiroController@store']);
    Route::post("/contato/parceiro/update/{id}", ['as' => 'contato.parceiro.update', 'uses'=>'ContatoParceiroController@update']);
    Route::get("/contato/parceiro/show/{id}", ['as' => 'contato.parceiro.show', 'uses'=>'ContatoParceiroController@show']);
    Route::get("/contato/parceiro/destroy/{id}", ['as' => 'contato.parceiro.destroy', 'uses'=>'ContatoParceiroController@destroy']);

    Route::get("/relatorio/cliente", ['as' => 'relatorio.cliente', 'uses'=>'RelatorioController@cliente']);
    Route::get("/relatorio/visita", ['as' => 'relatorio.visita', 'uses'=>'RelatorioController@visita']);
    Route::get("/relatorio/contato", ['as' => 'relatorio.contato', 'uses'=>'RelatorioController@contato']);
    Route::get("/relatorio/credenciamento", ['as' => 'relatorio.credenciamento', 'uses'=>'RelatorioController@credenciamento']);
    Route::get("/relatorio/ligacao", ['as' => 'relatorio.ligacao', 'uses'=>'RelatorioController@ligacao']);

    Route::post("table/cliente", ['as' => 'table.cliente', 'uses'=>'TableController@cliente']);
    Route::post("table/visita", ['as' => 'table.visita', 'uses'=>'TableController@visita']);
    Route::post("table/credenciamento", ['as' => 'table.credenciamento', 'uses'=>'TableController@credenciamento']);
    Route::post("table/contato", ['as' => 'table.contato', 'uses'=>'TableController@contato']);
    Route::post("table/ligacao", ['as' => 'table.ligacao', 'uses'=>'TableController@ligacao']);

    Route::get("/relatorio/cliente/excel/{id}/{status_par}/{param}", ['as' => 'relatorio.cliente.excel', 'uses'=>'RelatorioController@generateCliente']);
    Route::get("/relatorio/visita/excel/{id}/{status_par}/{param}", ['as' => 'relatorio.visita.excel', 'uses'=>'RelatorioController@generateVisita']);
    Route::get("/relatorio/contato/excel/{id}/{status_par}/{param}", ['as' => 'relatorio.contato.excel', 'uses'=>'RelatorioController@generateContato']);
    Route::get("/relatorio/credenciamento/excel/{id}/{param}", ['as' => 'relatorio.credenciamento.excel', 'uses'=>'RelatorioController@generateCredenciamento']);
    Route::get("/relatorio/ligacao/excel/{id}/{param}", ['as' => 'relatorio.ligacao.excel', 'uses'=>'RelatorioController@generateLigacao']);

    Route::post("/relatorio/get/cliente", ['as' => 'relatorio.get.cliente', 'uses'=>'AbertaController@servico']);


    Route::get('/home', 'HomeController@index');
});